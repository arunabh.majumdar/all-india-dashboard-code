#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 21 15:03:22 2021

@author: arunabhmajumdar
"""

import json
import datetime
from datetime import timedelta
import re
import os
import psycopg2
import gspread
import df2gspread as d2g
import pandas as pd
import warnings
warnings.filterwarnings("ignore")
from oauth2client.service_account import ServiceAccountCredentials
import gspread
import gspread_dataframe as gd
import time
import pytz
my_timezone = pytz.timezone('Asia/Calcutta')


connection = psycopg2.connect(user="rainadmin",
                                  password="Mudar123",
                                  host="localhost",
                                  port=55432,
                                  database="rain")

    # Create a cursor to perform database operations
cursor = connection.cursor()
# Print PostgreSQL details
print("PostgreSQL server information")
print(connection.get_dsn_parameters(), "\n")

cursor.execute("SELECT version();")
    # Fetch result
record = cursor.fetchone()
print("You are connected to - ", record, "\n")

def dataframe_generator(query):
    cursor.execute(query)
    print('Read table in PostgreSQL')
    data = cursor.fetchall()
    cols = []
    for elt in cursor.description:
        cols.append(elt[0])
    df= pd.DataFrame(data = data, columns=cols)
    return df
def clean(df):
    df["created_at"] = df["created_at"].dt.date.astype(str)
    df = df[df["created_at"]>"2021-08-31"]
    return df
import base64
import boto3
key_id = "be2bdea3-4ed9-48f2-8123-467fd62292fa"


query = """select * from iam.users u ;"""
iam = dataframe_generator(query)
iam = clean(iam)
iam.rename(columns={"id":"user_id"},inplace=True)
phone_number = iam["phone_number"].astype(str).tolist()
phone_numbers = []
for x in phone_number:
    phone_numbers.append(re.sub("[^0-9]", "", x))
phone_number_2 =[]
for x in phone_numbers:
    if len(x)>10:
        phone_number_2.append(x[2:])
    else:
        phone_number_2.append(x)
iam["phone_number"] = phone_number_2



def iam_checker(user_id):
    return iam[iam["user_id"]==user_id]

query = """select * from ems.employees e ;"""
ems_employees = dataframe_generator(query)
ems_employees = ems_employees[['id', 'user_id', 'employer_id', 'status', 'first_name', 'last_name',
        'created_at', 'updated_at', 'group_id', 'hr_employee_code', 'organization_id', 'paused']]
ems_employees = ems_employees.rename(columns={'id': 'employee_id'})
ems_employees= clean(ems_employees)

def ems_checker(user_id):
    return ems_employees[ems_employees["user_id"]==user_id]



old_values = ems_employees[ems_employees["employee_id"]=="d1cf70ae-9e21-4338-95d8-bec899557c22"].values.tolist()[-1]
old_index = ems_employees[ems_employees["employee_id"]=="d1cf70ae-9e21-4338-95d8-bec899557c22"].columns.tolist()
error_1 = dict(zip(old_index,old_values))
error_1["user_id"]="65bf5ec8-ad63-4f62-a205-ae449800603c"
ems_employees = ems_employees.append(error_1, ignore_index = True)


old_values = ems_employees[ems_employees["employee_id"]=="174b9ecd-dde7-47ea-b357-9ab463a41d67"].values.tolist()[-1]
old_index = ems_employees[ems_employees["employee_id"]=="174b9ecd-dde7-47ea-b357-9ab463a41d67"].columns.tolist()
error_2 = dict(zip(old_index,old_values))
error_2["user_id"]="423889c4-5b1d-403d-aa25-e1bff51af34f"
ems_employees = ems_employees.append(error_2, ignore_index = True)

old_values = ems_employees[ems_employees["employee_id"]=="7a319ee3-3b3c-463d-a67a-7828d75aaba0"].values.tolist()[-1]
old_index = ems_employees[ems_employees["employee_id"]=="7a319ee3-3b3c-463d-a67a-7828d75aaba0"].columns.tolist()
error_2 = dict(zip(old_index,old_values))
error_2["user_id"]="ba1f1026-e586-414a-a3e8-dd8ad58c46b0"
ems_employees = ems_employees.append(error_2, ignore_index = True)


old_values = ems_employees[ems_employees["employee_id"]=="ae742c42-0641-4f9d-a51c-f79371e6a90b"].values.tolist()[-1]
old_index = ems_employees[ems_employees["employee_id"]=="ae742c42-0641-4f9d-a51c-f79371e6a90b"].columns.tolist()
error_2 = dict(zip(old_index,old_values))
error_2["user_id"]="effb1a1a-a43f-4630-bd8f-c6b967e8bcfd"
error_2["employer_id"] = "5b81572b-7422-405e-89a9-4af836bc3895"
ems_employees = ems_employees.append(error_2, ignore_index = True)



old_values = ems_employees[ems_employees["employee_id"]=="44875e29-fab1-4111-9493-858a028ddfc4"].values.tolist()[-1]
old_index = ems_employees[ems_employees["employee_id"]=="44875e29-fab1-4111-9493-858a028ddfc4"].columns.tolist()
error_2 = dict(zip(old_index,old_values))
error_2["user_id"]="0430eb0a-c986-43a0-b873-76963c437ca5"
ems_employees = ems_employees.append(error_2, ignore_index = True)



old_values = ems_employees[ems_employees["employee_id"]=="e0232b50-da50-47dd-86d2-e1bd9fa85185"].values.tolist()[-1]
old_index = ems_employees[ems_employees["employee_id"]=="e0232b50-da50-47dd-86d2-e1bd9fa85185"].columns.tolist()
error_2 = dict(zip(old_index,old_values))
error_2["user_id"]="7c85d049-12e1-4479-b661-3e25223d3b7e"
ems_employees = ems_employees.append(error_2, ignore_index = True)



iam_users = iam[["user_id", "full_name", "email","phone_number", "metadata","document_number"]]
iam_ems_employees = pd.merge(ems_employees,iam_users, on = "user_id")
iam_ems_employees = iam_ems_employees.sort_values("created_at")
gender=[]
birth_date=[]
for i in range(0,iam_ems_employees.shape[0]):
    gender.append(iam_ems_employees["metadata"].iloc[i]["gender"])
    birth_date.append(iam_ems_employees["metadata"].iloc[i]["birth_date"])

iam_ems_employees["Gender"] = gender
iam_ems_employees["birth_date"] = birth_date
iam_ems_employees.drop(["metadata"],1,inplace=True)
iam_ems_employees = iam_ems_employees[['user_id', 'employee_id', 'full_name', 'birth_date', 'Gender', 'employer_id', 'email', 'status', 'phone_number',
'organization_id','document_number','created_at', 'paused']]

iam_ems_employees[iam_ems_employees["user_id"]=="7c85d049-12e1-4479-b661-3e25223d3b7e"]

query = """select * from ems.compensations c ;"""
compensations = dataframe_generator(query)
compensations = clean(compensations)
compensations = compensations[["employee_id", "monthly_salary"]]
compensations["monthly_salary"] = compensations["monthly_salary"]/100
rows_1_39 = pd.merge(iam_ems_employees,compensations, on = "employee_id")


rows_1_39[rows_1_39["user_id"]=="ca357710-e199-4458-9c68-536f62a9836c"]


query = """select * from ems.loan_agreements la ;"""
loan_agreements = dataframe_generator(query)
loan_agreements = clean(loan_agreements)
loan_agreements=loan_agreements[["employee_id", "loan_agreement_number", "expiration_date", "path", "accepted", "accepted_at"]]
loan_agreements["expiration_date"] = pd.to_datetime(loan_agreements["expiration_date"]).dt.date
loan_agreements["today"] = pd.to_datetime("today")
loan_agreements["today"] = loan_agreements["today"].dt.date
loan_agreements["loan_duration"] = loan_agreements["expiration_date"] - loan_agreements["today"]
loan_agreements["loan_Closure_date"] = loan_agreements["expiration_date"]
loan_agreements["accepted_at"] = pd.to_datetime(loan_agreements["accepted_at"]).dt.date
loan_agreements.drop(["expiration_date","today"],1,inplace=True)
loan_agreements.rename(columns={"accepted_date":"loan_agreement_date"},inplace=True)



l = loan_agreements[loan_agreements["employee_id"]=="ab6b57ad-6569-4a85-9513-35cab91011f7"].values.tolist()
l[0][0]='42ed6761-d5ab-46a7-902f-99c873d23ee8'
l1 = pd.DataFrame(l)
l1.columns = loan_agreements.columns.tolist()
loan_agreements = loan_agreements.append(l1, ignore_index=True)
loan_agreements[loan_agreements["employee_id"]=="42ed6761-d5ab-46a7-902f-99c873d23ee8"]


all_rows = pd.merge(rows_1_39,loan_agreements,on = "employee_id")

query = """select * from bnk.transactions t  ;"""
txns = dataframe_generator(query)
txns["second_creation_dummy"] = txns["created_at"]
txns = clean(txns)
txns.rename(columns={"entity_id":"user_id"},inplace=True)
txns.rename(columns={"amount":"Withdrawn Amount"}, inplace=True)
txns.rename(columns={"second_creation_dummy":"disbursal(txn) date"},inplace=True)
txns.rename(columns={"fee":"Total Fees"},inplace=True)
# txns.rename(columns={"reference_id":"Loan Number"},inplace=True)
# txns["Loan Number"] = "'"+txns["Loan Number"]
txns["Total Fees"] = txns["Total Fees"]/100
txns = txns[txns["status"]=="COMPLETE"]
txns = txns[["id","user_id", "Withdrawn Amount","disbursal(txn) date", "Total Fees"]]
txns["Withdrawn Amount"] = txns["Withdrawn Amount"]/100
txns.rename(columns={"id":"tid"},inplace=True)
txns["Total Amount"] = txns["Withdrawn Amount"]+txns["Total Fees"]


query = """select * from kbill.employer_invoice_deductions eid ;"""
eid = dataframe_generator(query)
eid = clean(eid)
eid = eid[["entity_id","reference_id"]]
eid["reference_id"] = "'"+eid["reference_id"]
eid.rename(columns  = {"entity_id":"tid", "reference_id":"Loan_Number"},inplace=True)
txns = pd.merge(txns,eid, on = "tid")
txns["disbursal(txn) date"] = txns["disbursal(txn) date"].dt.tz_convert(my_timezone)

all_rows = pd.merge(all_rows, txns, on = "user_id")
total_amounts=all_rows["Total Amount"].tolist()



total_fees = []
for x in total_amounts:
    if 50<x<301:
        total_fees.append(12)
    elif 300<x<501:
        total_fees.append(18)
    elif 500<x<1001:
        total_fees.append(35)
    elif 1000<x<1501:
        total_fees.append(60)
    elif 1500<x<2501:
        total_fees.append(100)
    elif 2500<x<5001:
        total_fees.append(200)
    elif 5000<x<10001:
        total_fees.append(275)
    elif 10000<x<25001:
        total_fees.append(700)
    elif 25000<x<50001:
        total_fees.append(800)
    elif 50000<x<100001:
        total_fees.append(1500)
    else:
        total_fees.append(0)
all_rows["Total Fees Calculated"] = total_fees
all_rows["processing_fees"] = round(all_rows["Total Fees Calculated"]/1.18,2)
all_rows["GST_fees"] = all_rows["Total Fees Calculated"] - all_rows["processing_fees"]


all_rows["Annual_income"] = (all_rows["monthly_salary"]*12).astype(float)
all_rows["Annual_income"] = all_rows["Annual_income"].astype(str)
all_rows["Total Amount"] = all_rows["Withdrawn Amount"]+all_rows["GST_fees"]+all_rows["processing_fees"]
query = """select * from ems.work_period_balances wpb ;"""
wpb = dataframe_generator(query)
wpb = clean(wpb)

dd = wpb[wpb["employee_id"]=="ab6b57ad-6569-4a85-9513-35cab91011f7"]

dd["employee_id"]=["42ed6761-d5ab-46a7-902f-99c873d23ee8", "42ed6761-d5ab-46a7-902f-99c873d23ee8", "42ed6761-d5ab-46a7-902f-99c873d23ee8"]
wpb = wpb.append(dd, ignore_index=True)

wpb = wpb[["employee_id", "available_amount", "payment_date"]]
wpb.rename(columns={"available_amount":"overall_limit", "payment_date":"next_payment_date"},inplace=True)
wpb["next_payment_date"] = pd.to_datetime(wpb["next_payment_date"]).dt.date
wpb["overall_limit"]  = wpb["overall_limit"]/100
all_rows = pd.merge(all_rows, wpb, on = "employee_id")
# all_rows["disbursal(txn) date"] = pd.to_datetime(all_rows["disbursal(txn) date"])
all_rows = all_rows.sort_values("disbursal(txn) date", ascending=True)
all_rows = all_rows.drop_duplicates(["tid"])


all_rows = all_rows.sort_values("disbursal(txn) date", ascending=True)
all_rows = all_rows.drop_duplicates(["tid"])

all_rows["Sanctioned Loan Limit"] = (all_rows["monthly_salary"]*0.4)

uid = all_rows["user_id"].unique().tolist()
sum_of_withdrawals = []
for x in uid:
    sum_of_withdrawals.append(all_rows[all_rows["user_id"]==x]["Total Amount"].sum())
dddd = dict(zip(uid,sum_of_withdrawals))
all_rows["disbursed amount"] = all_rows['user_id'].map(dddd)  
all_rows["Undisbursed amount"] = all_rows["Sanctioned Loan Limit"] - all_rows["disbursed amount"]


def hello_kms_bank(encrypted_text):
    session = boto3.session.Session(profile_name="rain-india-production")
    client = session.client("kms")
    encrypted_text = encrypted_text
    decrypted = client.decrypt(CiphertextBlob=base64.b64decode(encrypted_text))
    decrypted_text = decrypted["Plaintext"].decode("utf-8").split("\x1d")
    d = {}
    for c in range(len(decrypted_text)):
        d[decrypted_text[c].split("::")[0]] = decrypted_text[c].split("::")[1]
    dd = pd.DataFrame(d.items()).T
    dd.columns = dd.iloc[0]
    dd = dd[1:]
    return dd
def hello_kms_address(encrypted_text):
    session = boto3.session.Session(profile_name="rain-india-production")
    client = session.client("kms")
    encrypted_text = encrypted_text
    decrypted = client.decrypt(CiphertextBlob=base64.b64decode(encrypted_text))
    decrypted_text = decrypted["Plaintext"].decode("utf-8").split("\x1d")
    return decrypted_text


due_dates = pd.read_table("API1-20-Dec-21.txt", header = None)

due_dates = due_dates[2:]
ll = []
for x in due_dates[0].tolist():
    ll.append(x.strip("|"))
due_dates[0] = ll

import ast
loan_number = []
due_date = []
l = []
for x in due_dates[0].tolist():
    l.append(ast.literal_eval(x))
for x in l:
    loan_number.append(x["Loan_Number"])
    due_date.append(x["LoanStartDate"])
all_rows["Loan_Number"] = all_rows["Loan_Number"].str.strip("'").astype(str)
all_rows["Loan_Number_1"] = all_rows["Loan_Number"]
dd = dict(zip(loan_number,due_date))
all_rows["Due_Date"] = all_rows["Loan_Number_1"].map(dd)
all_rows["Loan_Number"] = "'"+ all_rows["Loan_Number"]
all_rows.drop(["Loan_Number_1"],1,inplace=True)


a = all_rows[all_rows["user_id"]=="03c696bf-1034-4c1d-8b5b-d639c645f188"]
import numpy as np
a["Due_Date"].fillna(0,inplace=True)
a["Due_Date"].replace({0:"2021-12-08"},inplace=True)

a = all_rows[all_rows["user_id"]=="65bf5ec8-ad63-4f62-a205-ae449800603c"]
import numpy as np
a["Due_Date"].fillna(0,inplace=True)
a["Due_Date"].replace({0:"2021-12-08"},inplace=True)

a = all_rows[all_rows["user_id"]=="0c604c37-f25f-43f7-a309-bfa343d665ea"]
import numpy as np
a["Due_Date"].fillna(0,inplace=True)
a["Due_Date"].replace({0:"2021-12-08"},inplace=True)

a = all_rows[all_rows["user_id"]=="423889c4-5b1d-403d-aa25-e1bff51af34f"]
import numpy as np
a["Due_Date"].fillna(0,inplace=True)
a["Due_Date"].replace({0:"2022-01-08"},inplace=True)

all_rows.sort_values(["disbursal(txn) date"], inplace=True)

# query = """select * from bnk.external_accounts ea ;"""
# external_accounts = dataframe_generator(query)
# external_accounts = clean(external_accounts)
# external_accounts = external_accounts[external_accounts["user_id"].isin(all_rows["user_id"].unique().tolist())]
# account_details = external_accounts["account_details"].tolist()
# decrypted_account_details = []
# count=0
# for x in account_details:
#     decrypted_account_details.append(hello_kms_bank(x))
#     count+=1
#     print (count)
# decrypted_account_details_df = pd.concat(decrypted_account_details)
# acc_holder_name = decrypted_account_details_df["accountHolderName"].tolist()
# bank_acc_number = decrypted_account_details_df["bankAccountNumber"].tolist()
# bank_acc_number= ["'"+str(x) for x in bank_acc_number]
# bank_ifsc= decrypted_account_details_df["bankRoutingNumber"].tolist()
# external_accounts["accountHolderName"] = acc_holder_name
# external_accounts["bankAccountNumber"] = bank_acc_number
# external_accounts["bankIFSCNumber"] = bank_ifsc
# external_accounts = external_accounts[["user_id","accountHolderName", "bankAccountNumber","bankIFSCNumber"]]
# all_rows = pd.merge(all_rows, external_accounts, on = "user_id")



all_rows = all_rows.drop_duplicates(["tid"])
all_rows.sort_values(["disbursal(txn) date"], inplace=True)
all_rows_1 = all_rows.copy()

new_row = pd.read_excel("data_2.xlsx")

cols = all_rows_1.columns.tolist()
new_row = new_row[cols]

all_rows_1 = pd.concat([all_rows_1,new_row])

os.chdir("..")
os.chdir("Outputs")
all_rows_1.to_csv("all_rows.csv")
os.chdir("..")
os.chdir("Code")
scope = ["https://www.googleapis.com/auth/spreadsheets", "https://www.googleapis.com/auth/drive.file", 
        "https://www.googleapis.com/auth/drive"]
creds = ServiceAccountCredentials.from_json_keyfile_name("cs-and-ops-dashboard-8febbecf58a8.json", scope)
client = gspread.authorize(creds)

os.chdir("..")
os.chdir("Outputs")
df = all_rows_1.copy()
employees_kyc_demographic= client.open("CS/OPS Dashboard").worksheet("Withdrawals(txns)")
employees_kyc_demographic.clear()
existing = gd.get_as_dataframe(employees_kyc_demographic)
existing = pd.DataFrame(employees_kyc_demographic.get_all_records())
updated = existing.append(df)
gd.set_with_dataframe(employees_kyc_demographic, updated)

os.chdir("..")
os.chdir("Code")



all_rows_grouped = all_rows_1.groupby("user_id").last().reset_index()

wd = []
pf = []
gst = []
ta =[]
for x in all_rows_grouped["user_id"].unique().tolist():
    wd.append(all_rows[all_rows["user_id"]==x]["Withdrawn Amount"].sum())
    pf.append(all_rows[all_rows["user_id"]==x]["processing_fees"].sum())
    gst.append(all_rows[all_rows["user_id"]==x]["GST_fees"].sum())
    ta.append(all_rows[all_rows["user_id"]==x]["Total Amount"].sum())
    
all_rows_grouped["Withdrawn Amount"] = wd
all_rows_grouped["processing_fees"] = pf
all_rows_grouped["GST_fees"] = gst
all_rows_grouped["Total Amount"] = ta


print (os.getcwd())
#printing to make sure we are in code_folder, Repayments are stored 
print (os.getcwd())# Changing code folder to Google Drive folder for Repayemnts

os.chdir("..") #Going back a folder
os.chdir("..") #Going back a folder
os.chdir("..") #Going back a folder
print (os.getcwd())


os.chdir("Google Drive/Shared drives/Repayments") #Changing directory to Repayments


#Because the repayments are stored date wise, selecting today's date and 
#searching for a folder which has been named today's date'


from datetime import date
from datetime import timedelta
today = date.today() #Getting today's date

print("Today's date:", today) #Today's date is in timestamp format
today = today.strftime("%d-%b-%Y")

os.chdir(today)
print (os.getcwd())
# Checking to see if folder is correct
# pd.read_excel(os.listdir()[0])


#The repayments 
total_refunds_2_oct= pd.read_excel((os.listdir()[0]), sheet_name="Oct") #Reading sheet Oct and so on
total_refunds_2_nov= pd.read_excel((os.listdir()[0]), sheet_name="Nov")
total_refunds_2_dec= pd.read_excel((os.listdir()[0]), sheet_name="Dec")
#combining all sheets into one consolidated dataframe column wise. 
total_refunds_2 = pd.concat([total_refunds_2_oct,total_refunds_2_nov, total_refunds_2_dec])
total_refunds_2.rename(columns={"Due date":"created at"},inplace=True)
#Changing name of Due date to created at to make sure we don't have issues with Data Studio. DS takes only created at date now. 
total_refunds_2.drop(["full_name"],1,inplace=True)
total_refunds = total_refunds_2.copy()
total_refunds["created at"] = total_refunds["created at"].astype(str)
#Always changing date time to string (object in pandas ) to make sure we can filter dates effectively. 

print (os.getcwd())
os.chdir("/Users/arunabhmajumdar/Documents/all-india-dashboard-code/Code")

other_refunds = pd.read_excel("Refund_18112021.xlsx")
other_refunds.rename(columns={"CODES":"user_id", "AMOUNT":"Other_Refunded"},inplace=True)
all_rows_grouped = pd.merge(all_rows_grouped, other_refunds, on = "user_id", how = "left")
all_rows_grouped["Other_Refunded"] = all_rows_grouped["Other_Refunded"].fillna(0)
total_refunds = total_refunds[["user_id", "repayment"]]

total_refunds = total_refunds.groupby(["user_id"]).sum().reset_index()
rd = dict(zip(total_refunds["user_id"].tolist(), total_refunds["repayment"].tolist()))


all_rows_grouped["TA"] = all_rows_grouped["user_id"]
all_rows_grouped["Repayment"]= all_rows_grouped["TA"].map(rd)
all_rows_grouped["Repayment"] = all_rows_grouped["Repayment"].fillna(0)
all_rows_grouped["Closing Balance"] = all_rows_grouped["Total Amount"] - all_rows_grouped["Repayment"] + all_rows_grouped["Other_Refunded"]
all_rows_grouped["Undisbursed Amount"] = all_rows_grouped["Sanctioned Loan Limit"] - all_rows_grouped["Closing Balance"]


query = """select * from kyc.documents kyc ;"""
kyc = dataframe_generator(query)
kyc = clean(kyc)
uid = all_rows_grouped["user_id"].unique().tolist()
details = []
for x in uid:
    details.append(kyc[kyc["user_id"]==x]["details"].tolist())

details_3=[]
for x in details:
    details_2=[]
    for y in x:
#         print (json.loads(y))
        details_2.append(json.loads(y))
    details_3.append(details_2)  

data=[]
for x in details_3:
    data_2=[]
    for y in x:
        if type(y)==dict:
            if "data" in y:
                if "data" in y["data"]:
                    if "address" in y["data"]["data"]:
                        print (y["data"]["data"]["address"]["value"])
                        data_2.append(y["data"]["data"]["address"]["value"])
    data.append(data_2)

data_23 = []
for x in data:
    if len(x)>1:
        data_23.append(x[-1])
    else:
        data_23.append(x)
        
        
        
all_rows_grouped["address"] = data_23
query = """select * from kyc.documents kyc ;"""
kyc = dataframe_generator(query)
kyc = clean(kyc)
uid = all_rows_grouped["user_id"].unique().tolist()
details = []
for x in uid:
    details.append(kyc[kyc["user_id"]==x]["details"].tolist())

details_3=[]
for x in details:
    details_2=[]
    for y in x:
#         print (json.loads(y))
        details_2.append(json.loads(y))
    details_3.append(details_2)  

data=[]
for x in details_3:
    data_2=[]
    for y in x:
        if type(y)==dict:
            if "data" in y:
                if "data" in y["data"]:
                    if "aadhaar" in y["data"]["data"]:
                        print (y["data"]["data"]["aadhaar"]["value"])
                        data_2.append(y["data"]["data"]["aadhaar"]["value"])
    data.append(data_2)

data_23 = []
for x in data:
    if len(x)>1:
        data_23.append(x[-1])
    else:
        data_23.append(x)
        
        
        
        


all_rows_grouped["aadhaar"] = data_23



# all_rows_grouped = all_rows_grouped[['user_id', 'employee_id', 'full_name', 'birth_date', 'Gender',
#         'employer_id', 'email', 'status', 'phone_number', 'organization_id','address', 'paused','aadhaar',
#         'document_number', 'created_at', 'monthly_salary', 'accepted', 'accepted_at',
#         'loan_duration', 'loan_Closure_date', 'tid','disbursal(txn) date', 'Withdrawn Amount',
#     'processing_fees', 'GST_fees',  'Total Fees','Repayment', 'Closing Balance','Total Amount', 'Sanctioned Loan Limit', 'Undisbursed Amount','Annual_income','overall_limit', 'next_payment_date', 'Due_Date',
#         'accountHolderName', 'bankAccountNumber', 'bankIFSCNumber', 'Other_Refunded']]



all_rows_grouped = all_rows_grouped[['user_id', 'employee_id', 'full_name', 'birth_date', 'Gender',
        'employer_id', 'email', 'status', 'phone_number', 'organization_id','address', 'paused','aadhaar',
        'document_number', 'created_at', 'monthly_salary', 'accepted', 'accepted_at',
        'loan_duration', 'loan_Closure_date', 'tid','disbursal(txn) date', 'Withdrawn Amount',
    'processing_fees', 'GST_fees',  'Total Fees','Repayment', 'Closing Balance','Total Amount', 'Sanctioned Loan Limit', 
    'Undisbursed Amount','Annual_income','overall_limit', 'next_payment_date', 'Due_Date','Other_Refunded']]

# ifsc_list = all_rows_grouped["bankIFSCNumber"].tolist()
# import requests

# URL = "https://ifsc.razorpay.com/"
# bank = []
# bc = 0
# for x in ifsc_list:
#     bc+=1
#     ba = {}
#     try:
#         data = requests.get(URL+x).json()
#         ba['IFSC'] = x
#         ba["Bank Name"] = data["BANK"]
#         ba["Bank Address"] = data["ADDRESS"]
#         ba["Bank Branch"] = data["BRANCH"]
#         bank.append(ba)
#     except:
#         data = {"BANK":"Not Found", "ADDRESS":"Not Found", "BRANCH":"Not Found"}
#         ba['IFSC'] = x
#         ba["Bank Name"] = data["BANK"]
#         ba["Bank Address"] = data["ADDRESS"]
#         ba["Bank Branch"] = data["BRANCH"]
#         bank.append(ba)
        
#     print (bc)
    
# bank = pd.DataFrame(bank)
# all_rows_grouped["Bank Name"] = bank["Bank Name"]
# all_rows_grouped["Bank Branch"] = bank["Bank Branch"]
# all_rows_grouped["Bank Address"] = bank["Bank Address"]


df = all_rows_grouped.copy()
employees_kyc_demographic= client.open("CS/OPS Dashboard").worksheet("Withdrawals(users)")
employees_kyc_demographic.clear()
existing = gd.get_as_dataframe(employees_kyc_demographic)
existing = pd.DataFrame(employees_kyc_demographic.get_all_records())
updated = existing.append(all_rows_grouped.copy())
gd.set_with_dataframe(employees_kyc_demographic, updated)


overflow_users  = all_rows_grouped[all_rows_grouped["Undisbursed Amount"]<0]
df = overflow_users.copy()
employees_kyc_demographic= client.open("CS/OPS Dashboard").worksheet("Overflow users")
employees_kyc_demographic.clear()
existing = gd.get_as_dataframe(employees_kyc_demographic)
existing = pd.DataFrame(employees_kyc_demographic.get_all_records())
updated = existing.append(df)
gd.set_with_dataframe(employees_kyc_demographic, updated)
print ("Successful Run - Withdrawals without bank updated")














