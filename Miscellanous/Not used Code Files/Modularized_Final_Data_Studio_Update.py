#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 21 16:03:44 2021

@author: arunabhmajumdar
"""


import warnings
warnings.filterwarnings("ignore")
import pandas as pd,os
import base64
import boto3
import time
import pandas as pd, os
from oauth2client.service_account import ServiceAccountCredentials
import gspread
import gspread_dataframe as gd
import psycopg2
import df2gspread as d2g
import pandas as pd
import warnings
warnings.filterwarnings("ignore")
import numpy as np
import re
import calendar
import os
import json
import time
import numpy as np
import pytz
my_timezone = pytz.timezone('Asia/Calcutta')
import ast
import gzip
from datetime import timedelta
start = time.time()
print ("Starting timer")

scope = ["https://www.googleapis.com/auth/spreadsheets", "https://www.googleapis.com/auth/drive.file", 
            "https://www.googleapis.com/auth/drive"]
creds = ServiceAccountCredentials.from_json_keyfile_name("cs-and-ops-dashboard-8febbecf58a8.json", scope)
client_2 = gspread.authorize(creds)
employees_kyc_demographic= client_2.open("CS/OPS Dashboard").worksheet("New Quess Landing Page (updated)")
existing = gd.get_as_dataframe(employees_kyc_demographic)
existing = pd.DataFrame(employees_kyc_demographic.get_all_records())
existing["created_at"] = pd.to_datetime(existing["created_at"]).dt.date
landing_page = existing.copy()


scope = ["https://www.googleapis.com/auth/spreadsheets", "https://www.googleapis.com/auth/drive.file", 
            "https://www.googleapis.com/auth/drive"]
creds = ServiceAccountCredentials.from_json_keyfile_name("cs-and-ops-dashboard-8febbecf58a8.json", scope)
client_2 = gspread.authorize(creds)
employees_kyc_demographic= client_2.open("Data Studio Final Dashboard").worksheet("Sheet3")
employees_kyc_demographic.clear()
existing = gd.get_as_dataframe(employees_kyc_demographic)
existing = pd.DataFrame(employees_kyc_demographic.get_all_records())
updated = existing.append(landing_page.copy())
gd.set_with_dataframe(employees_kyc_demographic, updated)


print (os.getcwd())
#printing to make sure we are in code_folder, Repayments are stored 
print (os.getcwd())# Changing code folder to Google Drive folder for Repayemnts

os.chdir("..") #Going back a folder
os.chdir("..") #Going back a folder
os.chdir("..") #Going back a folder
print (os.getcwd())


os.chdir("Google Drive/Shared drives/Repayments") #Changing directory to Repayments


#Because the repayments are stored date wise, selecting today's date and 
#searching for a folder which has been named today's date'


from datetime import date
from datetime import timedelta
today = date.today() #Getting today's date

print("Today's date:", today) #Today's date is in timestamp format
today = today.strftime("%d-%b-%Y")

os.chdir(today)
print (os.getcwd())
# Checking to see if folder is correct
# pd.read_excel(os.listdir()[0])


#The repayments 
total_refunds_2_oct= pd.read_excel((os.listdir()[0]), sheet_name="Oct") #Reading sheet Oct and so on
total_refunds_2_nov= pd.read_excel((os.listdir()[0]), sheet_name="Nov")
total_refunds_2_dec= pd.read_excel((os.listdir()[0]), sheet_name="Dec")
#combining all sheets into one consolidated dataframe column wise. 
total_refunds_2 = pd.concat([total_refunds_2_oct,total_refunds_2_nov, total_refunds_2_dec])
total_refunds_2.rename(columns={"Due date":"created at"},inplace=True)
#Changing name of Due date to created at to make sure we don't have issues with Data Studio. DS takes only created at date now. 
total_refunds_2.drop(["full_name"],1,inplace=True)
total_refunds = total_refunds_2.copy()
total_refunds["created at"] = total_refunds["created at"].astype(str)
#Always changing date time to string (object in pandas ) to make sure we can filter dates effectively. 

print (os.getcwd())
os.chdir("/Users/arunabhmajumdar/Documents/all-india-dashboard-code/Code")







def group_clean(df, date):
    df1 = df[["user_id", "repayment"]]
    df1 = df1.groupby("user_id").sum().reset_index()
    df2 = df[["user_id", "Due date"]]
    df2 = df2.groupby("user_id").last().reset_index()
    df = pd.merge(df1,df2, on = "user_id")
    df["disbursal(txn) date"] = df["Due date"]
    df.drop(["Due date"],1,inplace = True)
    df["disbursal(txn) date"] = date
    return df



sep_repayment = group_clean(total_refunds_2_oct, "2021-09-30")
oct_repayment = group_clean(total_refunds_2_nov, "2021-10-31")
dec_repayment = group_clean(total_refunds_2_dec, "2021-11-30")

tr = pd.concat([sep_repayment, oct_repayment, dec_repayment])



print ("starting run")
session = boto3.session.Session(profile_name="rain-india-production")
client = session.client("dynamodb")
dynamodb = boto3.resource("dynamodb")

connection = psycopg2.connect(user="rainadmin",
                                      password="Mudar123",
                                      host="localhost",
                                      port=55432,
                                      database="rain")
cursor = connection.cursor()
# Print PostgreSQL details
print("PostgreSQL server information")
print(connection.get_dsn_parameters(), "\n")

cursor.execute("SELECT version();")
    # Fetch result
record = cursor.fetchone()
print("You are connected to - ", record, "\n")
def dataframe_generator(query):
    cursor.execute(query)
    print('Read table in PostgreSQL')
    data = cursor.fetchall()
    cols = []
    for elt in cursor.description:
        cols.append(elt[0])
    df= pd.DataFrame(data = data, columns=cols)
    return df

def clean(df):
    df["created_at"] = df["created_at"].dt.date.astype(str)
    df = df[df["created_at"]>"2021-08-31"]
    return df


os.chdir("..")
os.chdir("Outputs")

all_rows_1 = pd.read_csv("all_rows.csv")
query = """select * from xorg.employers e;"""
xorg = dataframe_generator(query)
xorg = xorg[["id","organization_id", "lookup_name"]]
xorg["lookup_name"] = xorg["lookup_name"].str.lower()
xorg.rename(columns={"id":"employer_id"},inplace=True)
xorg = xorg[["employer_id", "lookup_name"]]
all_rows_1 = pd.merge(all_rows_1, xorg, on = "employer_id", how = "left")

all_rows_1["disbursal(txn) date"] = pd.to_datetime(all_rows_1["disbursal(txn) date"]).dt.date.astype(str)
all_rows_1= all_rows_1[['user_id','birth_date', "email", 'Gender','organization_id','monthly_salary',
           'tid','disbursal(txn) date','Total Amount', 'processing_fees', 
       'overall_limit', 'Sanctioned Loan Limit', "lookup_name"]]
all_rows_1["organization_id"].replace({"c2a6a007-e625-456f-8c36-92cd2654c971": 'Quess',
                                              "916227f6-cb69-46ec-8cb1-a735ed98f2c4": 'D2C Org', 
                                             "d779558a-09cc-4920-9f39-d8409c8f0728":"B2B Test"},inplace=True)

gender_dict = {'Male':"male", 'MALE':"male", 'male':"male", 'Female':"female", 'M':"male", ' MALE ':"male", 
               'F':"female", 'female':"female"}
all_rows_1["Gender"] = all_rows_1["Gender"].map(gender_dict)
all_rows_1["age"] = (pd.to_datetime("today") - pd.to_datetime(all_rows_1["birth_date"])).dt.days
all_rows_1["age"] = round(all_rows_1["age"]/365,0)
bins = [0, 20, 25, 30, 35, 40, 50, 60,100]
labels = ["under 20","20-25","25-30","30-35","35-40","40-50", "50-60", "60+"]
all_rows_1['binned_age'] = pd.cut(all_rows_1['age'], bins=bins, labels=labels)
bins = [0, 15000, 30000, 50000, 80000, 100000, 500000]
labels = ["under 15k","15k-30k","30k-50k","50k-80k","80k-100k","100k+"]
all_rows_1['binned_salary'] = pd.cut(all_rows_1['monthly_salary'], bins=bins, labels=labels)
os.chdir("..")
os.chdir("Code")
tr = pd.merge(tr, all_rows_1[["user_id", "organization_id"]].groupby("user_id").last().reset_index(), on = "user_id")
scope = ["https://www.googleapis.com/auth/spreadsheets", "https://www.googleapis.com/auth/drive.file", 
            "https://www.googleapis.com/auth/drive"]
creds = ServiceAccountCredentials.from_json_keyfile_name("cs-and-ops-dashboard-8febbecf58a8.json", scope)
client_2 = gspread.authorize(creds)
employees_kyc_demographic= client_2.open("Data Studio Final Dashboard").worksheet("Sheet1")
employees_kyc_demographic.clear()
existing = gd.get_as_dataframe(employees_kyc_demographic)
existing = pd.DataFrame(employees_kyc_demographic.get_all_records())
updated = existing.append(all_rows_1.copy())
gd.set_with_dataframe(employees_kyc_demographic, updated)


scope = ["https://www.googleapis.com/auth/spreadsheets", "https://www.googleapis.com/auth/drive.file", 
            "https://www.googleapis.com/auth/drive"]
creds = ServiceAccountCredentials.from_json_keyfile_name("cs-and-ops-dashboard-8febbecf58a8.json", scope)
client_2 = gspread.authorize(creds)
employees_kyc_demographic= client_2.open("Data Studio Final Dashboard").worksheet("Sheet2")
employees_kyc_demographic.clear()
existing = gd.get_as_dataframe(employees_kyc_demographic)
existing = pd.DataFrame(employees_kyc_demographic.get_all_records())
updated = existing.append(tr.copy())
gd.set_with_dataframe(employees_kyc_demographic, updated)


query = """select * from iam.users u ;"""
iam = dataframe_generator(query)
iam = clean(iam)
iam.rename(columns={"id":"user_id"},inplace=True)
phone_number = iam["phone_number"].astype(str).tolist()
phone_numbers = []
for x in phone_number:
    phone_numbers.append(re.sub("[^0-9]", "", x))
phone_number_2 =[]
for x in phone_numbers:
    if len(x)>10:
        phone_number_2.append(x[2:])
    else:
        phone_number_2.append(x)
iam["phone_number"] = phone_number_2

iam = iam[["user_id", "full_name", "status", "email", "phone_number", "created_at", "metadata"]]

gender=[]
birth_date=[]
for i in range(0,iam.shape[0]):
    gender.append(iam["metadata"].iloc[i]["gender"])
    birth_date.append(iam["metadata"].iloc[i]["birth_date"])

iam["Gender"] = gender
iam["birth_date"] = birth_date
iam.drop(["metadata"],1,inplace=True)

query = """select * from ems.employees e ;"""
ems_employees = dataframe_generator(query)
ems_employees = ems_employees[['id', 'user_id', 'employer_id', 'status', 'first_name', 'last_name',
        'created_at', 'updated_at', 'group_id', 'hr_employee_code', 'organization_id']]
ems_employees = ems_employees.rename(columns={'id': 'employee_id'})
ems_employees= clean(ems_employees)

ems_employees = ems_employees[["employee_id", "user_id", "employer_id"]]

query = """select * from xorg.employers e;"""
xorg = dataframe_generator(query)
xorg = xorg[["id","organization_id", "lookup_name"]]
xorg["lookup_name"] = xorg["lookup_name"].str.lower()
xorg.rename(columns={"id":"employer_id"},inplace=True)



xorg["organization_id"].replace({"c2a6a007-e625-456f-8c36-92cd2654c971": 'Quess',
                                              "916227f6-cb69-46ec-8cb1-a735ed98f2c4": 'D2C Org', 
                                             "d779558a-09cc-4920-9f39-d8409c8f0728":"B2B Test"},inplace=True)


ems_xorg= pd.merge(ems_employees, xorg, on = "employer_id")

iam_ems_xorg = pd.merge(iam, ems_xorg, on = "user_id", how = "left")

quess = iam_ems_xorg[iam_ems_xorg["organization_id"]=="Quess"]
d2c = iam_ems_xorg[iam_ems_xorg["organization_id"]=="D2C Org"]

query = """select * from risk.user_risk_verifications urv; """
cv = dataframe_generator(query)
cv = clean(cv)

cv.rename(columns={"score":"Approved"},inplace=True)

os.chdir("..")
os.chdir("AWS_Data/")
rootdir = os.getcwd()
files_dump =[]
for subdir, dirs, files in os.walk(rootdir):
    for file in files:
#             print(os.path.join(subdir, file))
        if file.endswith("json"):
            files_dump.append(os.path.join(subdir, file))
ff = []
for x in files_dump:
    fff = {}
    f = open(x)
    try:
        data = json.load(f)
    except:
        data = "Json Failure - Issue at our AWS end"
    fff["user_id"] = str(f).split("/")[-2]
    fff["data"] = data
    ff.append(fff)
ffff = pd.DataFrame(ff)
os.chdir("..")
os.chdir("Code")

aws_approved = []
for x in ffff["data"]:
    if x=="Yes":
        aws_approved.append(True)
    else:
        aws_approved.append(False)
ffff["Aws Approved"] = aws_approved
cv = pd.merge(cv,ffff, on = "user_id", how = "left")
quess_cv = cv[["user_id", "Approved", "data"]]
d2c_cv = cv[["user_id", "Approved", "data", "underwriting", "fraud", "kyc"]]

quess = pd.merge(quess, quess_cv, on = "user_id", how = "left")
quess["Approved"].replace({False: 'Rejected', True: 'Approved'}, inplace=True)
d2c = pd.merge(d2c, d2c_cv, on = "user_id", how = "left")
d2c["Approved"].replace({False: 'Rejected', True: 'Approved'}, inplace=True)
d2c['fraud'] = d2c['fraud'].replace({False: 'Rejected', True: 'Approved'})
d2c['underwriting'] = d2c['underwriting'].replace({False: 'Rejected', True: 'Approved'})
d2c["kyc"] = d2c["kyc"].replace({False: 'Rejected', True: 'Approved'})

query = """select * from elog.events e;"""
elog = dataframe_generator(query)
categories = elog["action"].value_counts().index.tolist()

query = """select * from kyc.documents kyc ;"""
kyc = dataframe_generator(query)
kyc = clean(kyc)


kyc = kyc.groupby("user_id").last().reset_index()[["user_id", "approved", "document_type", "side"]]
kyc.rename(columns={"approved":"kyc_approved"},inplace=True)
quess = pd.merge(quess, kyc, on = "user_id", how = "left")
d2c = pd.merge(d2c, kyc, on = "user_id", how = "left")
d2c["kyc_approved"].replace({False: 'Rejected', True: 'Approved'}, inplace=True)
quess["kyc_approved"].replace({False: 'Rejected', True: 'Approved'}, inplace=True)


query = """select * from bnk.external_accounts ea ;"""
bnk_external = dataframe_generator(query)
bnk_external = clean(bnk_external)



bnk_external = bnk_external[["user_id", "status"]]
bnk_external.rename(columns={"status":"bank_status"},inplace=True)

bnk_external = bnk_external[bnk_external["bank_status"]=="ACTIVE"]

quess = pd.merge(quess, bnk_external, on = "user_id", how = "left")

d2c = pd.merge(d2c,bnk_external, on = "user_id", how = "left")

query = """select * from ems.loan_agreements la ;"""
la = dataframe_generator(query)
la = clean(la)
la = la[["employee_id", "accepted"]]


quess = pd.merge(quess, la, on = "employee_id", how = "left")
d2c = pd.merge(d2c, la, on = "employee_id", how = "left")
scope = ["https://www.googleapis.com/auth/spreadsheets", "https://www.googleapis.com/auth/drive.file", 
            "https://www.googleapis.com/auth/drive"]
creds = ServiceAccountCredentials.from_json_keyfile_name("cs-and-ops-dashboard-8febbecf58a8.json", scope)
client_2 = gspread.authorize(creds)
employees_kyc_demographic= client_2.open("Data Studio Final Dashboard").worksheet("Sheet4")
employees_kyc_demographic.clear()
existing = gd.get_as_dataframe(employees_kyc_demographic)
existing = pd.DataFrame(employees_kyc_demographic.get_all_records())
updated = existing.append(quess.copy())
gd.set_with_dataframe(employees_kyc_demographic, updated)
scope = ["https://www.googleapis.com/auth/spreadsheets", "https://www.googleapis.com/auth/drive.file", 
            "https://www.googleapis.com/auth/drive"]
creds = ServiceAccountCredentials.from_json_keyfile_name("cs-and-ops-dashboard-8febbecf58a8.json", scope)
client_2 = gspread.authorize(creds)
employees_kyc_demographic= client_2.open("Data Studio Final Dashboard").worksheet("Sheet5")
employees_kyc_demographic.clear()
existing = gd.get_as_dataframe(employees_kyc_demographic)
existing = pd.DataFrame(employees_kyc_demographic.get_all_records())
updated = existing.append(d2c.copy())
gd.set_with_dataframe(employees_kyc_demographic, updated)




query = """select * from iam.users u ;"""
iam = dataframe_generator(query)
iam = clean(iam)
iam.rename(columns={"id":"user_id"},inplace=True)
phone_number = iam["phone_number"].astype(str).tolist()
phone_numbers = []
for x in phone_number:
    phone_numbers.append(re.sub("[^0-9]", "", x))
phone_number_2 =[]
for x in phone_numbers:
    if len(x)>10:
        phone_number_2.append(x[2:])
    else:
        phone_number_2.append(x)
iam["phone_number"] = phone_number_2
query = """select * from ems.employees e ;"""
ems_employees = dataframe_generator(query)
ems_employees = ems_employees[['id', 'user_id', 'employer_id', 'status', 'first_name', 'last_name',
        'created_at', 'updated_at', 'group_id', 'hr_employee_code', 'organization_id']]
ems_employees = ems_employees.rename(columns={'id': 'employee_id'})
ems_employees= clean(ems_employees)
query = """select * from xorg.employers e;"""
xorg = dataframe_generator(query)
xorg = xorg[["id", "lookup_name"]]
xorg["lookup_name"] = xorg["lookup_name"].str.lower()
xorg.rename(columns={"id":"employer_id"},inplace=True)
ems_xorg = pd.merge(ems_employees, xorg, on = "employer_id")
iam_users = iam[["user_id", "full_name", "email","phone_number", "metadata","document_number"]]
iam_ems_employees = pd.merge(ems_xorg, iam_users, on = "user_id")
iam_ems_employees = iam_ems_employees.sort_values("created_at")
gender=[]
birth_date=[]
for i in range(0,iam_ems_employees.shape[0]):
    gender.append(iam_ems_employees["metadata"].iloc[i]["gender"])
    birth_date.append(iam_ems_employees["metadata"].iloc[i]["birth_date"])

iam_ems_employees["Gender"] = gender
iam_ems_employees["birth_date"] = birth_date
iam_ems_employees.drop(["metadata"],1,inplace=True)
iam_ems_employees = iam_ems_employees[['user_id', 'employee_id', 'full_name', 'birth_date', 'Gender', 'employer_id', 'email', 'status', 'phone_number',
'organization_id','document_number','created_at', "lookup_name"]]
d2c = iam_ems_employees[iam_ems_employees["organization_id"]=="916227f6-cb69-46ec-8cb1-a735ed98f2c4"]

uid = d2c["user_id"].unique().tolist()
query = """select * from risk.user_risk_verifications urv; """
cv = dataframe_generator(query)
cv = clean(cv)
cv.rename(columns={"score":"Bureau Approved"
                  },inplace=True)
cv = cv[cv["user_id"].isin(uid)]
cv = cv[['user_id','Bureau Approved','underwriting', 'fraud', 'kyc']]
cv = cv.fillna("API Not Hit/No Value")
cv['Bureau Approved'] = cv['Bureau Approved'].replace({False: 'Rejected', True: 'Approved'})
cv['fraud'] = cv['fraud'].replace({False: 'Rejected', True: 'Approved'})
cv['underwriting'] = cv['underwriting'].replace({False: 'Rejected', True: 'Approved'})
cv["kyc"] = cv["kyc"].replace({False: 'Rejected', True: 'Approved'})
d2c = pd.merge(d2c,cv,on = "user_id", how = "left")
d2c["Bureau Approved"] = d2c["Bureau Approved"].fillna("API Not Hit/No Value")
d2c["fraud"] = d2c["fraud"].fillna("API Not Hit/No Value")
d2c["underwriting"] = d2c["underwriting"].fillna("API Not Hit/No Value")
d2c["kyc"] = d2c["kyc"].fillna("API Not Hit/No Value")
query = """select * from elog.events e;"""
elog = dataframe_generator(query)
categories = elog["action"].value_counts().index.tolist()
risk = []
for x in categories:
    if x.startswith("risk"):
        print (x)
        risk.append(x)
all_d = []
for x in risk:
    d = elog[elog["action"]==x]
    u = []
    a = []
    r = []
    for x in d["body"].tolist():
        u.append(x["userID"])
        a.append(x["action"])
        try:
            r.append(x["reason"])
        except:
            r.append("")
    d1 = pd.DataFrame(u, columns=["user_id"])
    d1["action"] = a[0]
    d1["reason"] = r
    all_d.append(d1)
all_d = pd.concat(all_d)
d2c_phone = d2c[d2c["user_id"].isin(all_d["user_id"].unique().tolist())][["user_id","phone_number" ]]

all_d_phone = pd.merge(all_d,d2c_phone, on = "user_id", how = "left").fillna("No Phone number")
def phone_number_checker(phone_number):
    phone_number = str(phone_number)
    u = d2c[d2c["phone_number"]==phone_number]["user_id"].tolist()[-1]
    m = all_d_phone[all_d_phone["user_id"]==u]
    n = m[m["reason"]!=""][["user_id", "action","reason"]]
    if n.shape[0]>0:
        d3 = n["reason"].unique().tolist()
    else:
        d3 = []
    return d3
print (phone_number_checker(9620126779))
ph = d2c["phone_number"].tolist()
ph_d = []
for x in ph:
    m = {}
    m[x] = phone_number_checker(x)
    ph_d.append(m)
ph_d = {k:v for element in ph_d for k,v in element.items()}
d2c["phone"] = d2c["phone_number"]
d2c["phone"] = d2c["phone"].map(ph_d)
d2c.rename(columns={"phone":"Rejection Reason"},inplace=True)

os.chdir("..")
os.chdir("Outputs")
all_rows_1 = pd.read_csv("all_rows.csv")
os.chdir("..")
os.chdir("Code")

d2c_rows_1 = all_rows_1[all_rows_1["user_id"].isin(d2c["user_id"].tolist())]

d2c_rows_1 = d2c_rows_1[['user_id', 'paused',
       'monthly_salary', 'loan_agreement_number', 'tid',
       'Withdrawn Amount', 'disbursal(txn) date', 'Total Fees', 'Total Amount',
       'Total Fees Calculated', 'processing_fees', 'GST_fees',
       'Annual_income', 'overall_limit']]
d2c_rows_1["disbursal(txn) date"] = pd.to_datetime(d2c_rows_1["disbursal(txn) date"]).dt.date.astype(str)



d2c = pd.merge(d2c,d2c_rows_1, on = "user_id", how = "left")

query = """select * from ems.loan_agreements la ;"""
loan_agreements = dataframe_generator(query)
loan_agreements = clean(loan_agreements)
loan_agreements=loan_agreements[["employee_id", "loan_agreement_number", "expiration_date", "path", "accepted", "accepted_at"]]
loan_agreements["expiration_date"] = pd.to_datetime(loan_agreements["expiration_date"]).dt.date
loan_agreements["today"] = pd.to_datetime("today")
loan_agreements["today"] = loan_agreements["today"].dt.date
loan_agreements["loan_duration"] = loan_agreements["expiration_date"] - loan_agreements["today"]
loan_agreements["loan_Closure_date"] = loan_agreements["expiration_date"]
loan_agreements["accepted_at"] = pd.to_datetime(loan_agreements["accepted_at"]).dt.date
loan_agreements.drop(["expiration_date","today"],1,inplace=True)
loan_agreements.rename(columns={"accepted_date":"loan_agreement_date"},inplace=True)
loan_agreements = loan_agreements[["employee_id", "accepted"]]
loan_agreements.rename(columns={"accepted":"Loan agreement accepted"},inplace=True)
d2c = pd.merge(d2c,loan_agreements, on = "employee_id", how ="left")

print (time.time() - start)

scope = ["https://www.googleapis.com/auth/spreadsheets", "https://www.googleapis.com/auth/drive.file", 
        "https://www.googleapis.com/auth/drive"]
creds = ServiceAccountCredentials.from_json_keyfile_name("cs-and-ops-dashboard-8febbecf58a8.json", scope)
client_2 = gspread.authorize(creds)
employees_kyc_demographic= client_2.open("CS/OPS Dashboard").worksheet("D2C_Funnel_Test")
existing = gd.get_as_dataframe(employees_kyc_demographic)
existing = pd.DataFrame(employees_kyc_demographic.get_all_records())
updated = existing.append(d2c.copy())
gd.set_with_dataframe(employees_kyc_demographic, updated)

scope = ["https://www.googleapis.com/auth/spreadsheets", "https://www.googleapis.com/auth/drive.file", 
        "https://www.googleapis.com/auth/drive"]
creds = ServiceAccountCredentials.from_json_keyfile_name("cs-and-ops-dashboard-8febbecf58a8.json", scope)
client_2 = gspread.authorize(creds)
employees_kyc_demographic= client_2.open("Internal Tracker - Marketing").worksheet("D2C Dump")
employees_kyc_demographic.clear()
existing = gd.get_as_dataframe(employees_kyc_demographic)
existing = pd.DataFrame(employees_kyc_demographic.get_all_records())
updated = existing.append(d2c.copy())
gd.set_with_dataframe(employees_kyc_demographic, updated)
print ("Succesful Final Data Studio Run")

























