#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 27 00:23:30 2021

@author: arunabhmajumdar
"""

import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.express as px
import pandas as pd

df = pd.read_csv("dash_all_rows.csv")
month = df["Month"].unique()

app = dash.Dash(__name__)

app.layout = html.Div([
    dcc.Checklist(
        id="checklist",
        options=[{"label": x, "value": x} 
                 for x in month],
        value=month,
        labelStyle={'display': 'inline-block'}
    ),
    dcc.Graph(id="line-chart"),
    # dcc.Graph(id="bar-chart"),
])

@app.callback(
    Output("line-chart", "figure"), 
    [Input("checklist", "value")])
def update_line_chart(continents):
    mask = df.Month.isin(continents)
    fig = px.line(df[mask], 
        x="Day", y="Withdrawn Amount", color='Month')
    return fig

# @app.callback(
#     Output("bar-chart", "figure"), 
#     [Input("checklist", "value")])
# def update_bar_chart(continents):
#     fig_1 = px.bar(df, x=df["binned_age"].value_counts().index.tolist(), y=df["binned_age"].value_counts().values.tolist())
#     return fig_1

app.run_server(debug=True, port = 8008)