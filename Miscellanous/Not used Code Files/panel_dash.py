#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 12 18:16:02 2022

@author: arunabhmajumdar
"""

import panel as pn
import pandas as pd, os

os.chdir("..")
os.chdir("Outputs")
df = pd.read_csv("all_rows.csv")

os.chdir("..")
os.chdir("Code")

df_pane = pn.pane.DataFrame(df, width=400)

df_pane