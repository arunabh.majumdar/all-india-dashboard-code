#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep  8 09:13:01 2021

@author: arunabhmajumdar
"""




import time
while True:
    time.sleep(300)

    print ("Starting Run")
    import os
    import warnings
    warnings.filterwarnings("ignore")
    import pandas as pd, os
    from oauth2client.service_account import ServiceAccountCredentials
    import gspread
    import gspread_dataframe as gd
    import psycopg2
    import df2gspread as d2g
    import pandas as pd
    import warnings
    warnings.filterwarnings("ignore")
    import numpy as np
    import re
    import calendar
    import os
    import json
    final = {}
    fields = ["Click on Rain Banner inside on Dash App",
    "Click on Product despcription Page",
    "User lands on Rain signup page",
    "Users landing on Password screen(password not yet created)",
    "User is created(Password has been set)",
    "Bureau Pull (Total attempts)"
    # "--Total Attempts",
    # "--Approved from Bureau (Approved, would see the download link to the app)",
    # "--Additional Address Mandatory field missing)",
    # "--No record found",
    # "--Mandatory field missing",
    # "--Invalid Gender Code",
    # "--Wrong State Code",
    # "--Rejected due to low bureau score",
    # "User reaches OTP screen",
    # "User enters correct OTP",
    # "User Downloads App (Installed base) - Once a day",
    # "User Logs into the app",
    # "KYC Stage",
    # "--Selfie",
    # "--Aadhar",
    # "--Pan",
    # "User completes penny drop",
    # "Loan Agreement Accepted",
    # "Success of Withdrawal"
             ]
    # source = ["Quess"]*2+["Rain"]*23
    for x in fields:
        final[x] = ""
    scope = ["https://www.googleapis.com/auth/spreadsheets", "https://www.googleapis.com/auth/drive.file", 
            "https://www.googleapis.com/auth/drive"]
    creds = ServiceAccountCredentials.from_json_keyfile_name("cs-and-ops-dashboard-8febbecf58a8.json", scope)
    client = gspread.authorize(creds)
    quess_landing_page = client.open("CS/OPS Dashboard").worksheet("Quess_Landing_Page_Data")
    existing = gd.get_as_dataframe(quess_landing_page)
    existing = pd.DataFrame(quess_landing_page.get_all_records())
    existing["created_at"] = pd.to_datetime(existing["created_at"]).dt.date.astype(str)
    existing = existing.set_index("created_at")
    existing = existing.loc["2021-09-01":]
    existing["phone_number"] = existing["phone_number"].astype(str)
    phone_number = []
    for x in existing["phone_number"]:
        if len(x)==10:
            phone_number.append(x)
        else:
            phone_number.append(x[2:])
    existing["phone_number"] = phone_number
    existing["phone_number"] = existing["phone_number"].astype(int)
    
    existing_2 = existing.reset_index()
    # Create a cursor to perform database operation
    connection = psycopg2.connect(user="rainadmin",
                                      password="Mudar123",
                                      host="localhost",
                                      port=55432,
                                      database="rain")
    cursor = connection.cursor()
    # Print PostgreSQL details
    print("PostgreSQL server information")
    print(connection.get_dsn_parameters(), "\n")
    
    cursor.execute("SELECT version();")
        # Fetch result
    record = cursor.fetchone()
    print("You are connected to - ", record, "\n")
    def dataframe_generator(query):
        cursor.execute(query)
        print('Read table in PostgreSQL')
        data = cursor.fetchall()
        cols = []
        for elt in cursor.description:
            cols.append(elt[0])
        df= pd.DataFrame(data = data, columns=cols)
        return df
    
    def clean(df):
        df["created_at"] = df["created_at"].dt.date.astype(str)
        df = df[df["created_at"]>"2021-08-31"]
        return df
    query = """select * from elog.events e ;"""
    elog = dataframe_generator(query)
    elog = clean(elog)
    user_id=[]
    for x in elog["body"].tolist():
        if "userID" in x:
            user_id.append(x["userID"])
        else:
            user_id.append(np.nan)
    elog["user_id"] = user_id
    query = """select * from iam.users u ;"""
    iam = dataframe_generator(query)
    iam = clean(iam)
    iam.rename(columns={"id":"user_id"},inplace=True)
    phone_number = iam["phone_number"].astype(str).tolist()
    phone_numbers = []
    for x in phone_number:
        phone_numbers.append(re.sub("[^0-9]", "", x))
    phone_number_2 =[]
    for x in phone_numbers:
        if len(x)>10:
            phone_number_2.append(x[2:])
        else:
            phone_number_2.append(x)
    iam["phone_number"] = phone_number_2
    query = """select * from risk.credit_verification cv ;"""
    cv = dataframe_generator(query)
    cv = clean(cv)
    os.chdir("..")
    os.chdir("AWS_Data/")
    rootdir = os.getcwd()
    files_dump =[]
    for subdir, dirs, files in os.walk(rootdir):
        for file in files:
            print(os.path.join(subdir, file))
            if file.endswith("json"):
                files_dump.append(os.path.join(subdir, file))
    ff = []
    for x in files_dump:
        fff = {}
        f = open(x)
        data = json.load(f)
        fff["user_id"] = str(f).split("/")[-2]
        fff["data"] = data
        ff.append(fff)
    ffff = pd.DataFrame(ff)
    cv = pd.merge(cv,ffff, on = "user_id")
    keys = cv["data"].value_counts().index.tolist()
    values = cv["data"].value_counts().values.tolist()
    ll = pd.DataFrame(dict(zip(keys,values)).items())
    keys_2 = [x.rstrip() for x in keys]
    ll[0] = keys_2
    lll = ll.groupby([0]).sum()
    lll = lll.reset_index()
    keys = lll[0].tolist()
    values = lll[1].tolist()
    inds_ = dict(zip(keys,values))
    inds_["Approved (Will see download link)"] = inds_.pop("Yes")
    inds_["Additional Address Mandatory field missing"] = inds_.pop("SYS100007(Invalid State)/ SYS100005 (Additional Address Mandatory field missing)")
    inds_["Last name Missing"] = inds_.pop("Last name is Missing")
    inds_["Wrong State Code"] = inds_.pop("Wrong State Code")
    inds_["PAN No incorrect format"] = inds_.pop("PAN No is not in Correct Format")
    inds_["No record found"] = inds_.pop("SYS100004  (No record found)")
    inds_["Invalid PIN Code/Invalid Additional Address PIN"] = inds_.pop("SYS100007(Invalid PIN Code/Invalid Additional Address PIN)")
    inds_["New to Bureau"] = inds_.pop("New")
    inds_["Rejected"] = inds_.pop("No")
    inds_["Server Unresponsive"] = inds_.pop("SYS00601 The server did not respond. Please contact Customer Support Helpdesk at +91 (0) 22 6641 9010 for technical assistance.")
    inds_["Mandatory field missing"] = inds_.pop("SYS100005 (Mandatory field missing)")
    final["Bureau Pull (Total attempts)"] = cv["user_id"].nunique()
    final.update(inds_)
    query = """select * from kyc.documents kyc ;"""
    kyc = dataframe_generator(query)
    kyc = clean(kyc)
    
    
    
    query = """select * from bnk.external_accounts ea ;"""
    bnk_external = dataframe_generator(query)
    bnk_external = clean(bnk_external)
    
    
    query = """select * from ems.loan_agreements la ;"""
    la = dataframe_generator(query)
    la = clean(la)
    la = la[1:]
    
    query = """select * from bnk.transactions t ;"""
    txns = dataframe_generator(query)
    txns.rename(columns={"entity_id":"user_id"},inplace=True)
    
    txns = clean(txns)
    txns = txns[txns["user_id"]!="50056340-528d-42c6-8cd3-8b7303f684b1"]
    txns[txns["status"]=="COMPLETE"]["user_id"].nunique()
    final["User lands on Rain signup page"] = existing["phone_number"].unique().shape[0]
    final["User is created(Password has been set)"] = iam.shape[0]
    final["User reaches OTP screen"] = iam.shape[0]
    final["User enters correct OTP"] = iam["status"].value_counts()[0]
    final["KYC Stage"] = kyc["user_id"].nunique()
    final["--Selfie"]=kyc[(kyc["document_type"]=="SILENTLIVENESS")&(kyc["approved"]==True)]["user_id"].nunique()
    final["--Aadhar"]=kyc[(kyc["document_type"]=="AADHAAR")&(kyc["approved"]==True)]["user_id"].nunique()
    final["--Pan"]=kyc[(kyc["document_type"]=="PANNSDL")&(kyc["approved"]==True)]["user_id"].nunique()
    final["User completes penny drop"] =bnk_external[bnk_external["status"]=="ACTIVE"].shape[0]
    final["Loan Agreement Accepted"] = la.shape[0]
    final["Success of Withdrawal"] = txns[txns["status"]=="COMPLETE"]["user_id"].nunique()
    final = pd.DataFrame(final.items())
    final.columns = ["Funnel Stage", "Total till date"]
    def dates_generator_with_key(date):
        existing_new = existing_2[existing_2["created_at"]==date]
        elog_new = elog[elog["created_at"]==date]
        iam_new = iam[iam["created_at"]==date]
        cv_new = cv[cv["created_at"]==date]
        kyc_new = kyc[kyc["created_at"]==date]
        bnk_external_new=bnk_external[bnk_external["created_at"]==date]
        la_new = la[la["created_at"]==date]
        txns_new = txns[txns["created_at"]==date]
        cv_new = pd.merge(cv_new,ffff, on = "user_id")
        cv_new.drop(["data_y"],1,inplace=True)
        cv_new.rename(columns={"data_x":"data"},inplace=True)
        keys_new = cv_new["data"].value_counts().index.tolist()
        values_new = cv_new["data"].value_counts().values.tolist()
        mm = pd.DataFrame(dict(zip(keys_new,values_new)).items())
        keys_2_new = [x.rstrip() for x in keys_new]
        mm[0] = keys_2_new
        mmm = mm.groupby([0]).sum()
        mmm = mmm.reset_index()
        keys_new = mmm[0].tolist()
        values_new = mmm[1].tolist()
        inds_new = dict(zip(keys_new,values_new))
        try:
            inds_new["Approved (Will see download link)"] = inds_new.pop("Yes")
        except:
            inds_new["Approved (Will see download link)"] = ""
        try:
            inds_new["Rejected"] = inds_new.pop("No")
        except:
            inds_new["Rejected"] = ""
        try:
            inds_new["Additional Address Mandatory field missing"] = inds_new.pop("SYS100007(Invalid State)/ SYS100005 (Additional Address Mandatory field missing)")
        except:
            inds_new["Additional Address Mandatory field missing"] = ""
        try:
            inds_new["Last name Missing"] = inds_new.pop("Last name is Missing")
        except:
            inds_new["Last name Missing"] = ""
        try:
            inds_new["Wrong State Code"] = inds_new.pop("Wrong State Code")
        except:
            inds_new["Wrong State Code"] = ""
        try:
            inds_new["PAN No incorrect format"] = inds_new.pop("PAN No is not in Correct Format")
        except:
            inds_new["PAN No incorrect format"] = ""
        try:
            inds_new["No record found"] = inds_new.pop("SYS100004  (No record found)")
        except:
            inds_new["No record found"] = ""
        try:
            inds_new["Invalid PIN Code/Invalid Additional Address PIN"] = inds_new.pop("SYS100007(Invalid PIN Code/Invalid Additional Address PIN)")
        except:
            inds_new["Invalid PIN Code/Invalid Additional Address PIN"] = ""
        try:
            inds_new["New to Bureau"] = inds_new.pop("New")
        except:
            inds_new["New to Bureau"] = ""
        try:
            inds_new["Server Unresponsive"] = inds_new.pop("SYS00601 The server did not respond. Please contact Customer Support Helpdesk at +91 (0) 22 6641 9010 for technical assistance.")
        except:
            inds_new["Server Unresponsive"] = ""
        try:
            inds_new["Mandatory Field Missing"] = inds_new.pop("SYS100005 (Mandatory field missing)")
        except:
            inds_new["Mandatory Field Missing"] = ""
        try:
            inds_new["No Reason/Blank on AWS"] = inds_new.pop('')
        except:
            inds_new["No Reason/Blank on AWS"] = ""
    
        final_new = {}
        fields = ["Click on Rain Banner inside on Dash App",
        "Click on Product despcription Page",
        "User lands on Rain signup page",
        "Users landing on Password screen(password not yet created)",
        "User is created(Password has been set)",
        "Bureau Pull (Total attempts)"]
        for x in fields:
            final_new[x] = ""
        final_new["Bureau Pull (Total attempts)"] = cv_new["user_id"].nunique()
        final_new.update(inds_new)
        final_new["User lands on Rain signup page"] = existing_new["phone_number"].unique().shape[0]
        final_new["User is created(Password has been set)"] = iam_new.shape[0]
        final_new["User reaches OTP screen"] = iam_new.shape[0]
        final_new["User enters correct OTP"] = iam_new["status"].value_counts()[0]
        final_new["KYC Stage"] = kyc_new["user_id"].nunique()
        final_new["--Selfie"]=kyc_new[(kyc_new["document_type"]=="SILENTLIVENESS")&(kyc_new["approved"]==True)]["user_id"].nunique()
        final_new["--Aadhar"]=kyc_new[(kyc_new["document_type"]=="AADHAAR")&(kyc_new["approved"]==True)]["user_id"].nunique()
        final_new["--Pan"]=kyc_new[(kyc_new["document_type"]=="PANNSDL")&(kyc_new["approved"]==True)]["user_id"].nunique()
        final_new["User completes penny drop"] =bnk_external_new[bnk_external_new["status"]=="ACTIVE"].shape[0]
        final_new["Loan Agreement Accepted"] = la_new.shape[0]
        final_new["Success of Withdrawal"] = txns_new[txns_new["status"]=="COMPLETE"]["user_id"].nunique()
        final_new_final = pd.DataFrame(final_new.items())
        final_new_final.columns = ["Funnel Stage", str(date)]
    #     final_new_final.drop(["Funnel Stage"],1,inplace = True)
        return final_new_final
    def dates_generator(date):
        existing_new = existing_2[existing_2["created_at"]==date]
        elog_new = elog[elog["created_at"]==date]
        iam_new = iam[iam["created_at"]==date]
        cv_new = cv[cv["created_at"]==date]
        kyc_new = kyc[kyc["created_at"]==date]
        bnk_external_new=bnk_external[bnk_external["created_at"]==date]
        la_new = la[la["created_at"]==date]
        txns_new = txns[txns["created_at"]==date]
        cv_new = pd.merge(cv_new,ffff, on = "user_id")
        cv_new.drop(["data_y"],1,inplace=True)
        cv_new.rename(columns={"data_x":"data"},inplace=True)
        keys_new = cv_new["data"].value_counts().index.tolist()
        values_new = cv_new["data"].value_counts().values.tolist()
        mm = pd.DataFrame(dict(zip(keys_new,values_new)).items())
        keys_2_new = [x.rstrip() for x in keys_new]
        mm[0] = keys_2_new
        mmm = mm.groupby([0]).sum()
        mmm = mmm.reset_index()
        keys_new = mmm[0].tolist()
        values_new = mmm[1].tolist()
        inds_new = dict(zip(keys_new,values_new))
        try:
            inds_new["Approved (Will see download link)"] = inds_new.pop("Yes")
        except:
            inds_new["Approved (Will see download link)"] = ""
        try:
            inds_new["Rejected"] = inds_new.pop("No")
        except:
            inds_new["Rejected"] = ""
        try:
            inds_new["Additional Address Mandatory field missing"] = inds_new.pop("SYS100007(Invalid State)/ SYS100005 (Additional Address Mandatory field missing)")
        except:
            inds_new["Additional Address Mandatory field missing"] = ""
        try:
            inds_new["Last name Missing"] = inds_new.pop("Last name is Missing")
        except:
            inds_new["Last name Missing"] = ""
        try:
            inds_new["Wrong State Code"] = inds_new.pop("Wrong State Code")
        except:
            inds_new["Wrong State Code"] = ""
        try:
            inds_new["PAN No incorrect format"] = inds_new.pop("PAN No is not in Correct Format")
        except:
            inds_new["PAN No incorrect format"] = ""
        try:
            inds_new["No record found"] = inds_new.pop("SYS100004  (No record found)")
        except:
            inds_new["No record found"] = ""
        try:
            inds_new["Invalid PIN Code/Invalid Additional Address PIN"] = inds_new.pop("SYS100007(Invalid PIN Code/Invalid Additional Address PIN)")
        except:
            inds_new["Invalid PIN Code/Invalid Additional Address PIN"] = ""
        try:
            inds_new["New to Bureau"] = inds_new.pop("New")
        except:
            inds_new["New to Bureau"] = ""
        try:
            inds_new["Server Unresponsive"] = inds_new.pop("SYS00601 The server did not respond. Please contact Customer Support Helpdesk at +91 (0) 22 6641 9010 for technical assistance.")
        except:
            inds_new["Server Unresponsive"] = ""
        try:
            inds_new["Mandatory Field Missing"] = inds_new.pop("SYS100005 (Mandatory field missing)")
        except:
            inds_new["Mandatory Field Missing"] = ""
        try:
            inds_new["No Reason/Blank on AWS"] = inds_new.pop('')
        except:
            inds_new["No Reason/Blank on AWS"] = ""
    
        final_new = {}
        fields = ["Click on Rain Banner inside on Dash App",
        "Click on Product despcription Page",
        "User lands on Rain signup page",
        "Users landing on Password screen(password not yet created)",
        "User is created(Password has been set)",
        "Bureau Pull (Total attempts)"]
        for x in fields:
            final_new[x] = ""
        final_new["Bureau Pull (Total attempts)"] = cv_new["user_id"].nunique()
        final_new.update(inds_new)
        final_new["User lands on Rain signup page"] = existing_new["phone_number"].unique().shape[0]
        final_new["User is created(Password has been set)"] = iam_new.shape[0]
        final_new["User reaches OTP screen"] = iam_new.shape[0]
        final_new["User enters correct OTP"] = iam_new["status"].value_counts()[0]
        final_new["KYC Stage"] = kyc_new["user_id"].nunique()
        final_new["--Selfie"]=kyc_new[(kyc_new["document_type"]=="SILENTLIVENESS")&(kyc_new["approved"]==True)]["user_id"].nunique()
        final_new["--Aadhar"]=kyc_new[(kyc_new["document_type"]=="AADHAAR")&(kyc_new["approved"]==True)]["user_id"].nunique()
        final_new["--Pan"]=kyc_new[(kyc_new["document_type"]=="PANNSDL")&(kyc_new["approved"]==True)]["user_id"].nunique()
        final_new["User completes penny drop"] =bnk_external_new[bnk_external_new["status"]=="ACTIVE"].shape[0]
        final_new["Loan Agreement Accepted"] = la_new.shape[0]
        final_new["Success of Withdrawal"] = txns_new[txns_new["status"]=="COMPLETE"]["user_id"].nunique()
        final_new_final = pd.DataFrame(final_new.items())
        final_new_final.columns = ["Funnel Stage", str(date)]
        final_new_final.drop(["Funnel Stage"],1,inplace = True)
        return final_new_final
    f_test = dates_generator_with_key("2021-09-01")
    l = f_test["Funnel Stage"].tolist()
    dates = ["2021-09-01","2021-09-02", "2021-09-03", "2021-09-04", "2021-09-05", "2021-09-06", "2021-09-07", "2021-09-08", 
            "2021-09-09", "2021-09-10", "2021-09-11", "2021-09-12", "2021-09-13", "2021-09-14"]
    
    all_others = []
    for x in dates:
        all_others.append(dates_generator(x))
    test = pd.concat(all_others,1)
    test["Funnel Stage"] = l
    test_2 = pd.merge(final, test, on= "Funnel Stage")
    test_2.to_csv("Funnel.csv")
    df = pd.read_csv('Funnel.csv')
    df.drop(["Unnamed: 0"],1,inplace=True)
    df = df.fillna("")
    funnel = client.open("CS/OPS Dashboard").worksheet("Funnel Data")
    funnel.clear()
    existing = gd.get_as_dataframe(funnel)
    existing = pd.DataFrame(funnel.get_all_records())
    updated = existing.append(df)
    gd.set_with_dataframe(funnel,updated)
        
    #Withdrawals
    query = """select * from ems.employees e ;"""
    ems_employees = dataframe_generator(query)
    ems_employees = ems_employees[['id', 'user_id', 'employer_id', 'status', 'first_name', 'last_name',
            'created_at', 'updated_at', 'group_id', 'hr_employee_code', 'organization_id']]
    ems_employees = ems_employees.rename(columns={'id': 'employee_id'})
    ems_employees= clean(ems_employees)
    
    query = """select * from iam.users u ;"""
    iam_users = dataframe_generator(query)
    iam_users = iam_users.rename(columns={'id': 'user_id'})
    iam_users = clean(iam_users)
    iam_users = iam_users[["user_id", "full_name", "email","phone_number", "metadata","document_number"]]
    phone_number = iam_users["phone_number"].astype(str).tolist()
    phone_numbers = []
    for x in phone_number:
        phone_numbers.append(re.sub("[^0-9]", "", x))
    phone_number_2 =[]
    for x in phone_numbers:
        if len(x)>10:
            phone_number_2.append(x[2:])
        else:
            phone_number_2.append(x)
    iam_users["phone_number"] = phone_number_2
    iam_ems_employees = pd.merge(ems_employees,iam_users, on = "user_id")
    
    iam_ems_employees = iam_ems_employees.sort_values("created_at")
    
    
    query = """select * from kyc.documents d ;"""
    kyc = dataframe_generator(query) 
    kyc = clean(kyc)
    kyc = kyc[["user_id","file_extension", "document_type", "side", "verified", "approved"]]
    kyc = kyc[kyc["approved"]==True]
    
    
    kyc_users = kyc["user_id"].unique().tolist()
    uid = []
    for x in kyc_users:
        if len(kyc[kyc["user_id"]==x]["document_type"].value_counts().tolist())>3:
            uid.append(x)
            
            
    kyc = kyc[kyc["user_id"].isin(uid)]
    
    
    
    
    
    
    iam_ems_kyc_employees = pd.merge(iam_ems_employees, kyc, on = "user_id")
    gender=[]
    birth_date=[]
    for i in range(0,iam_ems_kyc_employees.shape[0]):
        gender.append(iam_ems_kyc_employees["metadata"].iloc[i]["gender"])
        birth_date.append(iam_ems_kyc_employees["metadata"].iloc[i]["birth_date"])
        
    iam_ems_kyc_employees["Gender"] = gender
    iam_ems_kyc_employees["birth_date"] = birth_date
    iam_ems_kyc_employees.drop(["metadata"],1,inplace=True)
    iam_ems_kyc_employees = iam_ems_kyc_employees[['user_id', 'employee_id', 'full_name', 'birth_date', 'Gender', 'employer_id', 'email', 'status', 'phone_number',
    'organization_id','document_type', 'side','document_number', 'file_extension', 'verified', 'approved']]
    
    query = """select * from ems.compensations c ;"""
    compensations = dataframe_generator(query)
    compensations = clean(compensations)
    compensations = compensations[["employee_id", "monthly_salary"]]
    compensations["monthly_salary"] = compensations["monthly_salary"]/100
    rows_1_39 = pd.merge(iam_ems_kyc_employees,compensations, on = "employee_id")
    
    query = """select * from ems.loan_agreements la ;"""
    loan_agreements = dataframe_generator(query)
    loan_agreements = clean(loan_agreements)
    loan_agreements=loan_agreements[["employee_id", "loan_agreement_number", "expiration_date", "path", "accepted", "accepted_at"]]
    loan_agreements["expiration_date"] = pd.to_datetime(loan_agreements["expiration_date"]).dt.date
    loan_agreements["today"] = pd.to_datetime("today")
    loan_agreements["today"] = loan_agreements["today"].dt.date
    loan_agreements["loan_duration"] = loan_agreements["expiration_date"] - loan_agreements["today"]
    loan_agreements["loan_Closure_date"] = loan_agreements["expiration_date"]
    loan_agreements["accepted_at"] = pd.to_datetime(loan_agreements["accepted_at"]).dt.date
    loan_agreements.drop(["expiration_date","today"],1,inplace=True)
    loan_agreements.rename(columns={"accepted_date":"loan_agreement_date"},inplace=True)
    all_rows = pd.merge(rows_1_39,loan_agreements,on = "employee_id")
    
    query = """select * from bnk.transactions t  ;"""
    txns = dataframe_generator(query)
    txns = clean(txns)
    txns.rename(columns={"entity_id":"user_id"},inplace=True)
    txns.rename(columns={"amount":"Withdrawn Amount"}, inplace=True)
    txns.rename(columns={"created_at":"disbursal(txn) date"},inplace=True)
    txns = txns[txns["status"]=="COMPLETE"]
    txns = txns[["id","user_id", "Withdrawn Amount","disbursal(txn) date"]]
    txns["Withdrawn Amount"] = txns["Withdrawn Amount"]/100
    txns.rename(columns={"id":"tid"},inplace=True)
    
    all_rows = pd.merge(all_rows, txns, on = "user_id")
    withdrawn_amounts=all_rows["Withdrawn Amount"].tolist()
    total_fees = []
    for x in withdrawn_amounts:
        if 50<x<301:
            total_fees.append(12)
        elif 300<x<501:
            total_fees.append(18)
        elif 500<x<1001:
            total_fees.append(35)
        elif 1000<x<1501:
            total_fees.append(60)
        elif 1500<x<2501:
            total_fees.append(100)
        elif 2500<x<5001:
            total_fees.append(200)
        elif 5000<x<10001:
            total_fees.append(275)
        elif 10000<x<25001:
            total_fees.append(700)
        elif 25000<x<50001:
            total_fees.append(800)
        elif 50000<x<100001:
            total_fees.append(1500)
        else:
            total_fees.append(0)
    all_rows["total_fees"] = total_fees
    all_rows["processing_fees"] = round(all_rows["total_fees"]/1.18,2)
    all_rows["GST_fees"] = all_rows["total_fees"] - all_rows["processing_fees"]
    all_rows.drop(["total_fees"],1,inplace = True)
    all_rows["Annual_income"] = (all_rows["monthly_salary"]*12).astype(float)
    all_rows["Annual_income"] = all_rows["Annual_income"].astype(str)
    all_rows["Total Amount"] = all_rows["Withdrawn Amount"]+all_rows["GST_fees"]+all_rows["processing_fees"]
    query = """select * from ems.work_period_balances wpb ;"""
    wpb = dataframe_generator(query)
    wpb = clean(wpb)
    wpb = wpb[["employee_id", "available_amount", "payment_date"]]
    wpb.rename(columns={"available_amount":"overall_limit", "payment_date":"next_payment_date"},inplace=True)
    wpb["next_payment_date"] = pd.to_datetime(wpb["next_payment_date"]).dt.date
    wpb["overall_limit"]  = wpb["overall_limit"]/100
    all_rows = pd.merge(all_rows, wpb, on = "employee_id")
    all_rows["disbursal(txn) date"] = pd.to_datetime(all_rows["disbursal(txn) date"])
    all_rows = all_rows.sort_values("disbursal(txn) date", ascending=True)
    all_rows = all_rows.drop_duplicates(["tid"])
    
    
    
    #encryption
    import base64
    import boto3
    key_id = "be2bdea3-4ed9-48f2-8123-467fd62292fa"
    def hello_kms_bank(encrypted_text):
        session = boto3.session.Session(profile_name="rain-india-production")
        client = session.client("kms")
        encrypted_text = encrypted_text
        decrypted = client.decrypt(CiphertextBlob=base64.b64decode(encrypted_text))
        decrypted_text = decrypted["Plaintext"].decode("utf-8").split("\x1d")
        d = {}
        for c in range(len(decrypted_text)):
            d[decrypted_text[c].split("::")[0]] = decrypted_text[c].split("::")[1]
        dd = pd.DataFrame(d.items()).T
        dd.columns = dd.iloc[0]
        dd = dd[1:]
        return dd
    def hello_kms_address(encrypted_text):
        session = boto3.session.Session(profile_name="rain-india-production")
        client = session.client("kms")
        encrypted_text = encrypted_text
        decrypted = client.decrypt(CiphertextBlob=base64.b64decode(encrypted_text))
        decrypted_text = decrypted["Plaintext"].decode("utf-8").split("\x1d")
        return decrypted_text
    
    query = """select * from bnk.external_accounts ea ;"""
    external_accounts = dataframe_generator(query)
    external_accounts = clean(external_accounts)
    external_accounts = external_accounts[external_accounts["user_id"].isin(all_rows["user_id"].unique().tolist())]
    account_details = external_accounts["account_details"].tolist()
    decrypted_account_details = []
    count=0
    for x in account_details:
        decrypted_account_details.append(hello_kms_bank(x))
        count+=1
        print (count)
    decrypted_account_details_df = pd.concat(decrypted_account_details)
    acc_holder_name = decrypted_account_details_df["accountHolderName"].tolist()
    bank_acc_number = decrypted_account_details_df["bankAccountNumber"].tolist()
    bank_acc_number= ["'"+str(x) for x in bank_acc_number]
    bank_ifsc= decrypted_account_details_df["bankRoutingNumber"].tolist()
    external_accounts["accountHolderName"] = acc_holder_name
    external_accounts["bankAccountNumber"] = bank_acc_number
    external_accounts["bankIFSCNumber"] = bank_ifsc
    external_accounts = external_accounts[["user_id","accountHolderName", "bankAccountNumber","bankIFSCNumber"]]
    all_rows = pd.merge(all_rows, external_accounts, on = "user_id")
    
    query = """select * from ems.addresses a ;"""
    addresses = dataframe_generator(query)
    addresses= clean(addresses)
    addresses = addresses[addresses['employee_id'].isin(all_rows['employee_id'].unique().tolist())]
    addresses_encoded_data = addresses["encoded_data"].tolist()
    
    decrypted_address=[]
    count = 0
    for x in addresses_encoded_data:
        result = hello_kms_address(x)
        decrypted_address.append(result)
        count+=1
        print (count)
        
        
        
    addresses["Address"] = decrypted_address
    addresses = addresses[["employee_id", "Address"]]
    all_rows = pd.merge(all_rows, addresses, on = "employee_id")
    
    
    
    
    
    
    
    
    
    
    
    os.chdir("..")
    os.chdir("Outputs")
    all_rows.to_csv("all_rows.csv")
    os.chdir("..")
    os.chdir("Code")
    scope = ["https://www.googleapis.com/auth/spreadsheets", "https://www.googleapis.com/auth/drive.file", 
            "https://www.googleapis.com/auth/drive"]
    creds = ServiceAccountCredentials.from_json_keyfile_name("cs-and-ops-dashboard-8febbecf58a8.json", scope)
    client = gspread.authorize(creds)
    
    os.chdir("..")
    os.chdir("Outputs")
    df = all_rows.copy()
    employees_kyc_demographic= client.open("CS/OPS Dashboard").worksheet("All_withdrawals")
    employees_kyc_demographic.clear()
    existing = gd.get_as_dataframe(employees_kyc_demographic)
    existing = pd.DataFrame(employees_kyc_demographic.get_all_records())
    updated = existing.append(df)
    gd.set_with_dataframe(employees_kyc_demographic, updated)
    
    os.chdir("..")
    os.chdir("Code")










