#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb  6 12:51:20 2022

@author: arunabhmajumdar
"""
print ("Importing all packages and Google BQ credentials files")
import datetime
import warnings
warnings.filterwarnings("ignore")
import pandas as pd,os
import base64
import boto3
import time
import pandas as pd, os
from oauth2client.service_account import ServiceAccountCredentials
import gspread
import gspread_dataframe as gd
import psycopg2
import df2gspread as d2g
import pandas as pd
import warnings
warnings.filterwarnings("ignore")
import numpy as np
import re
import calendar
import os
import json
import time
print ("Changing directory to Code to aid ease of access to the various jsons and xlsx")
print(os.getcwd())
os.chdir("..")
time.sleep(2)
print ("Changed Directory")
print (os.getcwd())
start_1 = time.time()
import numpy as np
import pytz
my_timezone = pytz.timezone('Asia/Calcutta')
import ast
import gzip
from datetime import timedelta
from currency_converter import CurrencyConverter
from google.oauth2 import service_account
from google.cloud import bigquery
import pandas_gbq
import os, pandas as pd
os.getcwd()
KEY_PATH = "data-warehouse-india-84f5f8a775d1.json"
CREDS = service_account.Credentials.from_service_account_file(KEY_PATH)
bq_client = bigquery.Client(credentials=CREDS, project="data-warehouse-india")
import warnings
warnings.filterwarnings("ignore")


print ("Connecting to Dynamo DB using boto3")
start = time.time()
print ("starting run")
session = boto3.session.Session(profile_name="rain-india-prod")
client = session.client("dynamodb")
dynamodb = boto3.resource("dynamodb")
print ("Connecting to Postgres using psycopg2")
connection = psycopg2.connect(user="rainadmin",
                                      password="Mudar123",
                                      host="localhost",
                                      port=55432,
                                      database="rain")
cursor = connection.cursor()
# Print PostgreSQL details
print("PostgreSQL server information")
print(connection.get_dsn_parameters(), "\n")
# cursor.itersize = 10000
cursor.execute("SELECT version();")
    # Fetch result
record = cursor.fetchone()
print("You are connected to - ", record, "\n")

print ("Function to download Postgres data and concert that to dataframe")
def dataframe_generator(query):
    cursor.execute(query)
    print('Read table in PostgreSQL')
    data = cursor.fetchall()
    cols = []
    for elt in cursor.description:
        cols.append(elt[0])
    df= pd.DataFrame(data = data, columns=cols)
    return df

print ("Function to clean dataframe to include only data post Sept 1, 2021")
def clean(df):
    df["created_at"] = df["created_at"].dt.date.astype(str)
    df = df[df["created_at"]>"2021-08-31"]
    return df
print (time.time() - start)


print ("Function to clean dataframe to upload to BQ, BQ doesn't take spaces, dashes etc;")
def bq_cleaner(df):
    new_cols = []
    l = df.columns.tolist()
    for x in l:
        x = x.replace("(","_")
        x = x.replace(" ","_")
        x = x.replace(")","_")
        new_cols.append(x)
    df.columns = new_cols
    df = df.astype(str)
    return df











print ("Module 1 - Quess Landing Page")
print ("Downloading data dump from AWS_QUESS_LANDING_PAGE S3 dump from the most recent folder called json")
print (os.getcwd())
print ("ok")
#     time.sleep(3)
print ("Dynamo Update Landing Page")
 # time.sleep(60)
print ("starting run")
# session = boto3.session.Session(profile_name="rain-india-prod")
# client = session.client("dynamodb")
# dynamodb = boto3.resource("dynamodb")
# # table = dynamodb.Table("QuessLandingDropoff")
# response = client.scan(TableName = "QuessLandingDropoff")
# os.getcwd()


print ("Changing directory to json")
os.chdir("..")
os.chdir("AWS_Quess_Landing_Page")
os.chdir("AWSDynamoDB/")
files = [x for x in os.listdir(os.getcwd())]
newest = max(files , key = os.path.getctime)
print ("Recently modified Docs",newest)
os.chdir(newest)
# os.chdir("json")
os.chdir("data")


all_files = []
for x in os.listdir():
    print (x)
    f=gzip.open(x,'r')
    file_content=f.read()
    all_files.append(file_content)
json_strings = []
for x in all_files:
    my_json = x.decode('utf-8').replace("'",'"')
    json_strings.append(my_json)
all_cleaned =[]
for x in json_strings:
    all_cleaned.append(x.split("\n"))
c = []
c1 = []
c2 = []
c3 = []
try:
    for x in all_cleaned[0]:
        c.append(ast.literal_eval(x))
except:
    pass
try:
    for x in all_cleaned[1]:
        c1.append(ast.literal_eval(x))
except:
    pass
try:
    for x in all_cleaned[2]:
        c2.append(ast.literal_eval(x))
except:
    pass
try:
    for x in all_cleaned[3]:
        c3.append(ast.literal_eval(x))
except:
    pass
c = c+c1+c2+c3
employee_id = []
updated_at = []
created_at = []
phone_number = []
status = []
for x in c:
    employee_id.append(x["Item"]["employee_id"])
    updated_at.append(x["Item"]["updated_at"])
    phone_number.append(x["Item"]["phone_number"])
    created_at.append(x["Item"]["created_at"])
    status.append(x["Item"]["status"])
df = pd.DataFrame()
df["employee_id"] = employee_id
df["phone_number"] = phone_number
df["status"] = status
df["created_at"] = created_at
df["updated_at"] = updated_at
employee_id = []
phone_number = []
status = []
created_at = []
updated_at = []
for x in range(df.shape[0]):
    employee_id.append(df["employee_id"].iloc[x]["S"])
    phone_number.append(df["phone_number"].iloc[x]["S"])
    status.append(df["status"].iloc[x]["S"])
    created_at.append(df["created_at"].iloc[x]["S"])
    updated_at.append(df["updated_at"].iloc[x]["S"])
print (len(employee_id))
print (len(phone_number))
print (len(status))
print (len(created_at))
print (len(updated_at))

df["employee_id"] = employee_id
df["phone_number"] = phone_number
df["status"] = status
df["created_at"] = created_at
df["updated_at"] = updated_at



df = df.sort_values("created_at")


df["organization_id"]="Quess"
os.chdir("..")
os.chdir("..")
os.chdir("..")
os.chdir("..")
os.chdir("Code/")





print ("Uploading Landing page data to 2 Google Sheets - CS-OPS (New Quess Landing Page "+
        "and Data Studio Final Dashboard Sheet 3 ")


existing = df.copy()
existing["created_at"] = pd.to_datetime(existing["created_at"]).dt.date
landing_page = existing.copy()
print (time.time() - start)


print ("Uploading Landing page data to BQ")
landing_page_bq = bq_cleaner(df)
pandas_gbq.to_gbq(landing_page_bq, destination_table="Processed_data.Quess_Landing_Page", project_id="data-warehouse-india", if_exists="replace")


# print ("Module 2 - Repayments")
# print ("Changing directory to Google drive repayments for today's date")
# print (os.getcwd())
# os.chdir("..")
# os.chdir("..")
# os.chdir("..")
# print (os.getcwd())

# os.chdir("Google Drive/")
# os.chdir("Shared drives/")
# os.chdir("Repayments/")


# print ("Seperating Repayments Month wise from Excel")
# from datetime import date
# month_string = pd.to_datetime("today").strftime("%b")
# os.chdir(month_string)
# today = date.today()
# # yest = today-timedelta(days =3)
# print("Today's date:", today)
# today = today.strftime("%d-%b-%Y")

# os.chdir(today)
# print (os.getcwd())
# # pd.read_excel(os.listdir()[0])
# total_refunds_2_oct= pd.read_excel((os.listdir()[0]), sheet_name="Oct")
# total_refunds_2_nov= pd.read_excel((os.listdir()[0]), sheet_name="Nov")
# total_refunds_2_dec= pd.read_excel((os.listdir()[0]), sheet_name="Dec")
# total_refunds_2_jan= pd.read_excel((os.listdir()[0]), sheet_name="Jan")
# total_refunds_2_feb= pd.read_excel((os.listdir()[0]), sheet_name="Feb")
# total_refunds_2_mar= pd.read_excel((os.listdir()[0]), sheet_name="Mar")
# total_refunds_2_apr= pd.read_excel((os.listdir()[0]), sheet_name="Apr")
# total_refunds_2_may= pd.read_excel((os.listdir()[0]), sheet_name="May")
# total_refunds_2 = pd.concat([total_refunds_2_oct,total_refunds_2_nov, total_refunds_2_dec, total_refunds_2_jan, 
#                             total_refunds_2_feb, total_refunds_2_mar, total_refunds_2_apr, 
#                             total_refunds_2_may])
# total_refunds_2.rename(columns={"Due date":"created at"},inplace=True)
# total_refunds_2.drop(["full_name"],1,inplace=True)
# total_refunds = total_refunds_2.copy()
# total_refunds["created at"] = total_refunds["created at"].astype(str)
# print (os.getcwd())
# os.chdir("/Users/arunabhmajumdar/Documents/all-india-dashboard-code/Code")

# print ("Repayment cleaning to group by user_id")
# def group_clean(df, date):
#     df1 = df[["user_id", "repayment"]]
#     df1 = df1.groupby("user_id").sum().reset_index()
#     df2 = df[["user_id", "Due date"]]
#     df2 = df2.groupby("user_id").last().reset_index()
#     df = pd.merge(df1,df2, on = "user_id")
#     df["disbursal(txn) date"] = df["Due date"]
#     df.drop(["Due date"],1,inplace = True)
#     df["disbursal(txn) date"] = date
#     return df



# sep_repayment = group_clean(total_refunds_2_oct, "2021-09-30")
# oct_repayment = group_clean(total_refunds_2_nov, "2021-10-31")
# nov_repayment = group_clean(total_refunds_2_dec, "2021-11-30")
# dec_repayment = group_clean(total_refunds_2_dec, "2021-12-31")
# jan_repayment = group_clean(total_refunds_2_jan, "2022-01-31")
# feb_repayment = group_clean(total_refunds_2_feb, "2022-02-28")
# mar_repayment = group_clean(total_refunds_2_mar, "2022-03-31")
# apr_repayment = group_clean(total_refunds_2_mar, "2022-04-30")


# tr = pd.concat([sep_repayment, oct_repayment,nov_repayment, dec_repayment, jan_repayment, feb_repayment, mar_repayment, 
#                 apr_repayment])

# print ("Converting amounts to dollars - this is just to give metrics in dollars for the US team")
# c = CurrencyConverter()
# dollars = []
# rep = tr["repayment"].tolist()
# for x in rep:
#     dollars.append(c.convert(x,"INR","USD"))
# tr["In Dollars"] = dollars



print ("getting the all txns csv all_rows.csv from Outputs")
os.chdir("..")
os.chdir("Outputs")

all_rows_1 = pd.read_csv("all_rows.csv")

all_rows_1.drop(["lookup_name"],1,inplace=True)

print ("Grouping the txns gender wise, salary wise, organizationwise, age wise, "+
       "to fuel the all withdrawals sheet on the dashboard")

start = time.time()
query = """select * from xorg.employers e;"""
xorg = dataframe_generator(query)
xorg = xorg[["id","organization_id", "lookup_name"]]
xorg["lookup_name"] = xorg["lookup_name"].str.lower()
xorg.rename(columns={"id":"employer_id"},inplace=True)
print (time.time()-start)

xorg = xorg[["employer_id", "lookup_name"]]

all_rows_1 = pd.merge(all_rows_1, xorg, on = "employer_id", how = "left")

all_rows_1["disbursal(txn) date"] = pd.to_datetime(all_rows_1["disbursal(txn) date"]).dt.date.astype(str)
all_rows_1= all_rows_1[['user_id','birth_date', "email", 'Gender','organization_id','monthly_salary',
           'tid','disbursal(txn) date','Total Amount', 'processing_fees', 
       'overall_limit', 'Sanctioned Loan Limit', "lookup_name"]]
all_rows_1["organization_id"].replace({"c2a6a007-e625-456f-8c36-92cd2654c971": 'Quess',
                                              "916227f6-cb69-46ec-8cb1-a735ed98f2c4": 'D2C Org', 
                                             "d779558a-09cc-4920-9f39-d8409c8f0728":"B2B Test", 
                                             "0706c2e4-5153-4429-9645-a6d0c4a26a04":"HCL", 
                                    "6740ddac-09fa-4c0b-9cad-a261cc23997e":"Cloudnine", 
                                    "2f57a3bb-26a6-4cd6-953e-ce61fe607e6a":"Wissen Infotech"},inplace=True)

gender_dict = {'Male':"male", 'MALE':"male", 'male':"male", 'Female':"female", 'M':"male", ' MALE ':"male", 
               'F':"female", 'female':"female"}

all_rows_1["Gender"] = all_rows_1["Gender"].map(gender_dict)

all_rows_1["age"] = (pd.to_datetime("today") - pd.to_datetime(all_rows_1["birth_date"])).dt.days

all_rows_1["age"] = round(all_rows_1["age"]/365,0)

bins = [0, 20, 25, 30, 35, 40, 50, 60,100]
labels = ["under 20","20-25","25-30","30-35","35-40","40-50", "50-60", "60+"]
all_rows_1['binned_age'] = pd.cut(all_rows_1['age'], bins=bins, labels=labels)

bins = [0, 15000, 30000, 50000, 80000, 100000, 500000]
labels = ["under 15k","15k-30k","30k-50k","50k-80k","80k-100k","100k+"]
all_rows_1['binned_salary'] = pd.cut(all_rows_1['monthly_salary'], bins=bins, labels=labels)

os.chdir("..")

os.chdir("Code")
print (os.getcwd())

# tr = pd.merge(tr, all_rows_1[["user_id", "organization_id"]].groupby("user_id").last().reset_index(), on = "user_id")

all_rows_1 = all_rows_1[(all_rows_1["lookup_name"]!="quees corp ltd")&(all_rows_1["lookup_name"]!="quess corp limited")&(all_rows_1["lookup_name"]!="rainpay")]

mn = pd.to_datetime(all_rows_1["disbursal(txn) date"]).dt.month.tolist()
month_name = []
for x in mn:
    month_name.append(calendar.month_name[x])
print (len(month_name))
all_rows_1["Disbursal_month"] = month_name
all_rows_1.shape

c = CurrencyConverter()
dollars_tot_amount = []
tot = all_rows_1["Total Amount"].tolist()
for x in tot:
    dollars_tot_amount.append(c.convert(x,"INR","USD"))
dollars_processing = []
pro = all_rows_1["processing_fees"].tolist()
for y in pro:
    dollars_processing.append(c.convert(y,"INR", "USD"))
all_rows_1["Total amount in dollars"] = dollars_tot_amount
all_rows_1["Processing fees in dollars"] = dollars_processing

a = pd.to_datetime(all_rows_1["disbursal(txn) date"]).dt.month.tolist()
c1 = []
for m in a:
    c1.append(calendar.month_name[m])
all_rows_1["month"] = c1



# print ("Uploading MTD data to BQ")


# months = all_rows_1["month"].unique().tolist()
# users_mtd_overall = []
# users_mtd_quess = []
# users_mtd_d2c = []
# for x in months:
#     u = all_rows_1[all_rows_1["month"]==x]
#     users_mtd_overall.append(u["user_id"].nunique())
#     users_mtd_quess.append(u[u["organization_id"]=="Quess"]["user_id"].nunique())
#     users_mtd_d2c.append(u[u["organization_id"]=="D2C Org"]["user_id"].nunique())
    
# mtd = pd.DataFrame(months)
# mtd["Users_overall"] = users_mtd_overall
# mtd["MoM % Change in Users_overall"] = mtd["Users_overall"].pct_change()*100
# mtd["Users_quess"] = users_mtd_quess
# mtd["MoM % Change in Users_quess"] = mtd["Users_quess"].pct_change()*100
# mtd["Users_D2C"] = users_mtd_d2c
# mtd["MoM % Change in Users_D2C"] = mtd["Users_D2C"].pct_change()*100
# mtd.replace([np.inf, -np.inf], np.nan,inplace=True)
# mtd = round(mtd,2)
# mtd = mtd.fillna("")
# mtd = mtd.T
# mtd.columns = months
# mtd = mtd.reset_index()
# mtd = mtd[1:]

# months = all_rows_1["month"].unique().tolist()
# revenue_mtd_overall = []
# revenue_mtd_quess = []
# revenue_mtd_d2c = []
# for x in months:
#     u = all_rows_1[all_rows_1["month"]==x]
#     revenue_mtd_overall.append(u["processing_fees"].sum())
#     revenue_mtd_quess.append(u[u["organization_id"]=="Quess"]["processing_fees"].sum())
#     revenue_mtd_d2c.append(u[u["organization_id"]=="D2C Org"]["processing_fees"].sum())
    
# revenue = pd.DataFrame(months)
# revenue["Revenue_overall"] = revenue_mtd_overall
# revenue["MoM % Change in Revenue_overall"] = revenue["Revenue_overall"].pct_change()*100
# revenue["Revenue_quess"] = revenue_mtd_quess
# revenue["MoM % Change in Revenue_quess"] = revenue["Revenue_quess"].pct_change()*100
# revenue["Revenue_D2C"] = revenue_mtd_d2c
# revenue["MoM % Change in Revenue_D2C"] = revenue["Revenue_D2C"].pct_change()*100
# revenue.replace([np.inf, -np.inf], np.nan,inplace=True)
# revenue = round(revenue,2)
# revenue= revenue.fillna("")
# revenue = revenue.T
# revenue.columns = months
# revenue = revenue.reset_index()
# revenue = revenue[1:]

# months = all_rows_1["month"].unique().tolist()
# withdrawals_mtd_overall = []
# withdrawals_mtd_quess = []
# withdrawals_mtd_d2c = []
# for x in months:
#     u = all_rows_1[all_rows_1["month"]==x]
#     withdrawals_mtd_overall.append(u["Total Amount"].sum())
#     withdrawals_mtd_quess.append(u[u["organization_id"]=="Quess"]["Total Amount"].sum())
#     withdrawals_mtd_d2c.append(u[u["organization_id"]=="D2C Org"]["Total Amount"].sum())
    
# withdrawals = pd.DataFrame(months)
# withdrawals["Withdrawals_overall"] = withdrawals_mtd_overall
# withdrawals["MoM % Change in Withdrawals_overall"] = withdrawals["Withdrawals_overall"].pct_change()*100
# withdrawals["Withdrawals_quess"] = withdrawals_mtd_quess
# withdrawals["MoM % Change in Withdrawals_quess"] = withdrawals["Withdrawals_quess"].pct_change()*100
# withdrawals["Withdrawals_D2C"] = withdrawals_mtd_d2c
# withdrawals["MoM % Change in Withdrawals_D2C"] = withdrawals["Withdrawals_D2C"].pct_change()*100
# withdrawals.replace([np.inf, -np.inf], np.nan,inplace=True)
# withdrawals = round(withdrawals,2)
# withdrawals= withdrawals.fillna("")
# withdrawals = withdrawals.T
# withdrawals.columns = months
# withdrawals = withdrawals.reset_index()
# withdrawals = withdrawals[1:]



# months = all_rows_1["month"].unique().tolist()
# withdrawals_no_mtd_overall = []
# withdrawals_no_mtd_quess = []
# withdrawals_no_mtd_d2c = []
# for x in months:
#     u = all_rows_1[all_rows_1["month"]==x]
#     withdrawals_no_mtd_overall.append(u["tid"].nunique())
#     withdrawals_no_mtd_quess.append(u[u["organization_id"]=="Quess"]["tid"].nunique())
#     withdrawals_no_mtd_d2c.append(u[u["organization_id"]=="D2C Org"]["tid"].nunique())
    
# withdrawals_no = pd.DataFrame(months)
# withdrawals_no["withdrawals_no_overall"] = withdrawals_no_mtd_overall
# withdrawals_no["MoM % Change in withdrawals_no_overall"] = withdrawals_no["withdrawals_no_overall"].pct_change()*100
# withdrawals_no["withdrawals_no_quess"] = withdrawals_no_mtd_quess
# withdrawals_no["MoM % Change in withdrawals_no_quess"] = withdrawals_no["withdrawals_no_quess"].pct_change()*100
# withdrawals_no["withdrawals_no_D2C"] = withdrawals_no_mtd_d2c
# withdrawals_no["MoM % Change in withdrawals_no_D2C"] = withdrawals_no["withdrawals_no_D2C"].pct_change()*100
# withdrawals_no.replace([np.inf, -np.inf], np.nan,inplace=True)
# withdrawals_no = round(withdrawals_no,2)
# withdrawals_no= withdrawals_no.fillna("")
# withdrawals_no = withdrawals_no.T
# withdrawals_no.columns = months
# withdrawals_no = withdrawals_no.reset_index()
# withdrawals_no = withdrawals_no[1:]


# mtd_bq = bq_cleaner(mtd)
# pandas_gbq.to_gbq(mtd_bq, destination_table="Processed_data.MTD_user_Data", project_id="data-warehouse-india", if_exists="replace")

# revenue_bq = bq_cleaner(revenue)
# pandas_gbq.to_gbq(revenue_bq, destination_table="Processed_data.MTD_revenue_Data", project_id="data-warehouse-india", if_exists="replace")

# withdrawals_bq = bq_cleaner(withdrawals)
# pandas_gbq.to_gbq(withdrawals_bq, destination_table="Processed_data.MTD_withdrawals_Data", project_id="data-warehouse-india", if_exists="replace")

# withdrawals_no_bq = bq_cleaner(withdrawals_no)
# pandas_gbq.to_gbq(withdrawals_no_bq, destination_table="Processed_data.MTD_withdrawals_no_Data", project_id="data-warehouse-india", if_exists="replace")



# time.sleep(2)
# print ("End of MTD")










print ("Uploading binned salary, binned age, All withdrawals and cumulative sheet on Data Studio to Sheet 1")

# start = time.time()
# scope = ["https://www.googleapis.com/auth/spreadsheets", "https://www.googleapis.com/auth/drive.file", 
#             "https://www.googleapis.com/auth/drive"]
# creds = ServiceAccountCredentials.from_json_keyfile_name("warm-torus-341214-30e43816fb06.json", scope)
# client_2 = gspread.authorize(creds)
# employees_kyc_demographic= client_2.open("Data Studio Final Dashboard").worksheet("Sheet1")
# employees_kyc_demographic.clear()
# existing = gd.get_as_dataframe(employees_kyc_demographic)
# existing = pd.DataFrame(employees_kyc_demographic.get_all_records())
# updated = existing.append(all_rows_1.copy())
# gd.set_with_dataframe(employees_kyc_demographic, updated)
# print (time.time() - start)

sheet1_bq = bq_cleaner(all_rows_1.copy())
pandas_gbq.to_gbq(sheet1_bq, destination_table="Processed_data.Sheet_1_DataStudio", project_id="data-warehouse-india", if_exists="replace")



# print ("Uploading Repayment data to Sheet 2")
# start = time.time()
# scope = ["https://www.googleapis.com/auth/spreadsheets", "https://www.googleapis.com/auth/drive.file", 
#             "https://www.googleapis.com/auth/drive"]
# creds = ServiceAccountCredentials.from_json_keyfile_name("warm-torus-341214-30e43816fb06.json", scope)
# client_2 = gspread.authorize(creds)
# employees_kyc_demographic= client_2.open("Data Studio Final Dashboard").worksheet("Sheet2")
# employees_kyc_demographic.clear()
# existing = gd.get_as_dataframe(employees_kyc_demographic)
# existing = pd.DataFrame(employees_kyc_demographic.get_all_records())
# updated = existing.append(tr.copy())
# gd.set_with_dataframe(employees_kyc_demographic, updated)
# print (time.time() - start)


# tr_bq = bq_cleaner(tr)
# pandas_gbq.to_gbq(tr_bq, destination_table="Processed_data.Consolidated_Repayment_Data", project_id="data-warehouse-india", if_exists="replace")



print ("Seperating Quess and D2C for further drill downs, this will power the Quess,D2C, Pages on datastudio")
query = """select * from iam.users u ;"""
iam = dataframe_generator(query)
iam = clean(iam)
iam.rename(columns={"id":"user_id"},inplace=True)
phone_number = iam["phone_number"].astype(str).tolist()
phone_numbers = []
for x in phone_number:
    phone_numbers.append(re.sub("[^0-9]", "", x))
phone_number_2 =[]
for x in phone_numbers:
    if len(x)>10:
        phone_number_2.append(x[2:])
    else:
        phone_number_2.append(x)
iam["phone_number"] = phone_number_2

iam = iam[["user_id", "full_name", "status", "email", "phone_number", "created_at", "metadata"]]

gender=[]
birth_date=[]
for i in range(0,iam.shape[0]):
    try:
        gender.append(iam["metadata"].iloc[i]["gender"])
    except KeyError as e:
        print("KeyError:",e)
        gender.append("")
    try:
        birth_date.append(iam["metadata"].iloc[i]["birth_date"])
    except KeyError as e:
        print("KeyError:",e)
        birth_date.append("")


# for i in range(0,iam.shape[0]):
#     gender.append(iam["metadata"].iloc[i]["gender"])
#     birth_date.append(iam["metadata"].iloc[i]["birth_date"])

iam["Gender"] = gender
iam["birth_date"] = birth_date
iam.drop(["metadata"],1,inplace=True)

start = time.time()
query = """select * from ems.employees e ;"""
ems_employees = dataframe_generator(query)
ems_employees = ems_employees[['id', 'user_id', 'employer_id', 'status', 'first_name', 'last_name',
        'created_at', 'updated_at', 'group_id', 'hr_employee_code', 'organization_id']]
ems_employees = ems_employees.rename(columns={'id': 'employee_id'})
ems_employees= clean(ems_employees)
print (time.time() - start)


ems_employees = ems_employees[["employee_id", "user_id", "employer_id"]]

start = time.time()
query = """select id, organization_id, lookup_name from xorg.employers e;"""
xorg = dataframe_generator(query)
xorg = xorg[["id","organization_id", "lookup_name"]]
xorg["lookup_name"] = xorg["lookup_name"].str.lower()
xorg.rename(columns={"id":"employer_id"},inplace=True)
print (time.time() - start)


start = time.time()
xorg["organization_id"].replace({"c2a6a007-e625-456f-8c36-92cd2654c971": 'Quess',
                                              "916227f6-cb69-46ec-8cb1-a735ed98f2c4": 'D2C Org', 
                                             "d779558a-09cc-4920-9f39-d8409c8f0728":"B2B Test", 
                                             "0706c2e4-5153-4429-9645-a6d0c4a26a04":"HCL", 
                                    "6740ddac-09fa-4c0b-9cad-a261cc23997e":"Cloudnine", 
                                    "2f57a3bb-26a6-4cd6-953e-ce61fe607e6a":"Wissen Infotech"},inplace=True)
print (time.time() - start)

ems_xorg= pd.merge(ems_employees, xorg, on = "employer_id")

iam_ems_xorg = pd.merge(iam, ems_xorg, on = "user_id", how = "left")


b2b2c = iam_ems_xorg[iam_ems_xorg["organization_id"]!="D2C Org"]

quess = iam_ems_xorg[iam_ems_xorg["organization_id"]=="Quess"]
d2c = iam_ems_xorg[iam_ems_xorg["organization_id"]=="D2C Org"]


#new edits for B2B2C
# cn = iam_ems_xorg[iam_ems_xorg["organization_id"]=="Cloudnine"]
# hcl = iam_ems_xorg[iam_ems_xorg["organization_id"]=="HCL"]

query = """select * from risk.user_risk_verifications urv; """
cv = dataframe_generator(query)
cv = clean(cv)

cv.rename(columns={"score":"Approved"},inplace=True)

os.chdir("..")
os.chdir("AWS_Data/")
rootdir = os.getcwd()
files_dump =[]
for subdir, dirs, files in os.walk(rootdir):
    for file in files:
#             print(os.path.join(subdir, file))
        if file.endswith("json"):
            files_dump.append(os.path.join(subdir, file))
ff = []
for x in files_dump:
    fff = {}
    f = open(x)
    try:
        data = json.load(f)
    except:
        data = "Json Failure - Issue at our AWS end"
    fff["user_id"] = str(f).split("/")[-2]
    fff["data"] = data
    ff.append(fff)
ffff = pd.DataFrame(ff)
os.chdir("..")
os.chdir("Code")

aws_approved = []
for x in ffff["data"]:
    if x=="Yes":
        aws_approved.append(True)
    else:
        aws_approved.append(False)
ffff["Aws Approved"] = aws_approved
cv = pd.merge(cv,ffff, on = "user_id", how = "left")


cv= pd.merge(cv, iam_ems_xorg[["user_id", "organization_id", "lookup_name"]], on = "user_id", how = "left")

# start = time.time()
# scope = ["https://www.googleapis.com/auth/spreadsheets", "https://www.googleapis.com/auth/drive.file", 
#             "https://www.googleapis.com/auth/drive"]
# creds = ServiceAccountCredentials.from_json_keyfile_name("warm-torus-341214-30e43816fb06.json", scope)
# client_2 = gspread.authorize(creds)
# employees_kyc_demographic= client_2.open("Data Studio Final Dashboard").worksheet("Sheet18")
# employees_kyc_demographic.clear()
# existing = gd.get_as_dataframe(employees_kyc_demographic)
# existing = pd.DataFrame(employees_kyc_demographic.get_all_records())
# updated = existing.append(cv.copy())
# gd.set_with_dataframe(employees_kyc_demographic, updated)
# print (time.time() - start)

cv_bq = bq_cleaner(cv)
pandas_gbq.to_gbq(cv_bq, destination_table="Processed_data.Consolidated_Bureau_data", project_id="data-warehouse-india", if_exists="replace")

b2b2c_cv = cv[["user_id", "Approved", "data", "underwriting", "fraud", "kyc"]]
quess_cv = cv[["user_id", "Approved", "data", "underwriting", "fraud", "kyc"]]
d2c_cv = cv[["user_id", "Approved", "data", "underwriting", "fraud", "kyc"]]







b2b2c = pd.merge(b2b2c, b2b2c_cv, on = "user_id", how = "left")

quess = pd.merge(quess, quess_cv, on = "user_id", how = "left")
#b2b2c clients
# hcl = pd.merge(hcl, quess_cv, on = "user_id", how = "left")
# cn = pd.merge(cn, quess_cv, on = "user_id", how = "left")



b2b2c["Approved"].replace({False: 'Rejected', True: 'Approved'}, inplace=True)
quess["Approved"].replace({False: 'Rejected', True: 'Approved'}, inplace=True)

#b2b2c clients
# hcl["Approved"].replace({False: 'Rejected', True: 'Approved'}, inplace=True)
# cn["Approved"].replace({False: 'Rejected', True: 'Approved'}, inplace=True)

b2b2c['fraud'] = b2b2c['fraud'].replace({False: 'Rejected', True: 'Approved'})
b2b2c['underwriting'] = b2b2c['underwriting'].replace({False: 'Rejected', True: 'Approved'})
b2b2c["kyc"] = b2b2c["kyc"].replace({False: 'Rejected', True: 'Approved'})


# cn['fraud'] = cn['fraud'].replace({False: 'Rejected', True: 'Approved'})
# cn['underwriting'] = cn['underwriting'].replace({False: 'Rejected', True: 'Approved'})
# cn["kyc"] = cn["kyc"].replace({False: 'Rejected', True: 'Approved'})






d2c = pd.merge(d2c, d2c_cv, on = "user_id", how = "left")

d2c["Approved"].replace({False: 'Rejected', True: 'Approved'}, inplace=True)

d2c['fraud'] = d2c['fraud'].replace({False: 'Rejected', True: 'Approved'})
d2c['underwriting'] = d2c['underwriting'].replace({False: 'Rejected', True: 'Approved'})
d2c["kyc"] = d2c["kyc"].replace({False: 'Rejected', True: 'Approved'})

print ("Starting Event logs - This could take a while")
# start = time.time()
# query = """select body, action from elog.events e;"""
# elog = dataframe_generator(query)
# categories = elog["action"].value_counts().index.tolist()
# print (categories)
# print (time.time() - start)

print ("Connecting to risk.user_employment_verifications")
query = """select * from risk.user_employment_verifications uev ;"""
uev = dataframe_generator(query)
uev= clean(uev)
fb = uev[uev["vendor"]=="FINBOX"]
fb = fb.groupby("user_id").last().reset_index()
fb["finbox_hit"] = "Yes"
fb = fb[["user_id", "finbox_hit"]]
count_rules = uev.groupby("user_id").count().reset_index()

finbox_only = uev[uev["user_id"].isin(count_rules[(count_rules["id"]==1) & (count_rules["vendor"]==1)]["user_id"].tolist())]
finbox_only["finbox_only"]="Yes"
perfios = uev[~uev["user_id"].isin(finbox_only["user_id"].unique().tolist())]
perfios["finbox_only"]="No"
perfios["vendor"] = perfios["vendor"].fillna("Perfios")
finbox = pd.concat([finbox_only, perfios])
finbox = pd.merge(finbox, fb, on = "user_id", how = "left")




# uev["vendor"] = uev["vendor"].fillna("Perfios")

# finbox = uev.copy()
# finbox = finbox.groupby(by = "user_id").last().reset_index()
finbox = finbox[["user_id", "status", "vendor", "report", "finbox_only"]]

finbox_reports = finbox[finbox["report"].notnull()]

sms = finbox.copy()
sms["sms_flag"] = pd.DataFrame([x for x in finbox_reports['report']])["sms_permission_flag"]
sms.fillna("False",inplace=True)
sms = sms[["user_id", "sms_flag"]]


# uid = finbox["user_id"].unique().tolist()
# sms = []
# c = len(uid)
# for x in uid:
#     sms_flag = {}
#     sms_flag["user_id"] = x
#     try:
#         sms_flag["sms_flag"] = finbox_reports[finbox_reports["user_id"]==x]["report"].tolist()[0]["sms_permission_flag"]
#     except:
#         sms_flag = "False"


#     sms.append(sms_flag)
#     c-=1
#     print (c)
    
    
# sms = [x for x in sms if x != "False"]
# sms = pd.DataFrame(sms)
finbox = pd.merge(finbox, sms, on = "user_id", how = "left")
finbox.rename(columns={"status":"finbox_status"},inplace = True)
finbox["sms_flag"] = finbox["sms_flag"].fillna("False")
d2c = pd.merge(d2c, finbox, on = "user_id", how = "left")






print ("Starting KYC table")
start = time.time()
query = """select * from kyc.documents kyc ;"""
kyc = dataframe_generator(query)
kyc = clean(kyc)
print (time.time() - start)

kyc = kyc.groupby("user_id").last().reset_index()[["user_id", "approved", "document_type", "side"]]

kyc.rename(columns={"approved":"kyc_approved"},inplace=True)

b2b2c= pd.merge(b2b2c, kyc, on = "user_id", how = "left")
quess = pd.merge(quess, kyc, on = "user_id", how = "left")

d2c = pd.merge(d2c, kyc, on = "user_id", how = "left")

# cn = pd.merge(cn, kyc, on = "user_id", how = "left")
# hcl = pd.merge(hcl, kyc, on = "user_id", how = "left")

d2c["kyc_approved"].replace({False: 'Rejected', True: 'Approved'}, inplace=True)
b2b2c["kyc_approved"].replace({False: 'Rejected', True: 'Approved'}, inplace=True)
quess["kyc_approved"].replace({False: 'Rejected', True: 'Approved'}, inplace=True)


# cn["kyc_approved"].replace({False: 'Rejected', True: 'Approved'}, inplace=True)
# hcl["kyc_approved"].replace({False: 'Rejected', True: 'Approved'}, inplace=True)



kyc["Company"] = "Quess"


query = """select * from bnk.external_accounts ea ;"""
bnk_external = dataframe_generator(query)
bnk_external = clean(bnk_external)

bnk_external = bnk_external[["user_id", "status"]]

bnk_external.rename(columns={"status":"bank_status"},inplace=True)

bnk_external = bnk_external[bnk_external["bank_status"]=="ACTIVE"]

b2b2c = pd.merge(b2b2c, bnk_external, on = "user_id", how = "left")
quess = pd.merge(quess, bnk_external, on = "user_id", how = "left")

d2c = pd.merge(d2c,bnk_external, on = "user_id", how = "left")


# cn = pd.merge(cn,bnk_external, on = "user_id", how = "left")
# hcl = pd.merge(hcl,bnk_external, on = "user_id", how = "left")


#Enach

query = """select * from bnk.enach_registration be;"""
enach = dataframe_generator(query)
enach = clean(enach)

enach = enach.groupby(["user_id"]).last().reset_index()
enach = enach[["user_id", "status", "error_message"]]
enach.rename(columns = {"status":"enach_status", "error_message":"enach_error"},inplace=True)


d2c = pd.merge(d2c, enach, on = "user_id", how = "left")
d2c["enach_status"] = d2c["enach_status"].fillna("SKIP")

b2b2c = pd.merge(b2b2c, enach, on = "user_id", how = "left")
b2b2c["enach_status"] = b2b2c["enach_status"].fillna("SKIP")


# b2b2c = pd.merge(b2b2c, enach, on = "user_id", how = "left")
# b2b2c["enach_status"] = b2b2c["enach_status"].fillna("SKIP")

# hcl = pd.merge(hcl, enach, on = "user_id", how = "left")
# hcl["enach_status"] = hcl["enach_status"].fillna("SKIP")






query = """select * from ems.loan_agreements la ;"""
la = dataframe_generator(query)
la = clean(la)

la = la[["employee_id", "accepted"]]

b2b2c = pd.merge(b2b2c, la, on = "employee_id", how = "left")
quess = pd.merge(quess, la, on = "employee_id", how = "left")



# cn = pd.merge(cn, la, on = "employee_id", how = "left")

# hcl = pd.merge(hcl, la, on = "employee_id", how = "left")

d2c = pd.merge(d2c, la, on = "employee_id", how = "left")

d2c = d2c[(d2c["lookup_name"]!="quees corp ltd")&(d2c["lookup_name"]!="quess corp limited")&(d2c["lookup_name"]!="rainpay")]

# start = time.time()
# scope = ["https://www.googleapis.com/auth/spreadsheets", "https://www.googleapis.com/auth/drive.file", 
#             "https://www.googleapis.com/auth/drive"]
# creds = ServiceAccountCredentials.from_json_keyfile_name("warm-torus-341214-30e43816fb06.json", scope)
# client_2 = gspread.authorize(creds)
# employees_kyc_demographic= client_2.open("Data Studio Final Dashboard").worksheet("Sheet4")
# employees_kyc_demographic.clear()
# existing = gd.get_as_dataframe(employees_kyc_demographic)
# existing = pd.DataFrame(employees_kyc_demographic.get_all_records())
# updated = existing.append(quess.copy())
# gd.set_with_dataframe(employees_kyc_demographic, updated)
# print (time.time() - start)

print ("Uploading B2B2C data")
b2b2c_bq = bq_cleaner(b2b2c)
pandas_gbq.to_gbq(b2b2c_bq, destination_table="Processed_data.Consolidated_B2B2C_data", project_id="data-warehouse-india", if_exists="replace")


# quess_bq = bq_cleaner(quess)
# pandas_gbq.to_gbq(quess_bq, destination_table="Processed_data.Consolidated_Quess_data", project_id="data-warehouse-india", if_exists="replace")



# cn_bq = bq_cleaner(cn)
# pandas_gbq.to_gbq(cn_bq, destination_table="Processed_data.Consolidated_CN_data", project_id="data-warehouse-india", if_exists="replace")


# hcl_bq = bq_cleaner(hcl)
# pandas_gbq.to_gbq(hcl_bq, destination_table="Processed_data.Consolidated_HCL_data", project_id="data-warehouse-india", if_exists="replace")







# start = time.time()
# scope = ["https://www.googleapis.com/auth/spreadsheets", "https://www.googleapis.com/auth/drive.file", 
#             "https://www.googleapis.com/auth/drive"]
# creds = ServiceAccountCredentials.from_json_keyfile_name("warm-torus-341214-30e43816fb06.json", scope)
# client_2 = gspread.authorize(creds)
# employees_kyc_demographic= client_2.open("Data Studio Final Dashboard").worksheet("Sheet5")
# employees_kyc_demographic.clear()
# existing = gd.get_as_dataframe(employees_kyc_demographic)
# existing = pd.DataFrame(employees_kyc_demographic.get_all_records())
# updated = existing.append(d2c.copy())
# gd.set_with_dataframe(employees_kyc_demographic, updated)
# print (time.time() - start)

d2c_bq = bq_cleaner(d2c)
pandas_gbq.to_gbq(d2c_bq, destination_table="Processed_data.Consolidated_D2C_data", project_id="data-warehouse-india", if_exists="replace")


b2b2c_uid_kyc = b2b2c[["user_id", "phone_number", "full_name", "organization_id"]]
quess_uid_kyc = quess[["user_id", "phone_number", "full_name", "organization_id"]]
d2c_uid_kyc = d2c[["user_id", "phone_number", "full_name", "organization_id"]]

uid_kyc = pd.concat([b2b2c_uid_kyc, d2c_uid_kyc])

query = """select * from kyc.documents kyc ;"""
kyc = dataframe_generator(query)
kyc = clean(kyc)
kyc["side"].replace("",True, inplace=True)
kyc = kyc[["user_id", "document_type", "side", "verified", "created_at", "approved"]]
# kyc = kyc[kyc["approved"]==True]
kyc = pd.merge(kyc, uid_kyc, on = "user_id", how = "left")

lookup_name_kyc_filter = d2c[["user_id", "lookup_name"]]

kyc = pd.merge(kyc, lookup_name_kyc_filter, on = "user_id", how = "left")

start = time.time()
uid = kyc["user_id"].unique().tolist()

selfie_only= []
selfie_and_pan = []
all_3=[]
for x in uid:
    d = kyc[kyc["user_id"]==x]["document_type"].value_counts().index.tolist()
    if "SILENTLIVENESS" in d:
        selfie_only.append(x)
    if "SILENTLIVENESS" in d and "PAN" in d:
        selfie_and_pan.append(x)
    if "SILENTLIVENESS" in d and "PAN" in d and "AADHAAR" in d:
        all_3.append(x)
print (time.time() - start)


stage_1 = pd.DataFrame(selfie_only, columns=["user_id"])
stage_1["Stage_1"] = True
stage_1

kyc = pd.merge(kyc,stage_1, on = "user_id", how = "left")

stage_2 = pd.DataFrame(selfie_and_pan, columns=["user_id"])
stage_2["Stage_2"] = True
stage_2

kyc = pd.merge(kyc,stage_2, on = "user_id", how = "left")

stage_3 = pd.DataFrame(all_3, columns=["user_id"])
stage_3["Stage_3"] = True
stage_3

kyc = pd.merge(kyc,stage_3, on = "user_id", how = "left")


# start = time.time()
# scope = ["https://www.googleapis.com/auth/spreadsheets", "https://www.googleapis.com/auth/drive.file", 
#             "https://www.googleapis.com/auth/drive"]
# creds = ServiceAccountCredentials.from_json_keyfile_name("warm-torus-341214-30e43816fb06.json", scope)
# client_2 = gspread.authorize(creds)
# employees_kyc_demographic= client_2.open("Data Studio Final Dashboard").worksheet("Sheet6")
# employees_kyc_demographic.clear()
# existing = gd.get_as_dataframe(employees_kyc_demographic)
# existing = pd.DataFrame(employees_kyc_demographic.get_all_records())
# updated = existing.append(kyc.copy())
# gd.set_with_dataframe(employees_kyc_demographic, updated)
# print (time.time() - start)


kyc_bq = bq_cleaner(kyc)
pandas_gbq.to_gbq(kyc_bq, destination_table="Processed_data.Consolidated_KYC_data", project_id="data-warehouse-india", if_exists="replace")


# df["created_at"] = pd.to_datetime(df["created_at"]).dt.date.astype(str)
# from datetime import date
# sdate = date(2021,9,1)
# edate = pd.to_datetime("today").strftime('%Y-%m-%d')
# dates = pd.date_range(sdate,edate,freq='d').strftime('%Y-%m-%d').tolist()

# l = ['Users landing on Quess Dash',
#   'User Creates Password',
# #  'Bureau Approved',
#   'Kyc Hits',
#   'KYC All 3 stages Passed',
#   'Unique Withdrawing users',
#   'Total Number of withdrawals',
#   'Same Day Withdrawal',
#   'New users today']

# def dates_generator(date):
#     tf = {}
#     tf["Users landing on Quess Dash"] = df[df["created_at"]==date].shape[0]
#     tf["User Creates Password"] = quess[quess["created_at"]==date]["user_id"].nunique()
#     tf["Bureau Approved"] = quess[quess["created_at"]==date]["Approved"].value_counts().values[0]
#     tf["Kyc Hits"] = kyc[(kyc["created_at"]==date)&(kyc["organization_id"]=="Quess")]["user_id"].nunique()
#     k = kyc[(kyc["created_at"]==date)&(kyc["organization_id"]=="Quess")]
#     tf["KYC All 3 stages Passed"] = k[(k["Stage_1"]==True)&(k["Stage_2"]==True)&(k["Stage_3"]==True)]["user_id"].nunique()
#     a = all_rows_1[(all_rows_1["disbursal(txn) date"]==date)&(all_rows_1["organization_id"]=="Quess")]
#     tf["Unique Withdrawing users"] = a["user_id"].nunique()
#     tf["Total Number of withdrawals"] = a.shape[0]
#     a1 = a[["user_id", "disbursal(txn) date"]]
#     q = quess[["user_id", "created_at"]]
#     a1 = pd.merge(a1,q,on = "user_id", how = "left")
#     tf["Same Day Withdrawal"] = a1[(a1["disbursal(txn) date"]==a1["created_at"])].shape[0]
#     l1 = all_rows_1[all_rows_1["disbursal(txn) date"]<date]["user_id"].unique().tolist()
#     l2 = a["user_id"].unique().tolist()
#     # print (len(l1))
#     # print (len(l2))
#     tf["New users today"] = len(list(set(l2) - set(l1)))
#     tf = pd.DataFrame(tf.items())
#     tf.columns = ["Funnel_Stage",date]
#     tf.drop(["Funnel_Stage"],1,inplace = True)
#     return tf


# date = dates[1]
# tf = {}
# tf["Users landing on Quess Dash"] = df[df["created_at"]==date].shape[0]
# tf["User Creates Password"] = quess[quess["created_at"]==date]["user_id"].nunique()
# tf["Bureau Approved"] = quess[quess["created_at"]==date]["Approved"].value_counts().values[0]
# tf["Kyc Hits"] = kyc[(kyc["created_at"]==date)&(kyc["organization_id"]=="Quess")]["user_id"].nunique()
# k = kyc[(kyc["created_at"]==date)&(kyc["organization_id"]=="Quess")]
# tf["KYC All 3 stages Passed"] = k[(k["Stage_1"]==True)&(k["Stage_2"]==True)&(k["Stage_3"]==True)]["user_id"].nunique()
# a = all_rows_1[(all_rows_1["disbursal(txn) date"]==date)&(all_rows_1["organization_id"]=="Quess")]
# tf["Unique Withdrawing users"] = a["user_id"].nunique()
# tf["Total Number of withdrawals"] = a.shape[0]
# a1 = a[["user_id", "disbursal(txn) date"]]
# q = quess[["user_id", "created_at"]]
# a1 = pd.merge(a1,q,on = "user_id", how = "left")
# tf["Same Day Withdrawal"] = a1[(a1["disbursal(txn) date"]==a1["created_at"])].shape[0]
# l1 = all_rows_1[all_rows_1["disbursal(txn) date"]<date]["user_id"].unique().tolist()
# l2 = a["user_id"].unique().tolist()
# # print (len(l1))
# # print (len(l2))
# tf["New users today"] = len(list(set(l2) - set(l1)))
# tf = pd.DataFrame(tf.items())
# tf.columns = ["Funnel_Stage",date]
# tf.drop(["Funnel_Stage"],1,inplace = True)




# all_concat = []
# for x in dates:
#     all_concat.append(dates_generator(x))

        


# errors = []
# all_concat = []
# for x in dates:
#     try:
#         all_concat.append(dates_generator(x))
#     except:
#         errors.append(x)
        
        
        
        
# tff = pd.concat(all_concat,1)

# d= tff.columns.tolist()
# from datetime import datetime

# d1 = []
# for x in d:
#     d1.append(datetime.strptime(x, '%Y-%m-%d').strftime("%b_%d_%Y"))
    
# tff.columns = d1

# tff["Funnel_Stage"] = l
# cols = list(tff.columns)
# cols = [cols[-1]] + cols[:-1]
# tff = tff[cols]



# tff_bq = bq_cleaner(tff)
# pandas_gbq.to_gbq(tff_bq, destination_table="Processed_data.Quess_date_cross_sectional", project_id="data-warehouse-india", if_exists="replace")



print ("\n")
print ("Total Run Time: "+str(time.time() - start_1))










