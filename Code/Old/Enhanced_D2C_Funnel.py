#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 17 21:18:12 2022

@author: arunabhmajumdar
"""

print ("Importing all packages and Google BQ credentials files")
import warnings
warnings.filterwarnings("ignore")
import pandas as pd,os
import base64
import boto3
import time
import pandas as pd, os
from oauth2client.service_account import ServiceAccountCredentials
import gspread
import gspread_dataframe as gd
import psycopg2
import df2gspread as d2g
import pandas as pd
import warnings
warnings.filterwarnings("ignore")
import numpy as np
import re
import calendar
import os
import json
import time
print ("Changing directory to Code to aid ease of access to the various jsons and xlsx")
print(os.getcwd())
os.chdir("..")
time.sleep(2)
print ("Changed Directory")
print (os.getcwd())
start_1 = time.time()
import numpy as np
import pytz
my_timezone = pytz.timezone('Asia/Calcutta')
import ast
import gzip
from datetime import timedelta
from currency_converter import CurrencyConverter
from google.oauth2 import service_account
from google.cloud import bigquery
import pandas_gbq
import os, pandas as pd
os.getcwd()
KEY_PATH = "data-warehouse-india-84f5f8a775d1.json"
CREDS = service_account.Credentials.from_service_account_file(KEY_PATH)
bq_client = bigquery.Client(credentials=CREDS, project="data-warehouse-india")
import warnings
warnings.filterwarnings("ignore")


print ("Connecting to Dynamo DB using boto3")
start = time.time()
print ("starting run")
session = boto3.session.Session(profile_name="rain-india-prod")
client = session.client("dynamodb")
dynamodb = boto3.resource("dynamodb")
print ("Connecting to Postgres using psycopg2")
connection = psycopg2.connect(user="rainadmin",
                                      password="Mudar123",
                                      host="localhost",
                                      port=55432,
                                      database="rain")
cursor = connection.cursor()
# Print PostgreSQL details
print("PostgreSQL server information")
print(connection.get_dsn_parameters(), "\n")
# cursor.itersize = 10000
cursor.execute("SELECT version();")
    # Fetch result
record = cursor.fetchone()
print("You are connected to - ", record, "\n")

print ("Function to download Postgres data and concert that to dataframe")
def dataframe_generator(query):
    cursor.execute(query)
    print('Read table in PostgreSQL')
    data = cursor.fetchall()
    cols = []
    for elt in cursor.description:
        cols.append(elt[0])
    df= pd.DataFrame(data = data, columns=cols)
    return df

print ("Function to clean dataframe to include only data post Sept 1, 2021")
def clean(df):
    df["created_at"] = df["created_at"].dt.date.astype(str)
    df = df[df["created_at"]>"2021-08-31"]
    return df
print (time.time() - start)


print ("Function to clean dataframe to upload to BQ, BQ doesn't take spaces, dashes etc;")
def bq_cleaner(df):
    new_cols = []
    l = df.columns.tolist()
    for x in l:
        x = x.replace("(","_")
        x = x.replace(" ","_")
        x = x.replace(")","_")
        new_cols.append(x)
    df.columns = new_cols
    df = df.astype(str)
    return df


query = """select * from iam.users u ;"""
iam = dataframe_generator(query)
iam = clean(iam)
iam.rename(columns={"id":"user_id"},inplace=True)
phone_number = iam["phone_number"].astype(str).tolist()
phone_numbers = []
for x in phone_number:
    phone_numbers.append(re.sub("[^0-9]", "", x))
phone_number_2 =[]
for x in phone_numbers:
    if len(x)>10:
        phone_number_2.append(x[2:])
    else:
        phone_number_2.append(x)
iam["phone_number"] = phone_number_2

iam = iam[["user_id", "full_name", "status", "email", "phone_number","document_number", "created_at", "metadata"]]

gender=[]
birth_date=[]
for i in range(0,iam.shape[0]):
    try:
        gender.append(iam["metadata"].iloc[i]["gender"])
    except KeyError as e:
        print("KeyError:",e)
        gender.append("")
    try:
        birth_date.append(iam["metadata"].iloc[i]["birth_date"])
    except KeyError as e:
        print("KeyError:",e)
        birth_date.append("")

iam["Gender"] = gender
iam["birth_date"] = birth_date

new_d2c = iam.copy()
uid = new_d2c["user_id"].unique().tolist()


print ("Connecting to risk.user_employment_verifications")
query = """select * from risk.user_employment_verifications uev ;"""
uev = dataframe_generator(query)
uev= clean(uev)
uev = uev.groupby("user_id").last().reset_index()


all_hits = uev[uev["user_id"].isin(uid)]
finbox_hits = all_hits[all_hits["vendor"]=="FINBOX"]
# finbox_hits = uev[uev["user_id"].isin(uid)]
finbox_reports = finbox_hits[finbox_hits["report"].notnull()]

finbox_hits = finbox_hits[["user_id", "status"]]
finbox_hits.rename(columns={"status":"finbox_status"}, inplace=True)


new_d2c = pd.merge(new_d2c, finbox_hits, on = "user_id", how = "left")

new_d2c["finbox_status"] = new_d2c["finbox_status"].fillna("No hit")


sms = finbox_hits.copy()
sms["sms_flag"] = pd.DataFrame([x for x in finbox_reports['report']])["sms_permission_flag"]
sms.fillna("False",inplace=True)
sms = sms[["user_id", "sms_flag"]]


finbox_uid = finbox_hits["user_id"].unique().tolist()
# start_1 = time.time()
# finbox_uid = finbox_hit["user_id"].unique().tolist()
# sms = []
# location = []
# phone_state = []
# c = len(finbox_uid)
# for x in finbox_uid:
#     sms_flag = {}
#     sms_flag["user_id"] = x
#     location_flag = {}
#     location_flag["user_id"] = x
#     phone_state_flag = {}
#     phone_state_flag["user_id"] = x
#     try:
#         sms_flag["sms_flag"] = finbox_reports[finbox_reports["user_id"]==x]["report"].tolist()[0]["sms_permission_flag"]
#     except:
#         sms_flag["sms_flag"] = "False"
#     sms.append(sms_flag)
# #     try:
# #         location_flag["location_flag"] = finbox_reports[finbox_reports["user_id"]==x]["report"].tolist()[0]["location_permission_flag"]
# #     except:
# #         location_flag["location_flag"] = "False"
# #     location.append(location_flag)
# #     try:
# #         phone_state_flag["phone_state_flag"] = finbox_reports[finbox_reports["user_id"]==x]["report"].tolist()[0]["phone_state_permission_flag"]
# #     except:
# #         phone_state_flag["phone_state_flag"] = "False"
# #     phone_state.append(phone_state_flag)
#     c-=1
#     print (c)

    
    

# sms = pd.DataFrame(sms)
# # location = pd.DataFrame(location)
# # phone_state = pd.DataFrame(phone_state)
# print (time.time() - start_1)


location = sms.copy()
location.rename(columns = {"sms_flag":"location_flag"},inplace = True)
phone_state = sms.copy()
phone_state.rename(columns = {"sms_flag":"phone_state_flag"},inplace = True)


new_d2c = pd.merge(new_d2c, sms, on = "user_id", how = "left")
new_d2c = pd.merge(new_d2c, location, on = "user_id", how = "left")
new_d2c = pd.merge(new_d2c, phone_state, on = "user_id", how = "left")


stage_4 = new_d2c[new_d2c["user_id"].isin(finbox_hits["user_id"].unique().tolist())]
stage_4 = stage_4[stage_4["document_number"].notnull()]
stage_4["Account_Registration_Complete"] = "Yes"
stage_4 = stage_4[["user_id", "Account_Registration_Complete"]]
new_d2c = pd.merge(new_d2c, stage_4, on = "user_id", how = "left")
new_d2c["Account_Registration_Complete"] = new_d2c["Account_Registration_Complete"].fillna("No")

start = time.time()
query = """select * from ems.employees e ;"""
ems_employees = dataframe_generator(query)
ems_employees = ems_employees[['id', 'user_id', 'employer_id', 'status', 'first_name', 'last_name',
        'created_at', 'updated_at', 'group_id', 'hr_employee_code', 'organization_id']]
ems_employees = ems_employees.rename(columns={'id': 'employee_id'})
ems_employees= clean(ems_employees)
print (time.time() - start)


stage_5  = ems_employees[ems_employees["user_id"].isin(stage_4["user_id"].unique().tolist())]


stage_5["Employer_Selected"] = "Yes"

stage_5 = stage_5[["user_id", "Employer_Selected"]]
new_d2c = pd.merge(new_d2c, stage_5, on = "user_id", how = "left")
start = time.time()
query = """select * from iam.user_addresses iea ;"""
user_addresses = dataframe_generator(query)
user_addresses= clean(user_addresses)
user_addresses = user_addresses.groupby("user_id").last().reset_index()
stage_6 = user_addresses[user_addresses["user_id"].isin(stage_5["user_id"].unique().tolist())]
stage_6["Address_Selected"] = "Yes"
stage_6 = stage_6[["user_id", "Address_Selected"]]
new_d2c = pd.merge(new_d2c, stage_6, on = "user_id", how = "left")


finbox_reports["report"].tolist()


print ("Seperating Quess and D2C for further drill downs, this will power the Quess,D2C, Pages on datastudio")
query = """select * from iam.users u ;"""
iam = dataframe_generator(query)
iam = clean(iam)
iam.rename(columns={"id":"user_id"},inplace=True)
phone_number = iam["phone_number"].astype(str).tolist()
phone_numbers = []
for x in phone_number:
    phone_numbers.append(re.sub("[^0-9]", "", x))
phone_number_2 =[]
for x in phone_numbers:
    if len(x)>10:
        phone_number_2.append(x[2:])
    else:
        phone_number_2.append(x)
iam["phone_number"] = phone_number_2

iam = iam[["user_id", "full_name", "status", "email", "phone_number", "created_at", "metadata"]]

gender=[]
birth_date=[]
for i in range(0,iam.shape[0]):
    try:
        gender.append(iam["metadata"].iloc[i]["gender"])
    except KeyError as e:
        print("KeyError:",e)
        gender.append("")
    try:
        birth_date.append(iam["metadata"].iloc[i]["birth_date"])
    except KeyError as e:
        print("KeyError:",e)
        birth_date.append("")


# for i in range(0,iam.shape[0]):
#     gender.append(iam["metadata"].iloc[i]["gender"])
#     birth_date.append(iam["metadata"].iloc[i]["birth_date"])

iam["Gender"] = gender
iam["birth_date"] = birth_date
iam.drop(["metadata"],1,inplace=True)

start = time.time()
query = """select * from ems.employees e ;"""
ems_employees = dataframe_generator(query)
ems_employees = ems_employees[['id', 'user_id', 'employer_id', 'status', 'first_name', 'last_name',
        'created_at', 'updated_at', 'group_id', 'hr_employee_code', 'organization_id']]
ems_employees = ems_employees.rename(columns={'id': 'employee_id'})
ems_employees= clean(ems_employees)
print (time.time() - start)


ems_employees = ems_employees[["employee_id", "user_id", "employer_id"]]

start = time.time()
query = """select id, organization_id, lookup_name from xorg.employers e;"""
xorg = dataframe_generator(query)
xorg = xorg[["id","organization_id", "lookup_name"]]
xorg["lookup_name"] = xorg["lookup_name"].str.lower()
xorg.rename(columns={"id":"employer_id"},inplace=True)
print (time.time() - start)


start = time.time()
xorg["organization_id"].replace({"c2a6a007-e625-456f-8c36-92cd2654c971": 'Quess',
                                              "916227f6-cb69-46ec-8cb1-a735ed98f2c4": 'D2C Org', 
                                             "d779558a-09cc-4920-9f39-d8409c8f0728":"B2B Test", 
                                             "0706c2e4-5153-4429-9645-a6d0c4a26a04":"HCL", 
                                    "6740ddac-09fa-4c0b-9cad-a261cc23997e":"Cloudnine", 
                                    "2f57a3bb-26a6-4cd6-953e-ce61fe607e6a":"Wissen Infotech"},inplace=True)
print (time.time() - start)

ems_xorg= pd.merge(ems_employees, xorg, on = "employer_id")

iam_ems_xorg = pd.merge(iam, ems_xorg, on = "user_id", how = "left")


b2b2c = iam_ems_xorg[iam_ems_xorg["organization_id"]!="D2C Org"]

quess = iam_ems_xorg[iam_ems_xorg["organization_id"]=="Quess"]
d2c = iam_ems_xorg[iam_ems_xorg["organization_id"]=="D2C Org"]



d2c = d2c[["user_id", "organization_id", "lookup_name"]]


query = """select * from risk.user_risk_verifications urv; """
cv = dataframe_generator(query)
cv = clean(cv)

cv.rename(columns={"score":"Approved"},inplace=True)

os.chdir("..")
os.chdir("AWS_Data/")
rootdir = os.getcwd()
files_dump =[]
for subdir, dirs, files in os.walk(rootdir):
    for file in files:
#             print(os.path.join(subdir, file))
        if file.endswith("json"):
            files_dump.append(os.path.join(subdir, file))
ff = []
for x in files_dump:
    fff = {}
    f = open(x)
    try:
        data = json.load(f)
    except:
        data = "Json Failure - Issue at our AWS end"
    fff["user_id"] = str(f).split("/")[-2]
    fff["data"] = data
    ff.append(fff)
ffff = pd.DataFrame(ff)
os.chdir("..")
os.chdir("Code")

aws_approved = []
for x in ffff["data"]:
    if x=="Yes":
        aws_approved.append(True)
    else:
        aws_approved.append(False)
ffff["Aws Approved"] = aws_approved
cv = pd.merge(cv,ffff, on = "user_id", how = "left")



stage_7 = d2c[d2c["user_id"].isin(uid)]


new_d2c = pd.merge(new_d2c, d2c, on = "user_id", how = "left")


cv = cv[cv["user_id"].isin(stage_7["user_id"].unique().tolist())]
cv = cv[["user_id", "Approved", "underwriting", "fraud", "kyc", "Aws Approved"]]
new_d2c = pd.merge(new_d2c, cv, on = "user_id", how = "left")
new_d2c["Approved"] = new_d2c["Approved"].fillna("No Hit")
stage_8_finbox_d2c_hit = d2c[d2c["user_id"].isin(finbox_uid)]
cv = cv.groupby("user_id").last().reset_index()
cv = cv[cv["user_id"].isin(stage_8_finbox_d2c_hit["user_id"])]
stage_9 = cv[cv["Approved"]==True]


query = """select * from risk.user_employment_verifications uev ;"""
uev = dataframe_generator(query)
uev= clean(uev)
fb = uev[uev["vendor"]=="FINBOX"]
fb = fb.groupby("user_id").last().reset_index()
fb["finbox_hit"] = "Yes"
fb = fb[["user_id", "finbox_hit"]]
count_rules = uev.groupby("user_id").count().reset_index()

finbox_only = uev[uev["user_id"].isin(count_rules[(count_rules["id"]==1) & (count_rules["vendor"]==1)]["user_id"].tolist())]
finbox_only["finbox_only"]="Yes"
perfios = uev[~uev["user_id"].isin(finbox_only["user_id"].unique().tolist())]
perfios["finbox_only"]="No"
perfios["vendor"] = perfios["vendor"].fillna("Perfios")
finbox = pd.concat([finbox_only, perfios])
finbox = pd.merge(finbox, fb, on = "user_id", how = "left")


finbox = finbox[["user_id", "status", "vendor", "report", "finbox_only"]]
finbox = finbox[finbox["finbox_only"]=="Yes"]

stage_10 = finbox[finbox["user_id"].isin(stage_9["user_id"].unique().tolist())]
stage_10 = stage_10[stage_10["status"]=="COMPLETED"]



stage_11 = cv[cv["user_id"].isin(stage_10["user_id"].unique().tolist())]
stage_12_perfios_part_1  = stage_11[(stage_11["fraud"].isnull())&(stage_11["underwriting"].isnull())]
stage_12_perfios_part2 = stage_9[~stage_9["user_id"].isin(stage_11["user_id"].unique().tolist())]
stage_12_final_perfios_uid = stage_12_perfios_part_1["user_id"].unique().tolist()+stage_12_perfios_part2["user_id"].unique().tolist()




stage_13_perfios = stage_9[stage_9["user_id"].isin(stage_12_final_perfios_uid)]


perfios_drop_offs = stage_13_perfios[stage_13_perfios["fraud"].isnull()]


perfios_approved = stage_13_perfios[(stage_13_perfios["underwriting"]==True)&((stage_13_perfios["fraud"]==True))]

print ("Starting KYC table")
start = time.time()
query = """select * from kyc.documents kyc ;"""
kyc = dataframe_generator(query)
kyc = clean(kyc)
print (time.time() - start)

# kyc = kyc.groupby("user_id").last().reset_index()[["user_id", "approved", "document_type", "side"]]

kyc.rename(columns={"approved":"kyc_approved"},inplace=True)

stage_14_kyc_hits = kyc[kyc["user_id"].isin(perfios_approved["user_id"].unique().tolist())]

stage_14_kyc_unique_hit = stage_14_kyc_hits.groupby("user_id").last().reset_index()



stage_10 = stage_10[["user_id", "finbox_only"]]
new_d2c = pd.merge(new_d2c, stage_10, on = "user_id", how = "left")
stage_12_perfios_part_1["Data insufficient for UW_Finbox"] = "Yes"
stage_12_perfios_part_1= stage_12_perfios_part_1[["user_id", "Data insufficient for UW_Finbox"]]
new_d2c = pd.merge(new_d2c, stage_12_perfios_part_1, on = "user_id", how = "left")


stage_13_perfios["perfios_hits"] = "Yes"
stage_13_perfios = stage_13_perfios[["user_id", "perfios_hits"]]
new_d2c = pd.merge(new_d2c, stage_13_perfios, on = "user_id", how = "left")


perfios_drop_offs["perfios_dropoffs"] = "Yes"
perfios_drop_offs = perfios_drop_offs[["user_id", "perfios_dropoffs"]]
new_d2c = pd.merge(new_d2c, perfios_drop_offs, on = "user_id", how = "left")

stage_14_kyc_unique_hit["KYC Hit"]="Yes"
stage_14_kyc_unique_hit = stage_14_kyc_unique_hit[["user_id", "KYC Hit"]]
new_d2c = pd.merge(new_d2c, stage_14_kyc_unique_hit, on = "user_id", how = "left")


print ("Connecting to Big Query Table")
query_string = "select * FROM `data-warehouse-india.Processed_data.KYC_Metrics_DataStudio`"
kyc_metrics = pd.read_gbq(query_string, project_id="data-warehouse-india")

kyc_metrics = kyc_metrics[["user_id", "Stage_1", "Stage_2", "Stage_3"]]


stage_15 = kyc_metrics[kyc_metrics["user_id"].isin(stage_14_kyc_unique_hit["user_id"].tolist())]
stage_16_all_3_stages = stage_15[(stage_15["Stage_1"]=="True")&(stage_15["Stage_2"]=="True")&(stage_15["Stage_3"]=="True")] 





stage_16_dropoffs_kyc = stage_15[~stage_15["user_id"].isin(stage_16_all_3_stages["user_id"].tolist())]



stage_16_all_3_stages["All_3_stages_pass"] = "Yes"
stage_16_all_3_stages = stage_16_all_3_stages[["user_id", "All_3_stages_pass"]]
new_d2c = pd.merge(new_d2c, stage_16_all_3_stages, on = "user_id", how = "left")
stage_16_dropoffs_kyc["KYC Dropoff"] = "Yes"
stage_16_dropoffs_kyc = stage_16_dropoffs_kyc[["user_id", "KYC Dropoff"]]
new_d2c = pd.merge(new_d2c, stage_16_dropoffs_kyc, on = "user_id", how = "left")

query = """select * from bnk.external_accounts ea ;"""
bnk_external = dataframe_generator(query)
bnk_external = clean(bnk_external)

bnk_external = bnk_external[["user_id", "status"]]

bnk_external.rename(columns={"status":"bank_status"},inplace=True)

bnk_external = bnk_external[bnk_external["bank_status"]=="ACTIVE"]
stage_17 = bnk_external[bnk_external["user_id"].isin(stage_16_all_3_stages["user_id"].tolist())]

stage_17["Penny Drop completed"] = "Yes"
stage_17 = stage_17[["user_id", "Penny Drop completed"]]
new_d2c = pd.merge(new_d2c, stage_17,  on = "user_id", how = "left")


query = """select * from bnk.enach_registration be;"""
enach = dataframe_generator(query)
enach = clean(enach)

enach = enach.groupby(["user_id"]).last().reset_index()
enach = enach[["user_id", "status", "error_message"]]
enach.rename(columns = {"status":"enach_status", "error_message":"enach_error"},inplace=True)


new_d2c = pd.merge(new_d2c, enach,  on = "user_id", how = "left")

query = """select * from ems.loan_agreements la ;"""
la = dataframe_generator(query)
la = clean(la)

la = la[["employee_id", "accepted"]]

new_d2c = pd.merge(new_d2c, ems_employees, on = "user_id", how = "left")
new_d2c = pd.merge(new_d2c, la,  on = "employee_id", how = "left")

print ("Connecting to Big Query Table")
query_string = "select * FROM `data-warehouse-india.Processed_data.withdrawals-txns_without_bank`"
all_rows = pd.read_gbq(query_string, project_id="data-warehouse-india")


all_withdrawals = all_rows[all_rows["user_id"].isin(uid)]
all_withdrawals = all_withdrawals[["user_id", "Total_Amount"]]
all_withdrawals["Total_Amount"] = all_withdrawals["Total_Amount"].astype(float)
all_withdrawals = all_withdrawals.groupby("user_id").sum().reset_index()
new_d2c = pd.merge(new_d2c, all_withdrawals, on = "user_id", how = "left")









new_d2c_bq = bq_cleaner(new_d2c)
pandas_gbq.to_gbq(new_d2c_bq, destination_table="Processed_data.New_D2C_Funnel", project_id="data-warehouse-india", if_exists="replace")


print ("/n")
print ("Enhanced D2C Funnel run in "+str(time.time() - start_1))























