#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 30 16:54:37 2022

@author: arunabhmajumdar
"""
code_name = "final_paused_unpaused"
import json
import datetime
from datetime import timedelta
import re
import os
import psycopg2
import gspread
import df2gspread as d2g
import pandas as pd
import warnings
warnings.filterwarnings("ignore")
from oauth2client.service_account import ServiceAccountCredentials
import gspread
import gspread_dataframe as gd
import time
start_1 = time.time()
print ("Changing directory to Code to aid ease of access to the various jsons and xlsx")
print(os.getcwd())
os.chdir("..")
time.sleep(2)
print ("Changed Directory")
print (os.getcwd())
import pytz
my_timezone = pytz.timezone('Asia/Calcutta')
from google.oauth2 import service_account
from google.cloud import bigquery
import pandas_gbq
import os, pandas as pd
os.getcwd()
KEY_PATH = "data-warehouse-india-84f5f8a775d1.json"
CREDS = service_account.Credentials.from_service_account_file(KEY_PATH)
bq_client = bigquery.Client(credentials=CREDS, project="data-warehouse-india")
import warnings
warnings.filterwarnings("ignore")
import numpy as np

connection = psycopg2.connect(user="rainadmin",
                                  password="Mudar123",
                                  host="localhost",
                                  port=55432,
                                  database="rain")

    # Create a cursor to perform database operations
cursor = connection.cursor()
# Print PostgreSQL details
print("PostgreSQL server information")
print(connection.get_dsn_parameters(), "\n")

cursor.execute("SELECT version();")
    # Fetch result
record = cursor.fetchone()
print("You are connected to - ", record, "\n")

date = pd.to_datetime("today").strftime("%d-%b-%Y")


print ("Function to clean dataframe to upload to BQ, BQ doesn't take spaces, dashes etc;")
def bq_cleaner(df):
    new_cols = []
    l = df.columns.tolist()
    for x in l:
        x = x.replace("(","_")
        x = x.replace(" ","_")
        x = x.replace(")","_")
        new_cols.append(x)
    df.columns = new_cols
    df = df.astype(str)
    return df

try:
    os.chdir("..")
    os.chdir("..")
    os.chdir("..")
    os.chdir("Google Drive/")
    os.chdir("Shared drives/")
    os.chdir("Paused Unpaused/")
    date_string = date
    os.chdir(date_string.split("-")[1])
    os.chdir(date_string)
    for x in os.listdir():
        if "Unpause" in x:
            unpause = pd.read_excel(x)
        else:
            pause = pd.read_excel(x)
    os.chdir("/Users/arunabhmajumdar/Documents/all-india-dashboard-code/Code/")
    print (os.getcwd()) 
    
    pause_list = pause["user_id"].tolist()
    l = ['ea0fc8df-b4e5-4b32-998f-46f862638b9d',
    '07dfc634-bc12-41e3-9e24-68e9fc2a518c',
    '923410c2-1fbb-492a-8049-c50ff505f149']
    pause_list = pause_list+l
    pause_list = ["'" + str(x) + "'" for x in pause_list]
    pause_placeholders= ','.join(pause_list)
    pause_query = "update ems.employees set paused = True where user_id in (%s)" % pause_placeholders
    cursor.execute(pause_query)
    connection.commit()
    
    
    unpause_list = unpause["user_id"].tolist()
    unpause_list = ["'" + str(x) + "'" for x in unpause_list]
    unpause_placeholders= ','.join(unpause_list)
    unpause_query = "update ems.employees set paused = False where user_id in (%s)" % unpause_placeholders
    cursor.execute(unpause_query)
    connection.commit()
    print (os.getcwd())
    print ("Completed")

# except:
#     print ("Bypassed")
#     pass

except Exception as e:
    # Just print(e) is cleaner and more likely what you want,
    # but if you insist on printing message specifically whenever possible...
    if hasattr(e, 'message'):
        print(e.message)
    else:
        print(e)



print ("Connecting to Big Query Table")
query_string = "select * FROM `data-warehouse-india.Code_internal.Run_times_code`"
code__times = pd.read_gbq(query_string, project_id="data-warehouse-india")

c = code__times[code__times["Code"]==code_name]


code__times.at[c.index.tolist()[-1], "Run_time"] = time.time() - start_1
code__times.at[c.index.tolist()[-1], "time_updated"] = datetime.datetime.now()

code__times["time_updated"] = pd.to_datetime(code__times["time_updated"])
code__times = code__times.sort_values("time_updated")

code__times_bq = bq_cleaner(code__times)
pandas_gbq.to_gbq(code__times_bq, destination_table="Code_internal.Run_times_code", project_id="data-warehouse-india", if_exists="replace")






print (time.time() - start_1)



