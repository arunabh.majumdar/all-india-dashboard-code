# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

code_name = "time_update"
print ("Importing all packages and Google BQ credentials files")
print ("Time update code running now")
import warnings
import datetime
warnings.filterwarnings("ignore")
import pandas as pd,os
import base64
import boto3
import time
import pandas as pd, os
from oauth2client.service_account import ServiceAccountCredentials
import gspread
import gspread_dataframe as gd
import psycopg2
import df2gspread as d2g
import pandas as pd
import warnings
warnings.filterwarnings("ignore")
import numpy as np
import re
import calendar
import os
import json
import time
print ("Changing directory to Code to aid ease of access to the various jsons and xlsx")
print(os.getcwd())
os.chdir("..")
time.sleep(2)
print ("Changed Directory")
print (os.getcwd())
start_1 = time.time()
import numpy as np
import pytz
my_timezone = pytz.timezone('Asia/Calcutta')
import ast
import gzip
from datetime import timedelta
from currency_converter import CurrencyConverter
from google.oauth2 import service_account
from google.cloud import bigquery
import pandas_gbq
import os, pandas as pd
os.getcwd()
KEY_PATH = "data-warehouse-india-84f5f8a775d1.json"
CREDS = service_account.Credentials.from_service_account_file(KEY_PATH)
bq_client = bigquery.Client(credentials=CREDS, project="data-warehouse-india")
import warnings
warnings.filterwarnings("ignore")


print ("Connecting to Dynamo DB using boto3")
start = time.time()
print ("starting run")
session = boto3.session.Session(profile_name="rain-india-production")
client = session.client("dynamodb")
dynamodb = boto3.resource("dynamodb")
print ("Connecting to Postgres using psycopg2")
connection = psycopg2.connect(user="rainadmin",
                                      password="Mudar123",
                                      host="localhost",
                                      port=55432,
                                      database="rain")
cursor = connection.cursor()
# Print PostgreSQL details
print("PostgreSQL server information")
print(connection.get_dsn_parameters(), "\n")
# cursor.itersize = 10000
cursor.execute("SELECT version();")
    # Fetch result
record = cursor.fetchone()
print("You are connected to - ", record, "\n")

print ("Function to download Postgres data and concert that to dataframe")
def dataframe_generator(query):
    cursor.execute(query)
    print('Read table in PostgreSQL')
    data = cursor.fetchall()
    cols = []
    for elt in cursor.description:
        cols.append(elt[0])
    df= pd.DataFrame(data = data, columns=cols)
    return df

print ("Function to clean dataframe to include only data post Sept 1, 2021")
def clean(df):
    df["created_at"] = df["created_at"].dt.date.astype(str)
    df = df[df["created_at"]>"2021-08-31"]
    return df
print (time.time() - start)


print ("Function to clean dataframe to upload to BQ, BQ doesn't take spaces, dashes etc;")
def bq_cleaner(df):
    new_cols = []
    l = df.columns.tolist()
    for x in l:
        x = x.replace("(","_")
        x = x.replace(" ","_")
        x = x.replace(")","_")
        new_cols.append(x)
    df.columns = new_cols
    df = df.astype(str)
    return df


last_updated = datetime.datetime.now()
hour = last_updated.strftime("%H")
minute = last_updated.strftime("%M")
second = last_updated.strftime("%p")

hour = int(hour)
if hour>12:
    hour = hour - 12
else:
    hour = hour
    
hour = str(hour)
last_updated = str(hour)+":"+str(minute)+" "+str(second)
date_updated = pd.to_datetime("today").date().strftime("%B %d")
lt = {}
lt["last_updated"] = date_updated+", "+last_updated
ltr = pd.DataFrame(lt.values(), columns=["last_updated"])


ltr_bq = bq_cleaner(ltr)
pandas_gbq.to_gbq(ltr_bq, destination_table="Processed_data.Last_updated_time", project_id="data-warehouse-india", if_exists="replace")




print ("Connecting to Big Query Table")
query_string = "select * FROM `data-warehouse-india.Code_internal.Run_times_code`"
code__times = pd.read_gbq(query_string, project_id="data-warehouse-india")

c = code__times[code__times["Code"]==code_name]


code__times.at[c.index.tolist()[-1], "Run_time"] = time.time() - start_1
code__times.at[c.index.tolist()[-1], "time_updated"] = datetime.datetime.now()

code__times["time_updated"] = pd.to_datetime(code__times["time_updated"])
code__times = code__times.sort_values("time_updated")

code__times_bq = bq_cleaner(code__times)
pandas_gbq.to_gbq(code__times_bq, destination_table="Code_internal.Run_times_code", project_id="data-warehouse-india", if_exists="replace")



print ("\n")
print ("Total Run Time: "+str(time.time() - start_1))
