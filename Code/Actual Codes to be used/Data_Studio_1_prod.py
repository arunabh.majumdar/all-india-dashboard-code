
code_name = "Data_Studio_1_prod"
print ("New Data Studio Allwithdrawals")
print ("Importing all packages and Google BQ credentials files")
import datetime
import warnings
warnings.filterwarnings("ignore")
import pandas as pd,os
import base64
import boto3
import time
import pandas as pd, os
from oauth2client.service_account import ServiceAccountCredentials
import gspread
import gspread_dataframe as gd
import psycopg2
import df2gspread as d2g
import pandas as pd
import warnings
warnings.filterwarnings("ignore")
import numpy as np
import re
import calendar
import os
import json
import time
print ("Changing directory to Code to aid ease of access to the various jsons and xlsx")
print(os.getcwd())
os.chdir("..")
time.sleep(2)
print ("Changed Directory")
print (os.getcwd())
start_1 = time.time()
import numpy as np
import pytz
my_timezone = pytz.timezone('Asia/Calcutta')
import ast
import gzip
from datetime import timedelta
from currency_converter import CurrencyConverter
from google.oauth2 import service_account
from google.cloud import bigquery
import pandas_gbq
import os, pandas as pd
os.getcwd()
KEY_PATH = "data-warehouse-india-84f5f8a775d1.json"
CREDS = service_account.Credentials.from_service_account_file(KEY_PATH)
bq_client = bigquery.Client(credentials=CREDS, project="data-warehouse-india")
import warnings
warnings.filterwarnings("ignore")


print ("Connecting to Dynamo DB using boto3")
start = time.time()
print ("starting run")
session = boto3.session.Session(profile_name="rain-india-prod")
client = session.client("dynamodb")
dynamodb = boto3.resource("dynamodb")
print ("Connecting to Postgres using psycopg2")
connection = psycopg2.connect(user="rainadmin",
                                      password="Mudar123",
                                      host="localhost",
                                      port=55432,
                                      database="rain")
cursor = connection.cursor()
# Print PostgreSQL details
print("PostgreSQL server information")
print(connection.get_dsn_parameters(), "\n")
# cursor.itersize = 10000
cursor.execute("SELECT version();")
    # Fetch result
record = cursor.fetchone()
print("You are connected to - ", record, "\n")

print ("Function to download Postgres data and concert that to dataframe")
def dataframe_generator(query):
    cursor.execute(query)
    print('Read table in PostgreSQL')
    data = cursor.fetchall()
    cols = []
    for elt in cursor.description:
        cols.append(elt[0])
    df= pd.DataFrame(data = data, columns=cols)
    return df

print ("Function to clean dataframe to include only data post Sept 1, 2021")
def clean(df):
    df["created_at"] = df["created_at"].dt.date.astype(str)
    df = df[df["created_at"]>"2021-08-31"]
    return df
print (time.time() - start)


print ("Function to clean dataframe to upload to BQ, BQ doesn't take spaces, dashes etc;")
def bq_cleaner(df):
    new_cols = []
    l = df.columns.tolist()
    for x in l:
        x = x.replace("(","_")
        x = x.replace(" ","_")
        x = x.replace(")","_")
        new_cols.append(x)
    df.columns = new_cols
    df = df.astype(str)
    return df




print ("Module 1 - Quess Landing Page")
print ("Downloading data dump from AWS_QUESS_LANDING_PAGE S3 dump from the most recent folder called json")
print (os.getcwd())
print ("ok")
#     time.sleep(3)
print ("Dynamo Update Landing Page")
  # time.sleep(60)
print ("starting run")

print ("Changing directory to json")
os.chdir("..")
os.chdir("AWS_Quess_Landing_Page")
os.chdir("AWSDynamoDB/")
files = [x for x in os.listdir(os.getcwd())]
newest = max(files , key = os.path.getctime)
print ("Recently modified Docs",newest)
os.chdir(newest)
# os.chdir("json")
os.chdir("data")

all_files = []
for x in os.listdir():
    print (x)
    f=gzip.open(x,'r')
    file_content=f.read()
    all_files.append(file_content)
json_strings = []
for x in all_files:
    my_json = x.decode('utf-8').replace("'",'"')
    json_strings.append(my_json)
all_cleaned =[]
for x in json_strings:
    all_cleaned.append(x.split("\n"))
c = []
c1 = []
c2 = []
c3 = []
try:
    for x in all_cleaned[0]:
        c.append(ast.literal_eval(x))
except:
    pass
try:
    for x in all_cleaned[1]:
        c1.append(ast.literal_eval(x))
except:
    pass
try:
    for x in all_cleaned[2]:
        c2.append(ast.literal_eval(x))
except:
    pass
try:
    for x in all_cleaned[3]:
        c3.append(ast.literal_eval(x))
except:
    pass
c = c+c1+c2+c3
employee_id = []
updated_at = []
created_at = []
phone_number = []
status = []
for x in c:
#     print (x)
    employee_id.append(x["Item"]["employee_id"])
    updated_at.append(x["Item"]["updated_at"])
    phone_number.append(x["Item"]["phone_number"])
    created_at.append(x["Item"]["created_at"])
    status.append(x["Item"]["status"])
df = pd.DataFrame()
df["employee_id"] = employee_id
df["phone_number"] = phone_number
df["status"] = status
df["created_at"] = created_at
df["updated_at"] = updated_at
employee_id = []
phone_number = []
status = []
created_at = []
updated_at = []
for x in range(df.shape[0]):
    employee_id.append(df["employee_id"].iloc[x]["S"])
    phone_number.append(df["phone_number"].iloc[x]["S"])
    status.append(df["status"].iloc[x]["S"])
    created_at.append(df["created_at"].iloc[x]["S"])
    updated_at.append(df["updated_at"].iloc[x]["S"])
print (len(employee_id))
print (len(phone_number))
print (len(status))
print (len(created_at))
print (len(updated_at))

df["employee_id"] = employee_id
df["phone_number"] = phone_number
df["status"] = status
df["created_at"] = created_at
df["updated_at"] = updated_at



df = df.sort_values("created_at")


df["organization_id"]="Quess"
os.chdir("..")
os.chdir("..")
os.chdir("..")
os.chdir("..")
os.chdir("Code/")


print ("Uploading Landing page data to 2 Google Sheets - CS-OPS (New Quess Landing Page "+
        "and Data Studio Final Dashboard Sheet 3 ")

existing = df.copy()
existing["created_at"] = pd.to_datetime(existing["created_at"]).dt.date
landing_page = existing.copy()
print (time.time() - start)


print ("Uploading Landing page data to BQ")
landing_page_bq = bq_cleaner(df)
pandas_gbq.to_gbq(landing_page_bq, destination_table="Landing_Page.Quess_Landing_Page", project_id="data-warehouse-india", if_exists="replace")



print ("Connecting to Big Query Table")
query_string = "select * FROM `data-warehouse-india.Processed_data.withdrawals-txns_without_bank`"
all_rows = pd.read_gbq(query_string, project_id="data-warehouse-india")
all_rows_1 = all_rows.copy()
all_rows_1["disbursal_txn__date"] = pd.to_datetime(all_rows_1["disbursal_txn__date"]).dt.date.astype(str)
all_rows_1= all_rows_1[['user_id','birth_date', "email", 'employer_id', 'Gender','organization_id','monthly_salary',
           'tid','disbursal_txn__date','Total_Amount', 'processing_fees', 
       'overall_limit', 'Sanctioned_Loan_Limit', "lookup_name"]]
all_rows_1["organization_id"].replace({"c2a6a007-e625-456f-8c36-92cd2654c971": 'Quess',
                                              "916227f6-cb69-46ec-8cb1-a735ed98f2c4": 'D2C Org', 
                                             "d779558a-09cc-4920-9f39-d8409c8f0728":"B2B Test", 
                                             "0706c2e4-5153-4429-9645-a6d0c4a26a04":"HCL", 
                                    "6740ddac-09fa-4c0b-9cad-a261cc23997e":"Cloudnine", 
                                    "2f57a3bb-26a6-4cd6-953e-ce61fe607e6a":"Wissen Infotech", 
                                    "a8957481-3705-4d7f-b521-491a7e859f47":"Support.com", 
                                    "752927b6-2655-4760-8fea-33e7417b2a75":"Sutherland"},inplace=True)



nisa = all_rows_1[all_rows_1["employer_id"]=="e4e7c080-45a9-4132-82b3-533915e26a5d"]
sitel = all_rows_1[all_rows_1["employer_id"]=="19d26c3f-c797-4b90-a471-9b56996f8be8"]
trianz = all_rows_1[all_rows_1["employer_id"]=="323c383a-6422-43ed-8c6f-816fbe9635ab"]
mankind_pharma = all_rows_1[all_rows_1["employer_id"]=="38481b24-a474-40b3-903b-6da906401093"]

all_rows_new_b2b2c = pd.concat([nisa, sitel, trianz, mankind_pharma])

all_rows_new_b2b2c = pd.concat([nisa, sitel, trianz, mankind_pharma])
part_1 = all_rows_1[~all_rows_1["tid"].isin(all_rows_new_b2b2c["tid"])]
all_rows_new_b2b2c["New_B2B2C_Client"] = all_rows_new_b2b2c["employer_id"]

all_rows_new_b2b2c["New_B2B2C_Client"].replace({"e4e7c080-45a9-4132-82b3-533915e26a5d": 'Nisa',
                                              "19d26c3f-c797-4b90-a471-9b56996f8be8": 'Sitel', 
                                             "323c383a-6422-43ed-8c6f-816fbe9635ab":"Trianz", 
                                             "38481b24-a474-40b3-903b-6da906401093":"Mankind Pharma"},inplace=True)

part_1["New_B2B2C_Client"] =''

all_rows_1 = pd.concat([part_1, all_rows_new_b2b2c])












gender_dict = {'Male':"male", 'MALE':"male", 'male':"male", 'Female':"female", 'M':"male", ' MALE ':"male", 
               'F':"female", 'female':"female"}

all_rows_1["Total_Amount"] = all_rows_1["Total_Amount"].astype(float)
all_rows_1["processing_fees"] = all_rows_1["processing_fees"].astype(float)
all_rows_1["Yield"] = round(all_rows_1["processing_fees"]/all_rows_1["Total_Amount"], 2)*100

all_rows_1["Gender"] = all_rows_1["Gender"].map(gender_dict)

all_rows_1["age"] = (pd.to_datetime("today") - pd.to_datetime(all_rows_1["birth_date"])).dt.days

all_rows_1["age"] = round(all_rows_1["age"]/365,0)

bins = [0, 20, 25, 30, 35, 40, 50, 60,100]
labels = ["under 20","20-25","25-30","30-35","35-40","40-50", "50-60", "60+"]
all_rows_1['binned_age'] = pd.cut(all_rows_1['age'], bins=bins, labels=labels)

all_rows_1["monthly_salary"] = all_rows_1["monthly_salary"].astype(float)

bins = [0, 15000, 30000, 50000, 80000, 100000, 500000]
labels = ["under 15k","15k-30k","30k-50k","50k-80k","80k-100k","100k+"]
all_rows_1['binned_salary'] = pd.cut(all_rows_1['monthly_salary'], bins=bins, labels=labels)

os.chdir("..")

os.chdir("Code")
print (os.getcwd())







all_rows_1 = all_rows_1[(all_rows_1["lookup_name"]!="quees corp ltd")&(all_rows_1["lookup_name"]!="quess corp limited")&(all_rows_1["lookup_name"]!="rainpay")]

mn = pd.to_datetime(all_rows_1["disbursal_txn__date"]).dt.month.tolist()
month_name = []
for x in mn:
    month_name.append(calendar.month_name[x])
print (len(month_name))
all_rows_1["Disbursal_month"] = month_name
all_rows_1.shape

c = CurrencyConverter()
dollars_tot_amount = []
tot = all_rows_1["Total_Amount"].tolist()
for x in tot:
    dollars_tot_amount.append(c.convert(x,"INR","USD"))
dollars_processing = []
pro = all_rows_1["processing_fees"].tolist()
for y in pro:
    dollars_processing.append(c.convert(y,"INR", "USD"))
    

all_rows_1["Total amount in dollars"] = dollars_tot_amount
all_rows_1["Processing fees in dollars"] = dollars_processing

a = pd.to_datetime(all_rows_1["disbursal_txn__date"]).dt.month.tolist()
c1 = []
for m in a:
    c1.append(calendar.month_name[m])
all_rows_1["month"] = c1





print ("Uploading binned salary, binned age, All withdrawals and cumulative sheet on Data Studio to All Withdrawals")

sheet1_bq = bq_cleaner(all_rows_1.copy())
pandas_gbq.to_gbq(sheet1_bq, destination_table="Data_Studio.All_withdrawals", project_id="data-warehouse-india", if_exists="replace")







print ("Starting KYC_METRICS")
query = """select * from iam.users u ;"""
iam = dataframe_generator(query)
iam = clean(iam)
iam.rename(columns={"id":"user_id"},inplace=True)
phone_number = iam["phone_number"].astype(str).tolist()
phone_numbers = []
for x in phone_number:
    phone_numbers.append(re.sub("[^0-9]", "", x))
phone_number_2 =[]
for x in phone_numbers:
    if len(x)>10:
        phone_number_2.append(x[2:])
    else:
        phone_number_2.append(x)
iam["phone_number"] = phone_number_2
iam = iam[["user_id", "full_name", "status", "email", "phone_number", "created_at", "metadata"]]
gender=[]
birth_date=[]

for i in range(0,iam.shape[0]):
    try:
        gender.append(iam["metadata"].iloc[i]["gender"])
    except KeyError as e:
        print("KeyError:",e)
        gender.append("")
    try:
        birth_date.append(iam["metadata"].iloc[i]["birth_date"])
    except KeyError as e:
        print("KeyError:",e)
        birth_date.append("")
# for i in range(0,iam.shape[0]):
#     gender.append(iam["metadata"].iloc[i]["gender"])
#     birth_date.append(iam["metadata"].iloc[i]["birth_date"])

iam["Gender"] = gender
iam["birth_date"] = birth_date
iam.drop(["metadata"],1,inplace=True)
start = time.time()
query = """select * from ems.employees e ;"""
ems_employees = dataframe_generator(query)
ems_employees = ems_employees[['id', 'user_id', 'employer_id', 'status', 'first_name', 'last_name',
        'created_at', 'updated_at', 'group_id', 'hr_employee_code', 'organization_id']]
ems_employees = ems_employees.rename(columns={'id': 'employee_id'})
ems_employees= clean(ems_employees)
print (time.time() - start)
ems_employees = ems_employees[["employee_id", "user_id", "employer_id"]]
start = time.time()
query = """select * from xorg.employers e;"""
xorg = dataframe_generator(query)
xorg = xorg[["id","organization_id", "lookup_name"]]
xorg["lookup_name"] = xorg["lookup_name"].str.lower()
xorg.rename(columns={"id":"employer_id"},inplace=True)
print (time.time() - start)

start = time.time()
xorg["organization_id"].replace({"c2a6a007-e625-456f-8c36-92cd2654c971": 'Quess',
                                              "916227f6-cb69-46ec-8cb1-a735ed98f2c4": 'D2C Org', 
                                             "d779558a-09cc-4920-9f39-d8409c8f0728":"B2B Test", 
                                             "0706c2e4-5153-4429-9645-a6d0c4a26a04":"HCL", 
                                    "6740ddac-09fa-4c0b-9cad-a261cc23997e":"Cloudnine", 
                                    "2f57a3bb-26a6-4cd6-953e-ce61fe607e6a":"Wissen Infotech", 
                                    "a8957481-3705-4d7f-b521-491a7e859f47":"Support.com", 
                                    "752927b6-2655-4760-8fea-33e7417b2a75":"Sutherland"},inplace=True)
print (time.time() - start)

ems_xorg= pd.merge(ems_employees, xorg, on = "employer_id")
iam_ems_xorg = pd.merge(iam, ems_xorg, on = "user_id", how = "left")
iam_ems_xorg = iam_ems_xorg[['user_id', 'created_at', 'organization_id','lookup_name', 'phone_number']]


query = """select * from risk.user_risk_verifications urv; """
cv = dataframe_generator(query)
cv = clean(cv)

cv.rename(columns={"score":"Approved"},inplace=True)

os.chdir("..")
os.chdir("AWS_Data/")
rootdir = os.getcwd()
files_dump =[]
for subdir, dirs, files in os.walk(rootdir):
    for file in files:
#             print(os.path.join(subdir, file))
        if file.endswith("json"):
            files_dump.append(os.path.join(subdir, file))
ff = []
for x in files_dump:
    fff = {}
    f = open(x)
    try:
        data = json.load(f)
    except:
        data = "Json Failure - Issue at our AWS end"
    fff["user_id"] = str(f).split("/")[-2]
    fff["data"] = data
    ff.append(fff)
ffff = pd.DataFrame(ff)
os.chdir("..")
os.chdir("Code")

aws_approved = []
for x in ffff["data"]:
    if x=="Yes":
        aws_approved.append(True)
    else:
        aws_approved.append(False)
ffff["Aws Approved"] = aws_approved
cv = pd.merge(cv,ffff, on = "user_id", how = "left")


cv= pd.merge(cv, iam_ems_xorg[["user_id", "organization_id", "lookup_name"]], on = "user_id", how = "left")
cv = cv[["user_id", "Approved"]]
iam_ems_xorg = pd.merge(iam_ems_xorg, cv,on = "user_id",how = "left")










start = time.time()
query = """select * from kyc.documents kyc ;"""
kyc = dataframe_generator(query)
kyc = clean(kyc)
print (time.time() - start)
uid = kyc["user_id"].unique().tolist()

selfie_only= []
selfie_and_pan = []
all_3=[]

for x in uid:
    d = kyc[(kyc["user_id"]==x)&(kyc['verified']==True)&(kyc['approved']==True)]["document_type"].value_counts().index.tolist()
    if "SILENTLIVENESS" in d:
        selfie_only.append(x)
    if "SILENTLIVENESS" in d and "PAN" in d and "PANNSDL" in d:
        selfie_and_pan.append(x)
    if "SILENTLIVENESS" in d and "PAN" in d and "PANNSDL" in d and ("AADHAAR" in d or "VOTERID" in d):
        all_3.append(x)



# for x in uid:
#     d = kyc[kyc["user_id"]==x]["document_type"].value_counts().index.tolist()
#     if "SILENTLIVENESS" in d:
#         selfie_only.append(x)
#     if "SILENTLIVENESS" in d and "PAN" in d:
#         selfie_and_pan.append(x)
#     if "SILENTLIVENESS" in d and "PAN" in d and "AADHAAR" in d:
#         all_3.append(x)
print (time.time() - start)


stage_1 = pd.DataFrame(selfie_only, columns=["user_id"])
stage_1["Stage_1"] = True
stage_1

kyc = pd.merge(kyc,stage_1, on = "user_id", how = "left")

stage_2 = pd.DataFrame(selfie_and_pan, columns=["user_id"])
stage_2["Stage_2"] = True
stage_2

kyc = pd.merge(kyc,stage_2, on = "user_id", how = "left")

stage_3 = pd.DataFrame(all_3, columns=["user_id"])
stage_3["Stage_3"] = True
stage_3

kyc = pd.merge(kyc,stage_3, on = "user_id", how = "left")

kyc1 = kyc[["user_id", 'created_at', 'verified', 'document_type', 'Stage_1', 'Stage_2', 'Stage_3']]
kyc1 = kyc1.groupby("user_id").last().reset_index()
kyc1.columns = ["user_id", "kyc_hit_date", "verified", "document_type", "Stage_1", "Stage_2", "Stage_3"]
iam_ems_xorg = pd.merge(iam_ems_xorg, kyc1, on = "user_id", how = "left")
iam_ems_xorg['kyc_hit'] = np.where(iam_ems_xorg["kyc_hit_date"].notnull(), 'Yes', 'No')
def verified_flag(df):
    
    if (df['kyc_hit']=="No"):
        return 'No Hit'
    elif (df["verified"]==True):
        return "Karza Approved"
    elif (df["verified"]==False):
        return "Karza Rejected"
    

iam_ems_xorg['Karza Status'] = iam_ems_xorg.apply(verified_flag, axis = 1)
uid = iam_ems_xorg["user_id"].unique().tolist()
all_d = pd.read_pickle("Events.pkl")
all_d = all_d[all_d["action"]=="risk_verification.kyc_rejected"]
iam_ems_xorg = pd.merge(iam_ems_xorg,all_d, on = "user_id", how = "left")
reasons = iam_ems_xorg[iam_ems_xorg["reason"].notnull()]
reasons = reasons[["user_id", "reason"]]
reasons["reason"] = reasons["reason"].str.split(",").tolist()
m = pd.DataFrame(reasons["reason"].tolist())
reasons = reasons.reset_index(drop=True)
reasons = pd.concat([reasons,m],1)


pin_1 = []
pan_1 = []
aadhar_1 = []
voter_1 = []
reasons_1 = reasons[reasons[0].notnull()]
for x,y in zip(reasons_1["user_id"].tolist(), reasons_1[0].tolist()):
    pin_d = {}
    pan_d = {}
    aad_d = {}
    vot_d = {}
    if "PinCodeMismatch" in y:
        pin_d["user_id"] = x
        pin_1.append(pin_d)
    elif "PANNameMismatch" in y:
        pan_d["user_id"] = x
        pan_1.append(pan_d)
    elif "AadhaarNameMismatch" in y:
        aad_d["user_id"] = x
        aadhar_1.append(aad_d)
    elif "VoterIDNameMismatch" in y:
        vot_d["user_id"] = x
        voter_1.append(vot_d)
pin_2 = []
pan_2 = []
aadhar_2 = []
voter_2 = []
reasons_2 = reasons[reasons[1].notnull()]
for x,y in zip(reasons_2["user_id"].tolist(), reasons_2[1].tolist()):
    pin_d = {}
    pan_d = {}
    aad_d = {}
    vot_d = {}
    if "PinCodeMismatch" in y:
        pin_d["user_id"] = x
        pin_2.append(pin_d)
    elif "PANNameMismatch" in y:
        pan_d["user_id"] = x
        pan_2.append(pan_d)
    elif "AadhaarNameMismatch" in y:
        aad_d["user_id"] = x
        aadhar_2.append(aad_d)
    elif "VoterIDNameMismatch" in y:
        vot_d["user_id"] = x
        voter_2.append(vot_d)
        
pin_3 = []
pan_3 = []
aadhar_3 = []
voter_3 = []
reasons_3 = reasons[reasons[2].notnull()]
for x,y in zip(reasons_3["user_id"].tolist(), reasons_3[2].tolist()):
    pin_d = {}
    pan_d = {}
    aad_d = {}
    vot_d = {}
    if "PinCodeMismatch" in y:
        pin_d["user_id"] = x
        pin_3.append(pin_d)
    elif "PANNameMismatch" in y:
        pan_d["user_id"] = x
        pan_3.append(pan_d)
    elif "AadhaarNameMismatch" in y:
        aad_d["user_id"] = x
        aadhar_3.append(aad_d)
    elif "VoterIDNameMismatch" in y:
        vot_d["user_id"] = x
        voter_3.append(vot_d)
        
pin = pin_1+pin_2+pin_3
pan = pan_1+pan_2+pan_3
aadhar = aadhar_1+aadhar_2+aadhar_3
voter = voter_1+voter_2+voter_3

pin = pd.DataFrame(pin)
pin["Pincode mismatch"] = "Yes"
pan = pd.DataFrame(pan)
pan["PAN Mismatch"] = "Yes"
aadhar = pd.DataFrame(aadhar)
aadhar["Aadhaar mismatch"] = "Yes"
voter = pd.DataFrame(voter)
voter["Voter mismatch"] = "Yes"

reasons = pd.merge(reasons, pin, on = "user_id", how = "left")
reasons = pd.merge(reasons, pan, on = "user_id", how = "left")
reasons = pd.merge(reasons, voter, on = "user_id", how = "left")
reasons = pd.merge(reasons, aadhar, on = "user_id", how = "left")

reasons = reasons[["user_id", "Pincode mismatch", "PAN Mismatch", "Aadhaar mismatch", "Voter mismatch"]]
iam_ems_xorg = pd.merge(iam_ems_xorg, reasons, on = "user_id", how = "left")








iam_ems_xorg_bq = bq_cleaner(iam_ems_xorg)
pandas_gbq.to_gbq(iam_ems_xorg_bq, destination_table="Processed_data.KYC_Metrics_DataStudio", project_id="data-warehouse-india", if_exists="replace")



print ("Start Withdrawal status code")
query = """select * from bnk.transactions t  ;"""
txns_status = dataframe_generator(query)
txns_status = clean(txns_status)
print ("Capture all failed, pending and incomplete txns")
# txns = txns[txns["status"]!="COMPLETE"]
txns_status.rename(columns={"entity_id":"user_id"},inplace=True)
txns_status = txns_status[["user_id", "created_at","error_message", "status", "amount", "type"]]
txns_status = txns_status[txns_status["type"]=="PUSH"]

txns_bq = bq_cleaner(txns_status)
pandas_gbq.to_gbq(txns_bq, destination_table="Processed_data.Withdrawal_status_tracker", project_id="data-warehouse-india", if_exists="replace")





print ("OTP Sheet update")

os.getcwd()
os.chdir("..")
os.chdir("otp_S3_Kinesis_Dump")

def getListOfFiles(dirName):
    # create a list of file and sub directories 
    # names in the given directory 
    listOfFile = os.listdir(dirName)
    allFiles = list()
    # Iterate over all the entries
    for entry in listOfFile:
        # Create full path
        fullPath = os.path.join(dirName, entry)
        # If entry is a directory then get the list of files in this directory 
        if os.path.isdir(fullPath):
            allFiles = allFiles + getListOfFiles(fullPath)
        else:
            allFiles.append(fullPath)
                
    return allFiles


all_dumps = []
try:
    for x in getListOfFiles(os.getcwd()):
        if not x.endswith(".DS_Store"):
            print (x)
            all_dumps.append(x)
except:pass
os.chdir("..")
os.chdir("Code")
ad = []
try:
    for x in all_dumps:
        y = pd.read_table(x, header=None)[0].tolist()
        y = ast.literal_eval(y[0])
        print (y)
        ad.append(y)
except:pass

timestamps = []
delivery_number = []
sms_type = []
response = []
status = []

for x in ad:
    timestamps.append(x["notification"]["timestamp"])
    delivery_number.append(x["delivery"]["destination"])
    sms_type.append(x["delivery"]["smsType"])
    response.append(x["delivery"]["providerResponse"])
    status.append(x["status"])
    
final = pd.DataFrame()
final["timestamp"] = timestamps
final["Mobile number"] = delivery_number
final["sms_type"] = sms_type
final["Response"] = response
final["status"] = status
final["timestamp"] = pd.to_datetime(final["timestamp"])
final["timestamp"] = final["timestamp"].dt.tz_localize(pytz.utc).dt.tz_convert(my_timezone)
start = time.time()


otp_bq = bq_cleaner(final.copy())
pandas_gbq.to_gbq(otp_bq, destination_table="Processed_data.OTP_data_studio", project_id="data-warehouse-india", if_exists="replace")




query = """select * from ems.work_period_balances wpb ;"""
wpb = dataframe_generator(query)
wpb = clean(wpb)
wpb["available_amount"] = wpb["available_amount"]/100
wpb = wpb[["employee_id", "available_amount"]]
query = """select * from ems.employees ;"""
ems = dataframe_generator(query)
ems = clean(ems)
ems.rename(columns = {"id":"employee_id"},inplace  = True)
ems = ems[["employee_id", "user_id"]]
wpb = pd.merge(wpb, ems, on = "employee_id", how = "left")
wpb = wpb[wpb['user_id'].notnull()]
wpb = wpb.groupby("user_id").last().reset_index()
wpb_bq = bq_cleaner(wpb)
pandas_gbq.to_gbq(wpb_bq, destination_table="Processed_data.Current_Accrued", project_id="data-warehouse-india", if_exists="replace")







import datetime

print ("Connecting to Big Query Table")
query_string = "select * FROM `data-warehouse-india.Code_internal.Run_times_code`"
code__times = pd.read_gbq(query_string, project_id="data-warehouse-india")

c = code__times[code__times["Code"]==code_name]


code__times.at[c.index.tolist()[-1], "Run_time"] = time.time() - start_1
code__times.at[c.index.tolist()[-1], "time_updated"] = datetime.datetime.now()

code__times["time_updated"] = pd.to_datetime(code__times["time_updated"])
code__times = code__times.sort_values("time_updated")

code__times_bq = bq_cleaner(code__times)
pandas_gbq.to_gbq(code__times_bq, destination_table="Code_internal.Run_times_code", project_id="data-warehouse-india", if_exists="replace")

print ("\n")
print ("Total Run Time: "+str(time.time() - start_1))


























print ("/n")
print (time.time() - start_1)











































