#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 27 11:37:11 2022

@author: arunabhmajumdar
"""
code_name = "Withdrawals_prod"
import json
import datetime
from datetime import timedelta
import time
start_1 = time.time()
import re
import os
print ("Changing directory to Code to aid ease of access to the various jsons and xlsx")
print(os.getcwd())
os.chdir("..")
time.sleep(2)
print ("Changed Directory")
print (os.getcwd())
import psycopg2
import gspread
import df2gspread as d2g
import pandas as pd
import warnings
warnings.filterwarnings("ignore")
from oauth2client.service_account import ServiceAccountCredentials
import gspread
import gspread_dataframe as gd

import pytz
my_timezone = pytz.timezone('Asia/Calcutta')
from google.oauth2 import service_account
from google.cloud import bigquery
import pandas_gbq
import os, pandas as pd
os.getcwd()
KEY_PATH = "data-warehouse-india-84f5f8a775d1.json"
CREDS = service_account.Credentials.from_service_account_file(KEY_PATH)
bq_client = bigquery.Client(credentials=CREDS, project="data-warehouse-india")
import warnings
warnings.filterwarnings("ignore")
import numpy as np



connection = psycopg2.connect(user="rainadmin",
                                  password="Mudar123",
                                  host="localhost",
                                  port=55432,
                                  database="rain")

    # Create a cursor to perform database operations
cursor = connection.cursor()
# Print PostgreSQL details
print("PostgreSQL server information")
print(connection.get_dsn_parameters(), "\n")

cursor.execute("SELECT version();")
    # Fetch result
record = cursor.fetchone()
print("You are connected to - ", record, "\n")

def dataframe_generator(query):
    cursor.execute(query)
    print('Read table in PostgreSQL')
    data = cursor.fetchall()
    cols = []
    for elt in cursor.description:
        cols.append(elt[0])
    df= pd.DataFrame(data = data, columns=cols)
    return df
def clean(df):
    df["created_at"] = df["created_at"].dt.date.astype(str)
    df = df[df["created_at"]>"2021-08-31"]
    return df
import base64
import boto3
key_id = "be2bdea3-4ed9-48f2-8123-467fd62292fa"


print ("Connecting to Dynamo DB using boto3")
start = time.time()
print ("starting run")
session = boto3.session.Session(profile_name="rain-india-prod")
client = session.client("dynamodb", region_name="ap-south-1")
# dynamodb = boto3.resource("dynamodb")
print (list(client.list_tables()))

def hello_kms_bank(encrypted_text):
    client = session.client("kms", region_name="ap-south-1")
    encrypted_text = encrypted_text
    decrypted = client.decrypt(CiphertextBlob=base64.b64decode(encrypted_text))
    decrypted_text = decrypted["Plaintext"].decode("utf-8").split("\x1d")
    d = {}
    for c in range(len(decrypted_text)):
        d[decrypted_text[c].split("::")[0]] = decrypted_text[c].split("::")[1]
    dd = pd.DataFrame(d.items()).T
    dd.columns = dd.iloc[0]
    dd = dd[1:]
    return dd




query = """select * from iam.users u ;"""
iam = dataframe_generator(query)
iam = clean(iam)
iam.rename(columns={"id":"user_id"},inplace=True)
phone_number = iam["phone_number"].astype(str).tolist()
phone_numbers = []
for x in phone_number:
    phone_numbers.append(re.sub("[^0-9]", "", x))
phone_number_2 =[]
for x in phone_numbers:
    if len(x)>10:
        phone_number_2.append(x[2:])
    else:
        phone_number_2.append(x)
iam["phone_number"] = phone_number_2




query = """select * from ems.employees e ;"""
ems_employees = dataframe_generator(query)
ems_employees = ems_employees[['id', 'user_id', 'employer_id', 'status', 'first_name', 'last_name',
        'created_at', 'updated_at', 'group_id', 'hr_employee_code', 'organization_id', 'paused']]
ems_employees = ems_employees.rename(columns={'id': 'employee_id'})
ems_employees= clean(ems_employees)

old_values = ems_employees[ems_employees["employee_id"]=="eef0eefd-e2c6-494f-bf34-5fcb7c7ee22f"].values.tolist()[-1]
old_index = ems_employees[ems_employees["employee_id"]=="eef0eefd-e2c6-494f-bf34-5fcb7c7ee22f"].columns.tolist()
error_1 = dict(zip(old_index,old_values))
error_1["user_id"]="e797bb24-377f-48e3-9ee3-2dc28ea02827"
ems_employees = ems_employees.append(error_1, ignore_index = True)

old_values = ems_employees[ems_employees["employee_id"]=="d8815dbe-67e3-4b9a-a65e-663dd7e62e02"].values.tolist()[-1]
old_index = ems_employees[ems_employees["employee_id"]=="d8815dbe-67e3-4b9a-a65e-663dd7e62e02"].columns.tolist()
error_1 = dict(zip(old_index,old_values))
error_1["user_id"]="dc43dc6d-2075-4f9e-91f8-222bf962b58b"
ems_employees = ems_employees.append(error_1, ignore_index = True)

old_values = ems_employees[ems_employees["employee_id"]=="5f29d1ee-6a33-4662-8d9f-d47cbe2455ef"].values.tolist()[-1]
old_index = ems_employees[ems_employees["employee_id"]=="5f29d1ee-6a33-4662-8d9f-d47cbe2455ef"].columns.tolist()
error_1 = dict(zip(old_index,old_values))
error_1["user_id"]="ecde2d75-5132-4371-a716-e361883e835c"
ems_employees = ems_employees.append(error_1, ignore_index = True)

old_values = ems_employees[ems_employees["employee_id"]=="085bd49d-4993-402f-a12f-553be12eef6f"].values.tolist()[-1]
old_index = ems_employees[ems_employees["employee_id"]=="085bd49d-4993-402f-a12f-553be12eef6f"].columns.tolist()
error_1 = dict(zip(old_index,old_values))
error_1["user_id"]="e0180de9-e71c-4a01-8f6b-cfc93fa8f5bc"
ems_employees = ems_employees.append(error_1, ignore_index = True)

old_values = ems_employees[ems_employees["employee_id"]=="f05802ac-5933-4b29-9d65-0ea0569d332a"].values.tolist()[-1]
old_index = ems_employees[ems_employees["employee_id"]=="f05802ac-5933-4b29-9d65-0ea0569d332a"].columns.tolist()
error_1 = dict(zip(old_index,old_values))
error_1["user_id"]="cf387bc8-4d6d-4e7b-8e17-4a43655582ec"
ems_employees = ems_employees.append(error_1, ignore_index = True)

old_values = ems_employees[ems_employees["employee_id"]=="d1cf70ae-9e21-4338-95d8-bec899557c22"].values.tolist()[-1]
old_index = ems_employees[ems_employees["employee_id"]=="d1cf70ae-9e21-4338-95d8-bec899557c22"].columns.tolist()
error_1 = dict(zip(old_index,old_values))
error_1["user_id"]="65bf5ec8-ad63-4f62-a205-ae449800603c"
ems_employees = ems_employees.append(error_1, ignore_index = True)

old_values = ems_employees[ems_employees["employee_id"]=="174b9ecd-dde7-47ea-b357-9ab463a41d67"].values.tolist()[-1]
old_index = ems_employees[ems_employees["employee_id"]=="174b9ecd-dde7-47ea-b357-9ab463a41d67"].columns.tolist()
error_2 = dict(zip(old_index,old_values))
error_2["user_id"]="423889c4-5b1d-403d-aa25-e1bff51af34f"
ems_employees = ems_employees.append(error_2, ignore_index = True)

old_values = ems_employees[ems_employees["employee_id"]=="7a319ee3-3b3c-463d-a67a-7828d75aaba0"].values.tolist()[-1]
old_index = ems_employees[ems_employees["employee_id"]=="7a319ee3-3b3c-463d-a67a-7828d75aaba0"].columns.tolist()
error_2 = dict(zip(old_index,old_values))
error_2["user_id"]="ba1f1026-e586-414a-a3e8-dd8ad58c46b0"
ems_employees = ems_employees.append(error_2, ignore_index = True)

old_values = ems_employees[ems_employees["employee_id"]=="ae742c42-0641-4f9d-a51c-f79371e6a90b"].values.tolist()[-1]
old_index = ems_employees[ems_employees["employee_id"]=="ae742c42-0641-4f9d-a51c-f79371e6a90b"].columns.tolist()
error_2 = dict(zip(old_index,old_values))
error_2["user_id"]="effb1a1a-a43f-4630-bd8f-c6b967e8bcfd"
error_2["employer_id"] = "5b81572b-7422-405e-89a9-4af836bc3895"
ems_employees = ems_employees.append(error_2, ignore_index = True)

old_values = ems_employees[ems_employees["employee_id"]=="44875e29-fab1-4111-9493-858a028ddfc4"].values.tolist()[-1]
old_index = ems_employees[ems_employees["employee_id"]=="44875e29-fab1-4111-9493-858a028ddfc4"].columns.tolist()
error_2 = dict(zip(old_index,old_values))
error_2["user_id"]="0430eb0a-c986-43a0-b873-76963c437ca5"
ems_employees = ems_employees.append(error_2, ignore_index = True)

old_values = ems_employees[ems_employees["employee_id"]=="e0232b50-da50-47dd-86d2-e1bd9fa85185"].values.tolist()[-1]
old_index = ems_employees[ems_employees["employee_id"]=="e0232b50-da50-47dd-86d2-e1bd9fa85185"].columns.tolist()
error_2 = dict(zip(old_index,old_values))
error_2["user_id"]="7c85d049-12e1-4479-b661-3e25223d3b7e"
ems_employees = ems_employees.append(error_2, ignore_index = True)


#March 2nd, update
old_values = ems_employees[ems_employees["employee_id"]=="d2187e11-b8c8-452a-a885-87004f2e45eb"].values.tolist()[-1]
old_index = ems_employees[ems_employees["employee_id"]=="d2187e11-b8c8-452a-a885-87004f2e45eb"].columns.tolist()
error_2 = dict(zip(old_index,old_values))
error_2["user_id"]="1b5a030f-a5fc-47a8-bdeb-69cd727c4249"
ems_employees = ems_employees.append(error_2, ignore_index = True)


#March 8th, update
old_values = ems_employees[ems_employees["employee_id"]=="1b773dc5-a12a-42f6-94cb-9575cd03b169"].values.tolist()[-1]
old_index = ems_employees[ems_employees["employee_id"]=="1b773dc5-a12a-42f6-94cb-9575cd03b169"].columns.tolist()
error_2 = dict(zip(old_index,old_values))
error_2["user_id"]="208b3792-015f-4fd6-8fbc-6f7e7d8cab2e"
ems_employees = ems_employees.append(error_2, ignore_index = True)




#April 5th, update

old_values = ems_employees[ems_employees["employee_id"]=="8813c9fe-5a8c-47f4-b495-724e7fbfb4b1"].values.tolist()[-1]
old_index = ems_employees[ems_employees["employee_id"]=="8813c9fe-5a8c-47f4-b495-724e7fbfb4b1"].columns.tolist()
error_2 = dict(zip(old_index,old_values))
error_2["user_id"]="6e0ffe81-87fe-46b1-8173-3ab053b429b8"
ems_employees = ems_employees.append(error_2, ignore_index = True)


#April 13th, update
old_values = ems_employees[ems_employees["employee_id"]=="b2c596d3-6724-44b5-99ff-4c8b57e816e6"].values.tolist()[-1]
old_index = ems_employees[ems_employees["employee_id"]=="b2c596d3-6724-44b5-99ff-4c8b57e816e6"].columns.tolist()
error_2 = dict(zip(old_index,old_values))
error_2["user_id"]="24dfcbed-5f0c-45ab-99c1-af359ba9c756"
ems_employees = ems_employees.append(error_2, ignore_index = True)

#May 1st, update
old_values = ems_employees[ems_employees["employee_id"]=="df19f091-7153-4783-af86-3c3f81aacdff"].values.tolist()[-1]
old_index = ems_employees[ems_employees["employee_id"]=="df19f091-7153-4783-af86-3c3f81aacdff"].columns.tolist()
error_2 = dict(zip(old_index,old_values))
error_2["user_id"]="7d9f8923-7d03-40c8-8c4a-b196f3018124"
ems_employees = ems_employees.append(error_2, ignore_index = True)




#May 4th, update
old_values = ems_employees[ems_employees["employee_id"]=="a6b0025f-c569-40df-89c7-729307f027c2"].values.tolist()[-1]
old_index = ems_employees[ems_employees["employee_id"]=="a6b0025f-c569-40df-89c7-729307f027c2"].columns.tolist()
error_2 = dict(zip(old_index,old_values))
error_2["user_id"]="96fa2687-8557-485e-9b65-bafcf1c8f96f"
ems_employees = ems_employees.append(error_2, ignore_index = True)


#May 6th, update
old_values = ems_employees[ems_employees["employee_id"]=="1adc2d2a-23b0-4d75-8484-e167ce9a3c02"].values.tolist()[-1]
old_index = ems_employees[ems_employees["employee_id"]=="1adc2d2a-23b0-4d75-8484-e167ce9a3c02"].columns.tolist()
error_2 = dict(zip(old_index,old_values))
error_2["user_id"]="71b5a6fa-3b10-4b1d-b294-0389add02b82"
ems_employees = ems_employees.append(error_2, ignore_index = True)


#May 16th, update
old_values = ems_employees[ems_employees["employee_id"]=="46fe653e-ecb5-47b2-bdbe-eab62daaacde"].values.tolist()[-1]
old_index = ems_employees[ems_employees["employee_id"]=="46fe653e-ecb5-47b2-bdbe-eab62daaacde"].columns.tolist()
error_2 = dict(zip(old_index,old_values))
error_2["user_id"]="7d6195e9-91b9-4e45-9e74-32050b7172f7"
ems_employees = ems_employees.append(error_2, ignore_index = True)



#May 25th, update
old_values = ems_employees[ems_employees["employee_id"]=="0dd3e227-abe6-4017-b22b-705281a85e9e"].values.tolist()[-1]
old_index = ems_employees[ems_employees["employee_id"]=="0dd3e227-abe6-4017-b22b-705281a85e9e"].columns.tolist()
error_2 = dict(zip(old_index,old_values))
error_2["user_id"]="f79d195b-f46e-42c6-b52a-17c164866680"
ems_employees = ems_employees.append(error_2, ignore_index = True)





#Jul 19th, update
old_values = ems_employees[ems_employees["employee_id"]=="ff7144d9-588b-41b8-820a-524ae10b43c1"].values.tolist()[-1]
old_index = ems_employees[ems_employees["employee_id"]=="ff7144d9-588b-41b8-820a-524ae10b43c1"].columns.tolist()
error_2 = dict(zip(old_index,old_values))
error_2["user_id"]="2b789834-56e0-47a8-984a-9430b074b176"
ems_employees = ems_employees.append(error_2, ignore_index = True)


#Jul 22nd, update
old_values = ems_employees[ems_employees["employee_id"]=="c2f72ab4-972b-42f3-85b6-4d646ff3ee7e"].values.tolist()[-1]
old_index = ems_employees[ems_employees["employee_id"]=="c2f72ab4-972b-42f3-85b6-4d646ff3ee7e"].columns.tolist()
error_2 = dict(zip(old_index,old_values))
error_2["user_id"]="1fe96a2c-56fd-4925-811f-1eb83ea8c01e"
ems_employees = ems_employees.append(error_2, ignore_index = True)


#Jul 25th, update
old_values = ems_employees[ems_employees["employee_id"]=="69c8c04c-2f3a-4f3a-b173-aa970e5badd5"].values.tolist()[-1]
old_index = ems_employees[ems_employees["employee_id"]=="69c8c04c-2f3a-4f3a-b173-aa970e5badd5"].columns.tolist()
error_2 = dict(zip(old_index,old_values))
error_2["user_id"]="a2ede2e3-57b8-49f1-a928-e65ed2ac9c9d"
ems_employees = ems_employees.append(error_2, ignore_index = True)


iam_users = iam[["user_id", "full_name", "email","phone_number", "metadata","document_number"]]
iam_ems_employees = pd.merge(ems_employees,iam_users, on = "user_id")
iam_ems_employees = iam_ems_employees.sort_values("created_at")
gender=[]
birth_date=[]
for i in range(0,iam_ems_employees.shape[0]):
    gender.append(iam_ems_employees["metadata"].iloc[i]["gender"])
    birth_date.append(iam_ems_employees["metadata"].iloc[i]["birth_date"])

iam_ems_employees["Gender"] = gender
iam_ems_employees["birth_date"] = birth_date
iam_ems_employees.drop(["metadata"],1,inplace=True)
iam_ems_employees = iam_ems_employees[['user_id', 'employee_id', 'full_name', 'birth_date', 'Gender', 'employer_id', 'email', 'status', 'phone_number',
'organization_id','document_number','created_at', 'paused']]

query = """select * from ems.compensations c ;"""
compensations = dataframe_generator(query)
compensations = clean(compensations)
compensations = compensations[["employee_id", "monthly_salary"]]
compensations["monthly_salary"] = compensations["monthly_salary"]/100
rows_1_39 = pd.merge(iam_ems_employees,compensations, on = "employee_id")

query = """select * from ems.loan_agreements la ;"""
loan_agreements = dataframe_generator(query)
loan_agreements = clean(loan_agreements)
loan_agreements=loan_agreements[["employee_id", "loan_agreement_number", "expiration_date", "path", "accepted", "accepted_at"]]
loan_agreements["expiration_date"] = pd.to_datetime(loan_agreements["expiration_date"]).dt.date
loan_agreements["today"] = pd.to_datetime("today")
loan_agreements["today"] = loan_agreements["today"].dt.date
loan_agreements["loan_duration"] = loan_agreements["expiration_date"] - loan_agreements["today"]
loan_agreements["loan_Closure_date"] = loan_agreements["expiration_date"]
loan_agreements["accepted_at"] = pd.to_datetime(loan_agreements["accepted_at"]).dt.date
loan_agreements.drop(["expiration_date","today"],1,inplace=True)
loan_agreements.rename(columns={"accepted_date":"loan_agreement_date"},inplace=True)

l = loan_agreements[loan_agreements["employee_id"]=="ab6b57ad-6569-4a85-9513-35cab91011f7"].values.tolist()
l[0][0]='42ed6761-d5ab-46a7-902f-99c873d23ee8'
l1 = pd.DataFrame(l)
l1.columns = loan_agreements.columns.tolist()
loan_agreements = loan_agreements.append(l1, ignore_index=True)
loan_agreements[loan_agreements["employee_id"]=="42ed6761-d5ab-46a7-902f-99c873d23ee8"]

all_rows = pd.merge(rows_1_39,loan_agreements,on = "employee_id")

query = """select * from bnk.transactions t  ;"""
txns = dataframe_generator(query)
txns["second_creation_dummy"] = txns["created_at"]
txns = clean(txns)
txns.rename(columns={"entity_id":"user_id"},inplace=True)
txns.rename(columns={"amount":"Withdrawn Amount"}, inplace=True)
txns.rename(columns={"second_creation_dummy":"disbursal(txn) date"},inplace=True)
txns.rename(columns={"fee":"Total Fees"},inplace=True)
txns.rename(columns={"external_id":"merchant_id"},inplace=True)
# txns.rename(columns={"reference_id":"Loan Number"},inplace=True)
# txns["Loan Number"] = "'"+txns["Loan Number"]
txns["Total Fees"] = txns["Total Fees"]/100
txns = txns[txns["status"]=="COMPLETE"]
txns = txns[["id","user_id", "Withdrawn Amount","disbursal(txn) date", "Total Fees", "merchant_id"]]
txns["Withdrawn Amount"] = txns["Withdrawn Amount"]/100
txns.rename(columns={"id":"tid"},inplace=True)
txns["Total Amount"] = txns["Withdrawn Amount"]+txns["Total Fees"]

query = """select * from kbill.employer_invoice_deductions eid ;"""
eid = dataframe_generator(query)
eid = clean(eid)
eid = eid[["entity_id","reference_id"]]
eid["reference_id"] = "'"+eid["reference_id"]
eid.rename(columns  = {"entity_id":"tid", "reference_id":"Loan_Number"},inplace=True)
txns = pd.merge(txns,eid, on = "tid")
txns["disbursal(txn) date"] = txns["disbursal(txn) date"].dt.tz_convert(my_timezone)


all_rows = pd.merge(all_rows, txns, on = "user_id")
total_amounts=all_rows["Total Amount"].tolist()





#Taking Total fees values from DB  - June 30th, 2022

print ("Picking up total fees from db")


print ("Switching off Calculated")


# total_fees = []
# for x in total_amounts:
#     if 50<x<301:
#         total_fees.append(12)
#     elif 300<x<501:
#         total_fees.append(18)
#     elif 500<x<1001:
#         total_fees.append(35)
#     elif 1000<x<1501:
#         total_fees.append(60)
#     elif 1500<x<2501:
#         total_fees.append(100)
#     elif 2500<x<5001:
#         total_fees.append(200)
#     elif 5000<x<10001:
#         total_fees.append(275)
#     elif 10000<x<25001:
#         total_fees.append(700)
#     elif 25000<x<50001:
#         total_fees.append(800)
#     elif 50000<x<100001:
#         total_fees.append(1500)
#     else:
#         total_fees.append(0)
# all_rows["Total Fees Calculated"] = total_fees
# all_rows["processing_fees"] = round(all_rows["Total Fees Calculated"]/1.18,2)
# all_rows["GST_fees"] = all_rows["Total Fees Calculated"] - all_rows["processing_fees"]
all_rows["processing_fees"] = round(all_rows["Total Fees"]/1.18,2)
all_rows["GST_fees"] = all_rows["Total Fees"] - all_rows["processing_fees"]

all_rows["Annual_income"] = (all_rows["monthly_salary"]*12).astype(float)
all_rows["Annual_income"] = all_rows["Annual_income"].astype(str)
all_rows["Total Amount"] = all_rows["Withdrawn Amount"]+all_rows["GST_fees"]+all_rows["processing_fees"]
query = """select * from ems.work_period_balances wpb ;"""
wpb = dataframe_generator(query)
wpb = clean(wpb)

dd = wpb[wpb["employee_id"]=="ab6b57ad-6569-4a85-9513-35cab91011f7"]

dd["employee_id"]=["42ed6761-d5ab-46a7-902f-99c873d23ee8"]*dd.shape[0]

wpb = wpb.append(dd, ignore_index=True)

wpb = wpb[["employee_id", "available_amount", "payment_date"]]
wpb.rename(columns={"available_amount":"overall_limit", "payment_date":"next_payment_date"},inplace=True)
wpb["next_payment_date"] = pd.to_datetime(wpb["next_payment_date"]).dt.date
wpb["overall_limit"]  = wpb["overall_limit"]/100
all_rows = pd.merge(all_rows, wpb, on = "employee_id")
# all_rows["disbursal(txn) date"] = pd.to_datetime(all_rows["disbursal(txn) date"])
all_rows = all_rows.sort_values("disbursal(txn) date", ascending=True)
all_rows = all_rows.drop_duplicates(["tid"])

query = """select * from xorg.configurations xc ;"""
xc= dataframe_generator(query)

xc = xc[["organization_id","withdraw_limit_amount", "withdraw_amount_max"]]
xc["withdraw_limit_amount"] = xc["withdraw_limit_amount"]/100
xc["withdraw_amount_max"] = xc["withdraw_amount_max"]/100
xc.rename(columns={"withdraw_limit_amount":"salary_multiplier"},inplace=True)
xc.rename(columns={"withdraw_amount_max":"capping"},inplace=True)
all_rows = pd.merge(all_rows, xc, on = "organization_id", how = "left")
all_rows = all_rows.sort_values("disbursal(txn) date", ascending=True)
all_rows = all_rows.drop_duplicates(["tid"])
all_rows["Sanctioned Loan Limit"] = all_rows["monthly_salary"]*all_rows["salary_multiplier"]
all_rows["New Sanctioned Loan Limit"] = np.where(all_rows['Sanctioned Loan Limit']<all_rows["capping"], all_rows["Sanctioned Loan Limit"], all_rows["capping"])

all_rows["Sanctioned Loan Limit"] = all_rows["New Sanctioned Loan Limit"]

all_rows.drop(["capping", "New Sanctioned Loan Limit"],1,inplace=True)

uid = all_rows["user_id"].unique().tolist()
sum_of_withdrawals = []
for x in uid:
    sum_of_withdrawals.append(all_rows[all_rows["user_id"]==x]["Total Amount"].sum())
dddd = dict(zip(uid,sum_of_withdrawals))
all_rows["disbursed amount"] = all_rows['user_id'].map(dddd)  
all_rows["Undisbursed amount"] = all_rows["Sanctioned Loan Limit"] - all_rows["disbursed amount"]


# def hello_kms_bank(encrypted_text):
#     session = boto3.session.Session(profile_name="rain-india-production")
#     client = session.client("kms")
#     encrypted_text = encrypted_text
#     decrypted = client.decrypt(CiphertextBlob=base64.b64decode(encrypted_text))
#     decrypted_text = decrypted["Plaintext"].decode("utf-8").split("\x1d")
#     d = {}
#     for c in range(len(decrypted_text)):
#         d[decrypted_text[c].split("::")[0]] = decrypted_text[c].split("::")[1]
#     dd = pd.DataFrame(d.items()).T
#     dd.columns = dd.iloc[0]
#     dd = dd[1:]
#     return dd
def hello_kms_address(encrypted_text):
    session = boto3.session.Session(profile_name="rain-india-production")
    client = session.client("kms")
    encrypted_text = encrypted_text
    decrypted = client.decrypt(CiphertextBlob=base64.b64decode(encrypted_text))
    decrypted_text = decrypted["Plaintext"].decode("utf-8").split("\x1d")
    return decrypted_text

query = """select concat(
       '{"Loan_Number":"',eid.reference_id,'",',
       '"Name":"',u.full_name,'",',
       '"Employee_code":"',eid.employee_external_id,'",',
       '"Loan_date":"', eid.created_at::date,'",',
       '"Loan_amount":"',round(eid.amount/100::numeric,2),'",',
       '"Tenure":"1",',
       '"LoanStartDate":"',wpb.payment_date::date,'",',
       '"Monthly_EMIAMount":"',round(eid.amount/100::numeric,2),'",',
       '"Interest_percentage":"',round((t.fee::numeric/eid.amount::numeric)*100,2),'",',
       '"Vendor_ID":"RAIN-INSTANT-PAY"}'
       )
  from kbill.employer_invoice_deductions eid
 inner join bnk.transactions t on t.id = eid.entity_id
 inner join ems.employees e on e.id = eid.employee_id
 inner join iam.users u on u.id = e.user_id
 inner join ems.wpb_withdrawals ww on ww.transaction_id = t.id
 inner join ems.work_period_balances wpb on wpb.id = ww.wpb_id
 where eid.employee_id not in ('6e91b54d-a912-46e2-915e-10d56b5eee62','0a0ce4a1-86ab-45f0-8cc6-b881efb3d075',
 '774c2749-9c6a-44b6-954d-59cadbd15114')
 order by eid.created_at;"""


dd = dataframe_generator(query)
dd.columns = [0]

dd.columns = [0]
os.chdir("..")
os.chdir("due_dates")
month_string = pd.to_datetime("today").strftime("%b")
os.chdir(month_string)
dd.to_csv(pd.to_datetime("today").strftime("%d-%b-%Y")+".csv")

os.chdir("..")
os.chdir("..")
os.chdir("Code")






# os.chdir("..")
# print (os.getcwd())
# os.chdir("due_dates")




# month_string = pd.to_datetime("today").strftime("%b")
# os.chdir(month_string)

# file_to_be_read = pd.to_datetime("today").strftime("%d-%b-%Y")+".csv"

# due_dates = pd.read_csv(file_to_be_read,header=None)

# os.chdir("..")
# os.chdir("..")
# os.chdir("Code")

due_dates = dd.copy()
print (os.getcwd())

# due_dates = due_dates[2:]
ll = []
for x in due_dates[0].tolist():
    ll.append(x.strip("|"))
due_dates[0] = ll

import ast
loan_number = []
due_date = []
l = []
for x in due_dates[0].tolist():
    l.append(ast.literal_eval(x))
for x in l:
    loan_number.append(x["Loan_Number"])
    due_date.append(x["LoanStartDate"])
all_rows["Loan_Number"] = all_rows["Loan_Number"].str.strip("'").astype(str)
all_rows["Loan_Number_1"] = all_rows["Loan_Number"]
dd = dict(zip(loan_number,due_date))
all_rows["Due_Date"] = all_rows["Loan_Number_1"].map(dd)
all_rows["Loan_Number"] = "'"+ all_rows["Loan_Number"]
all_rows.drop(["Loan_Number_1"],1,inplace=True)

a = all_rows[all_rows["user_id"]=="03c696bf-1034-4c1d-8b5b-d639c645f188"]
import numpy as np
a["Due_Date"].fillna(0,inplace=True)
a["Due_Date"].replace({0:"2021-12-08"},inplace=True)
a = all_rows[all_rows["user_id"]=="65bf5ec8-ad63-4f62-a205-ae449800603c"]
import numpy as np
a["Due_Date"].fillna(0,inplace=True)
a["Due_Date"].replace({0:"2021-12-08"},inplace=True)
a = all_rows[all_rows["user_id"]=="0c604c37-f25f-43f7-a309-bfa343d665ea"]
import numpy as np
a["Due_Date"].fillna(0,inplace=True)
a["Due_Date"].replace({0:"2021-12-08"},inplace=True)
a = all_rows[all_rows["user_id"]=="423889c4-5b1d-403d-aa25-e1bff51af34f"]
import numpy as np
a["Due_Date"].fillna(0,inplace=True)
a["Due_Date"].replace({0:"2022-01-08"},inplace=True)


all_rows.sort_values(["disbursal(txn) date"], inplace=True)

query = """select * from iam.users u ;"""
iam = dataframe_generator(query)
iam = clean(iam)
iam.rename(columns={"id":"user_id"},inplace=True)
phone_number = iam["phone_number"].astype(str).tolist()
phone_numbers = []
for x in phone_number:
    phone_numbers.append(re.sub("[^0-9]", "", x))
phone_number_2 =[]
for x in phone_numbers:
    if len(x)>10:
        phone_number_2.append(x[2:])
    else:
        phone_number_2.append(x)
iam["phone_number"] = phone_number_2
iam = iam[["user_id"]]
start = time.time()
query = """select * from ems.employees e ;"""
ems_employees = dataframe_generator(query)
ems_employees = ems_employees[['id', 'user_id', 'employer_id', 'status', 'first_name', 'last_name',
        'created_at', 'updated_at', 'group_id', 'hr_employee_code', 'organization_id']]
ems_employees = ems_employees.rename(columns={'id': 'employee_id'})
ems_employees= clean(ems_employees)
print (time.time() - start)
ems_employees = ems_employees[["employee_id", "user_id", "employer_id"]]
start = time.time()
query = """select * from xorg.employers e;"""
xorg = dataframe_generator(query)
xorg = xorg[["id","organization_id", "lookup_name"]]
xorg["lookup_name"] = xorg["lookup_name"].str.lower()
xorg.rename(columns={"id":"employer_id"},inplace=True)
print (time.time() - start)
start = time.time()
xorg["organization_id"].replace({"c2a6a007-e625-456f-8c36-92cd2654c971": 'Quess',
                                              "916227f6-cb69-46ec-8cb1-a735ed98f2c4": 'D2C Org', 
                                             "d779558a-09cc-4920-9f39-d8409c8f0728":"B2B Test", 
                                             "0706c2e4-5153-4429-9645-a6d0c4a26a04":"HCL", 
                                    "6740ddac-09fa-4c0b-9cad-a261cc23997e":"Cloudnine", 
                                    "2f57a3bb-26a6-4cd6-953e-ce61fe607e6a":"Wissen Infotech", 
                                    "a8957481-3705-4d7f-b521-491a7e859f47":"Support.com", 
                                    "752927b6-2655-4760-8fea-33e7417b2a75":"Sutherland"},inplace=True)
print (time.time() - start)
ems_xorg= pd.merge(ems_employees, xorg, on = "employer_id")
iam_ems_xorg = pd.merge(iam, ems_xorg, on = "user_id", how = "left")
iam_ems_xorg = iam_ems_xorg[["user_id", "lookup_name"]]
print (all_rows.shape)
all_rows = pd.merge(all_rows, iam_ems_xorg, on = "user_id", how = "left")

start = time.time()
query = """select * from bnk.external_accounts ea ;"""
external_accounts = dataframe_generator(query)
external_accounts = clean(external_accounts)
external_accounts = external_accounts[external_accounts["user_id"].isin(all_rows["user_id"].unique().tolist())]
account_details = external_accounts["account_details"].tolist()
decrypted_account_details = []
count=len(account_details)
for x in account_details:
    decrypted_account_details.append(hello_kms_bank(x))
    count-=1
    print (count)
decrypted_account_details_df = pd.concat(decrypted_account_details)
acc_holder_name = decrypted_account_details_df["accountHolderName"].tolist()
bank_acc_number = decrypted_account_details_df["bankAccountNumber"].tolist()
bank_acc_number= ["'"+str(x) for x in bank_acc_number]
bank_ifsc= decrypted_account_details_df["bankRoutingNumber"].tolist()
external_accounts["accountHolderName"] = acc_holder_name
external_accounts["bankAccountNumber"] = bank_acc_number
external_accounts["bankIFSCNumber"] = bank_ifsc
external_accounts = external_accounts[["user_id","accountHolderName", "bankAccountNumber","bankIFSCNumber"]]
all_rows = pd.merge(all_rows, external_accounts, on = "user_id")
print (time.time() - start)


os.chdir("..")
os.chdir("Dumps")
all_rows.to_pickle("Recent_all_rows_dump.pkl")
os.chdir("..")
os.chdir("Code")


# os.chdir("..")
# os.chdir("Dumps")
# all_rows = pd.read_pickle("Recent_all_rows_dump.pkl")
# os.chdir("..")
# os.chdir("Code")


import ast
query = """select * from kyc.documents kyc ;"""
kyc = dataframe_generator(query)
kyc = clean(kyc)
kyc_pan = kyc[(kyc["document_type"]=="PAN")&(kyc["verified"]==True)&(kyc["approved"]==True)]

names = kyc_pan["details"].tolist()
names_dicts = []
for x in names:
    names_dicts.append(ast.literal_eval(x))
    
name_final = []
for x in names_dicts:
    try:
        print (x["data"]["data"]["name"]["value"])
        name_final.append(x["data"]["data"]["name"]["value"])
    except:
        name_final.append("No Name found")
        
kyc_pan["kyc_name"] = name_final
kyc_pan_name = kyc_pan[["user_id", "kyc_name"]]
kyc_pan_name = kyc_pan_name.groupby("user_id").first().reset_index()
all_rows = pd.merge(all_rows, kyc_pan_name, on = "user_id", how = "left")


query = """select * from kyc.documents kyc ;"""
kyc = dataframe_generator(query)
kyc = clean(kyc)
kyc_pan = kyc[(kyc["document_type"]=="PAN")&(kyc["verified"]==True)&(kyc["approved"]==True)]
import ast
dobs = kyc_pan["details"].tolist()
dobs_dicts = []
for x in dobs:
    dobs_dicts.append(ast.literal_eval(x))
    
dob_final = []
for x in dobs_dicts:
    try:
        print (x["data"]["data"]["date"]["value"])
        dob_final.append(x["data"]["data"]["date"]["value"])
    except:
        dob_final.append("No Birthdate")
        
kyc_pan["kyc_birthdate"] = dob_final
kyc_pan_dob = kyc_pan[["user_id", "kyc_birthdate"]]

kyc_pan_dob = kyc_pan_dob.groupby("user_id").first().reset_index()
all_rows = pd.merge(all_rows, kyc_pan_dob, on = "user_id", how = "left")

def cleaner_row(df):
    df["created_at"] = df["created_at"].astype(str)
    df["birth_date"] = df["birth_date"].astype(str)
    df["phone_number"] = df["phone_number"].astype(str)
    df["accepted_at"] = df["accepted_at"].astype(str)
    df["loan_Closure_date"] = df["loan_Closure_date"].astype(str)
#     df["disbursal(txn) date"] = pd.to_datetime(df["disbursal(txn) date"], utc=True)
    df["Due_Date"] = df["Due_Date"].astype(str)
    return df

all_rows = all_rows.drop_duplicates(["tid"])
all_rows.sort_values(["disbursal(txn) date"], inplace=True)
all_rows_1 = all_rows.copy()

all_rows_1.drop(["salary_multiplier"],1,inplace=True)
new_row = pd.read_excel("data_2.xlsx")

new_row["kyc_name"] = ""
new_row["kyc_birthdate"] = ""
new_row["lookup_name"] = ""
new_row["merchant_id"] = ""
new_row = cleaner_row(new_row)
supriya = pd.read_excel("supriya rao_1.xlsx")

supriya["lookup_name"] = ""
supriya["merchant_id"] = ""
supriya = cleaner_row(supriya)
supriya["Sanctioned Loan Limit"] = supriya["monthly_salary"]*0.4
cols = all_rows_1.columns.tolist()
new_row = new_row[cols]
all_rows_1 = pd.concat([all_rows_1,new_row])
supriya = supriya[cols]
all_rows_1 = pd.concat([all_rows_1,supriya])



os.chdir("..")
os.chdir("Outputs")
all_rows_1.to_csv("all_rows.csv")
os.chdir("..")
os.chdir("Code")
scope = ["https://www.googleapis.com/auth/spreadsheets", "https://www.googleapis.com/auth/drive.file", 
        "https://www.googleapis.com/auth/drive"]
creds = ServiceAccountCredentials.from_json_keyfile_name("warm-torus-341214-30e43816fb06.json", scope)
client = gspread.authorize(creds)

os.chdir("..")
os.chdir("Outputs")
# df = all_rows_1.copy()
# employees_kyc_demographic= client.open("CS/OPS Dashboard").worksheet("Withdrawals(txns) (Deprecated)")
# employees_kyc_demographic.clear()
# existing = gd.get_as_dataframe(employees_kyc_demographic)
# existing = pd.DataFrame(employees_kyc_demographic.get_all_records())
# updated = existing.append(df)
# gd.set_with_dataframe(employees_kyc_demographic, updated)

os.chdir("..")
os.chdir("Code")

all_rows_grouped = all_rows_1.groupby("user_id").last().reset_index()


wd = []
pf = []
gst = []
ta =[]
for x in all_rows_grouped["user_id"].unique().tolist():
    wd.append(all_rows[all_rows["user_id"]==x]["Withdrawn Amount"].sum())
    pf.append(all_rows[all_rows["user_id"]==x]["processing_fees"].sum())
    gst.append(all_rows[all_rows["user_id"]==x]["GST_fees"].sum())
    ta.append(all_rows[all_rows["user_id"]==x]["Total Amount"].sum())
    
    
all_rows_grouped["Withdrawn Amount"] = wd
all_rows_grouped["processing_fees"] = pf
all_rows_grouped["GST_fees"] = gst
all_rows_grouped["Total Amount"] = ta

print (os.getcwd())
os.chdir("..")
os.chdir("..")
os.chdir("..")
print (os.getcwd())

os.chdir("Google Drive/")
os.chdir("Shared drives/")
os.chdir("Repayments/")



from datetime import date
from datetime import timedelta

month_string = pd.to_datetime("today").strftime("%b")
os.chdir(month_string)
today = date.today()
# yest = today-timedelta(days =3)
print("Today's date:", today)
today = today.strftime("%d-%b-%Y")

os.chdir(today)
print (os.getcwd())
# pd.read_excel(os.listdir()[0])
total_refunds_2_oct= pd.read_excel((os.listdir()[0]), sheet_name="Oct")
total_refunds_2_nov= pd.read_excel((os.listdir()[0]), sheet_name="Nov")
total_refunds_2_dec= pd.read_excel((os.listdir()[0]), sheet_name="Dec")
total_refunds_2_jan= pd.read_excel((os.listdir()[0]), sheet_name="Jan")
total_refunds_2_feb= pd.read_excel((os.listdir()[0]), sheet_name="Feb")
total_refunds_2_mar= pd.read_excel((os.listdir()[0]), sheet_name="Mar")
total_refunds_2 = pd.concat([total_refunds_2_oct,total_refunds_2_nov, total_refunds_2_dec, total_refunds_2_jan,
                            total_refunds_2_feb, total_refunds_2_mar])
total_refunds_2.rename(columns={"Due date":"created at"},inplace=True)
total_refunds_2.drop(["full_name"],1,inplace=True)
total_refunds = total_refunds_2.copy()
total_refunds["created at"] = total_refunds["created at"].astype(str)
print (os.getcwd())
os.chdir("/Users/arunabhmajumdar/Documents/all-india-dashboard-code/Code")

other_refunds = pd.read_excel("Refund_18112021.xlsx")
other_refunds.rename(columns={"CODES":"user_id", "AMOUNT":"Other_Refunded"},inplace=True)
all_rows_grouped = pd.merge(all_rows_grouped, other_refunds, on = "user_id", how = "left")
all_rows_grouped["Other_Refunded"] = all_rows_grouped["Other_Refunded"].fillna(0)
total_refunds = total_refunds[["user_id", "repayment"]]
total_refunds = total_refunds.groupby(["user_id"]).sum().reset_index()
rd = dict(zip(total_refunds["user_id"].tolist(), total_refunds["repayment"].tolist()))
all_rows_grouped["TA"] = all_rows_grouped["user_id"]
all_rows_grouped["Repayment"]= all_rows_grouped["TA"].map(rd)
all_rows_grouped["Repayment"] = all_rows_grouped["Repayment"].fillna(0)
all_rows_grouped["Closing Balance"] = all_rows_grouped["Total Amount"] - all_rows_grouped["Repayment"] + all_rows_grouped["Other_Refunded"]
all_rows_grouped["Undisbursed Amount"] = all_rows_grouped["Sanctioned Loan Limit"] - all_rows_grouped["Closing Balance"]


query = """select * from kyc.documents kyc ;"""
kyc = dataframe_generator(query)
kyc = clean(kyc)
uid = all_rows_grouped["user_id"].unique().tolist()
details = []
for x in uid:
    details.append(kyc[kyc["user_id"]==x]["details"].tolist())

details_3=[]
for x in details:
    details_2=[]
    for y in x:
#         print (json.loads(y))
        details_2.append(json.loads(y))
    details_3.append(details_2)  

data=[]
for x in details_3:
    data_2=[]
    for y in x:
        if type(y)==dict:
            if "data" in y:
                if "data" in y["data"]:
                    if "address" in y["data"]["data"]:
                        print (y["data"]["data"]["address"]["value"])
                        data_2.append(y["data"]["data"]["address"]["value"])
    data.append(data_2)

data_23 = []
for x in data:
    if len(x)>1:
        data_23.append(x[-1])
    else:
        data_23.append(x)
        
        
all_rows_grouped["address"] = data_23
query = """select * from kyc.documents kyc ;"""
kyc = dataframe_generator(query)
kyc = clean(kyc)
uid = all_rows_grouped["user_id"].unique().tolist()
details = []
for x in uid:
    details.append(kyc[kyc["user_id"]==x]["details"].tolist())

details_3=[]
for x in details:
    details_2=[]
    for y in x:
#         print (json.loads(y))
        details_2.append(json.loads(y))
    details_3.append(details_2)  

data=[]
for x in details_3:
    data_2=[]
    for y in x:
        if type(y)==dict:
            if "data" in y:
                if "data" in y["data"]:
                    if "aadhaar" in y["data"]["data"]:
                        print (y["data"]["data"]["aadhaar"]["value"])
                        data_2.append(y["data"]["data"]["aadhaar"]["value"])
    data.append(data_2)

data_23 = []
for x in data:
    if len(x)>1:
        data_23.append(x[-1])
    else:
        data_23.append(x)
        
        
        
        


all_rows_grouped["aadhaar"] = data_23

all_rows_grouped = all_rows_grouped[['user_id', 'employee_id', 'full_name', 'birth_date', 'Gender',
       'employer_id', 'email', 'status', 'phone_number', 'organization_id','address', 'paused','aadhaar',
       'document_number', 'created_at', 'monthly_salary', 'accepted', 'accepted_at',
       'loan_duration', 'loan_Closure_date', 'tid','disbursal(txn) date', 'Withdrawn Amount',
    'processing_fees', 'GST_fees',  'Total Fees','Repayment', 'Closing Balance','Total Amount', 'Sanctioned Loan Limit', 'Undisbursed Amount','Annual_income','overall_limit', 'next_payment_date', 'Due_Date',
       'accountHolderName', 'bankAccountNumber', 'bankIFSCNumber', 'Other_Refunded', 'kyc_name', "kyc_birthdate"]]


ifsc_list = all_rows_grouped["bankIFSCNumber"].tolist()
import requests
URL = "https://ifsc.razorpay.com/"
bank = []
bc = len(ifsc_list)
for x in ifsc_list:
    bc-=1
    ba = {}
    try:
        data = requests.get(URL+x).json()
        ba['IFSC'] = x
        ba["Bank Name"] = data["BANK"]
        ba["Bank Address"] = data["ADDRESS"]
        ba["Bank Branch"] = data["BRANCH"]
        bank.append(ba)
    except:
        data = {"BANK":"Not Found", "ADDRESS":"Not Found", "BRANCH":"Not Found"}
        ba['IFSC'] = x
        ba["Bank Name"] = data["BANK"]
        ba["Bank Address"] = data["ADDRESS"]
        ba["Bank Branch"] = data["BRANCH"]
        bank.append(ba)
        
    print (bc)
    
bank = pd.DataFrame(bank)
all_rows_grouped["Bank Name"] = bank["Bank Name"]
all_rows_grouped["Bank Branch"] = bank["Bank Branch"]
all_rows_grouped["Bank Address"] = bank["Bank Address"]


# df = all_rows_grouped.copy()
# employees_kyc_demographic= client.open("CS/OPS Dashboard").worksheet("Withdrawals(users)  (Deprecated)")
# employees_kyc_demographic.clear()
# existing = gd.get_as_dataframe(employees_kyc_demographic)
# existing = pd.DataFrame(employees_kyc_demographic.get_all_records())
# updated = existing.append(all_rows_grouped.copy())
# gd.set_with_dataframe(employees_kyc_demographic, updated)

overflow_users  = all_rows_grouped[all_rows_grouped["Undisbursed Amount"]<0]
# df = overflow_users.copy()
# employees_kyc_demographic= client.open("CS/OPS Dashboard").worksheet("Overflow users  (Deprecated)")
# employees_kyc_demographic.clear()
# existing = gd.get_as_dataframe(employees_kyc_demographic)
# existing = pd.DataFrame(employees_kyc_demographic.get_all_records())
# updated = existing.append(df)
# gd.set_with_dataframe(employees_kyc_demographic, updated)
print ("ok then")


def bq_cleaner(df):
    new_cols = []
    l = df.columns.tolist()
    for x in l:
        x = x.replace("(","_")
        x = x.replace(" ","_")
        x = x.replace(")","_")
        new_cols.append(x)
    df.columns = new_cols
    df = df.astype(str)
    return df

all_rows_1 = bq_cleaner(all_rows_1)
pandas_gbq.to_gbq(all_rows_1.copy(), destination_table="Bank_Data.withdrawals-txns_with_bank", project_id="data-warehouse-india", if_exists="replace")
all_rows_grouped = bq_cleaner(all_rows_grouped)
pandas_gbq.to_gbq(all_rows_grouped.copy(), destination_table="Bank_Data.withdrawals_users_grouped_with_bank", project_id="data-warehouse-india", if_exists="replace")
overflow_users = bq_cleaner(overflow_users)
pandas_gbq.to_gbq(overflow_users.copy(), destination_table="Bank_Data.overflow_users_with_bank", project_id="data-warehouse-india", if_exists="replace")





print ("Connecting to Big Query Table")
query_string = "select * FROM `data-warehouse-india.Code_internal.Run_times_code`"
code__times = pd.read_gbq(query_string, project_id="data-warehouse-india")

c = code__times[code__times["Code"]==code_name]


code__times.at[c.index.tolist()[-1], "Run_time"] = time.time() - start_1
code__times.at[c.index.tolist()[-1], "time_updated"] = datetime.datetime.now()

code__times["time_updated"] = pd.to_datetime(code__times["time_updated"])
code__times = code__times.sort_values("time_updated")

code__times_bq = bq_cleaner(code__times)
pandas_gbq.to_gbq(code__times_bq, destination_table="Code_internal.Run_times_code", project_id="data-warehouse-india", if_exists="replace")



# print ("Starting Event logs - Could take a while")
# start = time.time()
# query = """select * from elog.events e;"""
# elog = dataframe_generator(query)
# elog_bq = bq_cleaner(elog)
# pandas_gbq.to_gbq(elog_bq, destination_table="Raw_Postgres_data.elog_events", project_id="data-warehouse-india", if_exists="replace")
# print (time.time() - start)
# print ("elog events uploaded")

print ("\n")
print ("Total Run Time: "+str(time.time() - start_1))
































