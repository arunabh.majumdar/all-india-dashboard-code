#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 15 13:24:25 2022

@author: arunabhmajumdar
"""

code_name = "Marketing_MOM_and_AU4s"
import datetime
from dateutil import relativedelta
print ("Importing all packages and Google BQ credentials files")
import warnings
warnings.filterwarnings("ignore")
import pandas as pd,os
import base64
import boto3
import time
import pandas as pd, os
from oauth2client.service_account import ServiceAccountCredentials
import gspread
import gspread_dataframe as gd
import psycopg2
import df2gspread as d2g
import pandas as pd
import warnings
warnings.filterwarnings("ignore")
import numpy as np
import re
import calendar
import os
import json
import time
print ("Changing directory to Code to aid ease of access to the various jsons and xlsx")
print(os.getcwd())
os.chdir("..")
time.sleep(2)
print ("Changed Directory")
print (os.getcwd())
start_1 = time.time()
import numpy as np
import pytz
my_timezone = pytz.timezone('Asia/Calcutta')
import ast
import gzip
from datetime import timedelta
from currency_converter import CurrencyConverter
from google.oauth2 import service_account
from google.cloud import bigquery
import pandas_gbq
import os, pandas as pd
os.getcwd()
KEY_PATH = "data-warehouse-india-84f5f8a775d1.json"
CREDS = service_account.Credentials.from_service_account_file(KEY_PATH)
bq_client = bigquery.Client(credentials=CREDS, project="data-warehouse-india")
import warnings
warnings.filterwarnings("ignore")


print ("Connecting to Dynamo DB using boto3")
start = time.time()
print ("starting run")
session = boto3.session.Session(profile_name="rain-india-prod")
client = session.client("dynamodb")
dynamodb = boto3.resource("dynamodb")
print ("Connecting to Postgres using psycopg2")
connection = psycopg2.connect(user="rainadmin",
                                      password="Mudar123",
                                      host="localhost",
                                      port=55432,
                                      database="rain")
cursor = connection.cursor()
# Print PostgreSQL details
print("PostgreSQL server information")
print(connection.get_dsn_parameters(), "\n")
# cursor.itersize = 10000
cursor.execute("SELECT version();")
    # Fetch result
record = cursor.fetchone()
print("You are connected to - ", record, "\n")

print ("Function to download Postgres data and concert that to dataframe")
def dataframe_generator(query):
    cursor.execute(query)
    print('Read table in PostgreSQL')
    data = cursor.fetchall()
    cols = []
    for elt in cursor.description:
        cols.append(elt[0])
    df= pd.DataFrame(data = data, columns=cols)
    return df

print ("Function to clean dataframe to include only data post Sept 1, 2021")
def clean(df):
    df["created_at"] = df["created_at"].dt.date.astype(str)
    df = df[df["created_at"]>"2021-08-31"]
    return df
print (time.time() - start)


print ("Function to clean dataframe to upload to BQ, BQ doesn't take spaces, dashes etc;")
def bq_cleaner(df):
    new_cols = []
    l = df.columns.tolist()
    for x in l:
        x = x.replace("(","_")
        x = x.replace(" ","_")
        x = x.replace(")","_")
        new_cols.append(x)
    df.columns = new_cols
    df = df.astype(str)
    return df







print ("Running Marketing MOM")
print ("Connecting to Big Query Table")
query_string = "select * FROM `data-warehouse-india.Data_Studio.All_withdrawals`"
all_rows = pd.read_gbq(query_string, project_id="data-warehouse-india")
all_rows_1 = all_rows.copy()
all_rows_1["disbursal_txn__date"] = pd.to_datetime(all_rows_1["disbursal_txn__date"]).dt.date.astype(str)
all_rows_1= all_rows_1[['user_id','birth_date', "email", 'Gender','organization_id','monthly_salary',
           'tid','disbursal_txn__date','Total_Amount', 'processing_fees', 
       'overall_limit', 'Sanctioned_Loan_Limit', "lookup_name"]]
all_rows_1["organization_id"].replace({"c2a6a007-e625-456f-8c36-92cd2654c971": 'Quess',
                                              "916227f6-cb69-46ec-8cb1-a735ed98f2c4": 'D2C Org', 
                                             "d779558a-09cc-4920-9f39-d8409c8f0728":"B2B Test", 
                                             "0706c2e4-5153-4429-9645-a6d0c4a26a04":"HCL", 
                                    "6740ddac-09fa-4c0b-9cad-a261cc23997e":"Cloudnine", 
                                    "2f57a3bb-26a6-4cd6-953e-ce61fe607e6a":"Wissen Infotech", 
                                    "a8957481-3705-4d7f-b521-491a7e859f47":"Support.com", 
                                    "752927b6-2655-4760-8fea-33e7417b2a75":"Sutherland"},inplace=True)


all_rows_1["processing_fees"] = all_rows_1['processing_fees'].astype(float)
print ("Connection Done")
a = all_rows_1.groupby("user_id").first().reset_index()[["user_id", "disbursal_txn__date"]]
a.rename(columns={"disbursal_txn__date":"first_txn_date"},inplace = True)
all_rows_1 = pd.merge(all_rows_1, a, on = "user_id", how = "left")
all_rows_1['New User'] = np.where(all_rows_1['first_txn_date']==all_rows_1["disbursal_txn__date"], 'Yes', 'No')


d2c = all_rows_1[all_rows_1["organization_id"]=="D2C Org"]
b2b2c = all_rows_1[all_rows_1["organization_id"]!="D2C Org"]

sep_withdrawals = all_rows_1[(all_rows_1["disbursal_txn__date"]>"2021-08-31")&(all_rows_1["disbursal_txn__date"]<"2021-10-01")]


oct_withdrawals = all_rows_1[(all_rows_1["disbursal_txn__date"]>"2021-09-30")&(all_rows_1["disbursal_txn__date"]<"2021-11-01")]
nov_withdrawals = all_rows_1[(all_rows_1["disbursal_txn__date"]>"2021-10-31")&(all_rows_1["disbursal_txn__date"]<"2021-12-01")]
dec_withdrawals = all_rows_1[(all_rows_1["disbursal_txn__date"]>"2021-11-30")&(all_rows_1["disbursal_txn__date"]<"2022-01-01")]
jan_withdrawals = all_rows_1[(all_rows_1["disbursal_txn__date"]>"2021-12-31")&(all_rows_1["disbursal_txn__date"]<"2022-02-01")]
feb_withdrawals = all_rows_1[(all_rows_1["disbursal_txn__date"]>"2022-01-31")&(all_rows_1["disbursal_txn__date"]<"2022-03-01")]
march_withdrawals = all_rows_1[(all_rows_1["disbursal_txn__date"]>"2022-02-28")&(all_rows_1["disbursal_txn__date"]<"2022-04-01")]
april_withdrawals = all_rows_1[(all_rows_1["disbursal_txn__date"]>"2022-03-31")&(all_rows_1["disbursal_txn__date"]<"2022-05-01")]
may_withdrawals = all_rows_1[(all_rows_1["disbursal_txn__date"]>"2022-04-30")&(all_rows_1["disbursal_txn__date"]<"2022-06-01")]
june_withdrawals = all_rows_1[(all_rows_1["disbursal_txn__date"]>"2022-05-31")&(all_rows_1["disbursal_txn__date"]<"2022-07-01")]
july_withdrawals = all_rows_1[(all_rows_1["disbursal_txn__date"]>"2022-06-30")&(all_rows_1["disbursal_txn__date"]<"2022-08-01")]



print ("First Run completed")



new_users_bq = bq_cleaner(all_rows_1.copy())
pandas_gbq.to_gbq(new_users_bq, destination_table="Data_Studio.New_Users", project_id="data-warehouse-india", if_exists="replace")

final = {}
final["Sep"] = sep_withdrawals.shape[0]
final["Oct"] = oct_withdrawals.shape[0]
final["Nov"] = nov_withdrawals.shape[0]
final["Dec"] = dec_withdrawals.shape[0]
final["Jan"] = jan_withdrawals.shape[0]
final["Feb"] = feb_withdrawals.shape[0]
final["March"] = march_withdrawals.shape[0]
final["April"] = april_withdrawals.shape[0]
final["May"] = may_withdrawals.shape[0]
final["June"] = june_withdrawals.shape[0]
final["July"] = july_withdrawals.shape[0]

final = pd.DataFrame(final.items())
final.columns = ["month", "Total no of withdrawals"]

final["index"] = final.index


print ("Second Run completed")
d2c_sep_withdrawals = d2c[(d2c["disbursal_txn__date"]>"2021-08-31")&(d2c["disbursal_txn__date"]<"2021-10-01")]
d2c_oct_withdrawals = d2c[(d2c["disbursal_txn__date"]>"2021-09-30")&(d2c["disbursal_txn__date"]<"2021-11-01")]
d2c_nov_withdrawals = d2c[(d2c["disbursal_txn__date"]>"2021-10-31")&(d2c["disbursal_txn__date"]<"2021-12-01")]
d2c_dec_withdrawals = d2c[(d2c["disbursal_txn__date"]>"2021-11-30")&(d2c["disbursal_txn__date"]<"2022-01-01")]
d2c_jan_withdrawals = d2c[(d2c["disbursal_txn__date"]>"2021-12-31")&(d2c["disbursal_txn__date"]<"2022-02-01")]
d2c_feb_withdrawals = d2c[(d2c["disbursal_txn__date"]>"2022-01-31")&(d2c["disbursal_txn__date"]<"2022-03-01")]
d2c_march_withdrawals = d2c[(d2c["disbursal_txn__date"]>"2022-02-28")&(d2c["disbursal_txn__date"]<"2022-04-01")]
d2c_april_withdrawals = d2c[(d2c["disbursal_txn__date"]>"2022-03-31")&(d2c["disbursal_txn__date"]<"2022-05-01")]
d2c_may_withdrawals = d2c[(d2c["disbursal_txn__date"]>"2022-04-30")&(d2c["disbursal_txn__date"]<"2022-06-01")]
d2c_june_withdrawals = d2c[(d2c["disbursal_txn__date"]>"2022-05-31")&(d2c["disbursal_txn__date"]<"2022-07-01")]
d2c_july_withdrawals = d2c[(d2c["disbursal_txn__date"]>"2022-06-30")&(d2c["disbursal_txn__date"]<"2022-08-01")]

f = {}
f["Sep"] = d2c_sep_withdrawals.shape[0]
f["Oct"] = d2c_oct_withdrawals.shape[0]
f["Nov"] = d2c_nov_withdrawals.shape[0]
f["Dec"] = d2c_dec_withdrawals.shape[0]
f["Jan"] = d2c_jan_withdrawals.shape[0]
f["Feb"] = d2c_feb_withdrawals.shape[0]
f["March"] = d2c_march_withdrawals.shape[0]
f["April"] = d2c_april_withdrawals.shape[0]
f["May"] = d2c_may_withdrawals.shape[0]
f["June"] = d2c_june_withdrawals.shape[0]
f["July"] = d2c_july_withdrawals.shape[0]


f = pd.DataFrame(f.items())
f.columns = ['month', 'D2C']
final = pd.merge(final, f, on = "month")
print ("Third Run completed")
b2b2c_sep_withdrawals = b2b2c[(b2b2c["disbursal_txn__date"]>"2021-08-31")&(b2b2c["disbursal_txn__date"]<"2021-10-01")]
b2b2c_oct_withdrawals = b2b2c[(b2b2c["disbursal_txn__date"]>"2021-09-30")&(b2b2c["disbursal_txn__date"]<"2021-11-01")]
b2b2c_nov_withdrawals = b2b2c[(b2b2c["disbursal_txn__date"]>"2021-10-31")&(b2b2c["disbursal_txn__date"]<"2021-12-01")]
b2b2c_dec_withdrawals = b2b2c[(b2b2c["disbursal_txn__date"]>"2021-11-30")&(b2b2c["disbursal_txn__date"]<"2022-01-01")]
b2b2c_jan_withdrawals = b2b2c[(b2b2c["disbursal_txn__date"]>"2021-12-31")&(b2b2c["disbursal_txn__date"]<"2022-02-01")]
b2b2c_feb_withdrawals = b2b2c[(b2b2c["disbursal_txn__date"]>"2022-01-31")&(b2b2c["disbursal_txn__date"]<"2022-03-01")]
b2b2c_march_withdrawals = b2b2c[(b2b2c["disbursal_txn__date"]>"2022-02-28")&(b2b2c["disbursal_txn__date"]<"2022-04-01")]
b2b2c_april_withdrawals = b2b2c[(b2b2c["disbursal_txn__date"]>"2022-03-31")&(b2b2c["disbursal_txn__date"]<"2022-05-01")]
b2b2c_may_withdrawals = b2b2c[(b2b2c["disbursal_txn__date"]>"2022-04-30")&(b2b2c["disbursal_txn__date"]<"2022-06-01")]
b2b2c_june_withdrawals = b2b2c[(b2b2c["disbursal_txn__date"]>"2022-05-31")&(b2b2c["disbursal_txn__date"]<"2022-07-01")]
b2b2c_july_withdrawals = b2b2c[(b2b2c["disbursal_txn__date"]>"2022-06-30")&(b2b2c["disbursal_txn__date"]<"2022-08-01")]


f = {}
f["Sep"] = b2b2c_sep_withdrawals.shape[0]
f["Oct"] = b2b2c_oct_withdrawals.shape[0]
f["Nov"] = b2b2c_nov_withdrawals.shape[0]
f["Dec"] = b2b2c_dec_withdrawals.shape[0]
f["Jan"] = b2b2c_jan_withdrawals.shape[0]
f["Feb"] = b2b2c_feb_withdrawals.shape[0]
f["March"] = b2b2c_march_withdrawals.shape[0]
f["April"] = b2b2c_april_withdrawals.shape[0]
f["May"] = b2b2c_may_withdrawals.shape[0]
f["June"] = b2b2c_june_withdrawals.shape[0]
f["July"] = b2b2c_july_withdrawals.shape[0]

f = pd.DataFrame(f.items())
f.columns = ['month', 'B2B2C']
final = pd.merge(final, f, on = "month")
print ("Fourth Run completed")


new_users_sep = sep_withdrawals[sep_withdrawals["New User"]=="Yes"].shape[0]
new_users_oct = oct_withdrawals[oct_withdrawals["New User"]=="Yes"].shape[0]
new_users_nov = nov_withdrawals[nov_withdrawals["New User"]=="Yes"].shape[0]
new_users_dec = dec_withdrawals[dec_withdrawals["New User"]=="Yes"].shape[0]
new_users_jan = jan_withdrawals[jan_withdrawals["New User"]=="Yes"].shape[0]
new_users_feb = feb_withdrawals[feb_withdrawals["New User"]=="Yes"].shape[0]
new_users_mar = march_withdrawals[march_withdrawals["New User"]=="Yes"].shape[0]
new_users_apr = april_withdrawals[april_withdrawals["New User"]=="Yes"].shape[0]
new_users_may = may_withdrawals[may_withdrawals["New User"]=="Yes"].shape[0]
new_users_june = june_withdrawals[june_withdrawals["New User"]=="Yes"].shape[0]
new_users_july = july_withdrawals[july_withdrawals["New User"]=="Yes"].shape[0]

f = {}
f["Sep"] = new_users_sep
f["Oct"] = new_users_oct
f["Nov"] = new_users_nov
f["Dec"] = new_users_dec
f["Jan"] = new_users_jan
f["Feb"] = new_users_feb
f["March"] = new_users_mar
f["April"] = new_users_apr
f["May"] = new_users_may
f["June"] = new_users_june
f["July"] = new_users_july


# sep_unique_users = sep_withdrawals["user_id"].unique().tolist()
# oct_unique_users = oct_withdrawals["user_id"].unique().tolist()
# nov_unique_users = nov_withdrawals["user_id"].unique().tolist()
# dec_unique_users = dec_withdrawals["user_id"].unique().tolist()
# jan_unique_users = jan_withdrawals["user_id"].unique().tolist()
# feb_unique_users = feb_withdrawals["user_id"].unique().tolist()
# mar_unique_users = march_withdrawals["user_id"].unique().tolist()
# apr_unique_users = april_withdrawals["user_id"].unique().tolist()
# may_unique_users = may_withdrawals["user_id"].unique().tolist()
# june_unique_users = june_withdrawals["user_id"].unique().tolist()

# new_users_sep = len(sep_unique_users)
# new_users_oct = len(list(set(oct_unique_users) - set(sep_unique_users)))
# new_users_nov = len(list(set(nov_unique_users) - set(oct_unique_users)))
# new_users_dec = len(list(set(dec_unique_users) - set(nov_unique_users)))
# new_users_jan = len(list(set(jan_unique_users) - set(dec_unique_users)))
# new_users_feb = len(list(set(feb_unique_users) - set(jan_unique_users)))
# new_users_mar = len(list(set(mar_unique_users) - set(feb_unique_users)))
# new_users_apr = len(list(set(apr_unique_users) - set(mar_unique_users)))
# new_users_may = len(list(set(may_unique_users) - set(apr_unique_users)))
# new_users_june = len(list(set(june_unique_users) - set(may_unique_users)))

f = {}
f["Sep"] = new_users_sep
f["Oct"] = new_users_oct
f["Nov"] = new_users_nov
f["Dec"] = new_users_dec
f["Jan"] = new_users_jan
f["Feb"] = new_users_feb
f["March"] = new_users_mar
f["April"] = new_users_apr
f["May"] = new_users_may
f["June"] = new_users_june
f["July"] = new_users_july

print ("Fifth Run completed")
f = pd.DataFrame(f.items())
f.columns = ['month', 'New users (Total)']
final = pd.merge(final, f, on = "month")


sep_unique_users = sep_withdrawals["user_id"].unique().tolist()
oct_unique_users = oct_withdrawals["user_id"].unique().tolist()
nov_unique_users = nov_withdrawals["user_id"].unique().tolist()
dec_unique_users = dec_withdrawals["user_id"].unique().tolist()
jan_unique_users = jan_withdrawals["user_id"].unique().tolist()
feb_unique_users = feb_withdrawals["user_id"].unique().tolist()
mar_unique_users = march_withdrawals["user_id"].unique().tolist()
apr_unique_users = april_withdrawals["user_id"].unique().tolist()
may_unique_users = may_withdrawals["user_id"].unique().tolist()
june_unique_users = june_withdrawals["user_id"].unique().tolist()
june_unique_users = june_withdrawals["user_id"].unique().tolist()
july_unique_users = july_withdrawals["user_id"].unique().tolist()
users_sep = len(sep_unique_users)
users_oct = len(oct_unique_users)
users_nov = len(nov_unique_users)
users_dec = len(dec_unique_users)
users_jan = len(jan_unique_users)
users_feb = len(feb_unique_users)
users_mar = len(mar_unique_users)
users_apr = len(apr_unique_users)
users_may = len(may_unique_users)
users_june = len(june_unique_users)
users_june = len(june_unique_users)
users_july = len(july_unique_users)
f = {}
f["Sep"] = users_sep
f["Oct"] = users_oct
f["Nov"] = users_nov
f["Dec"] = users_dec
f["Jan"] = users_jan
f["Feb"] = users_feb
f["March"] = users_mar
f["April"] = users_apr
f["May"] = users_may
f["June"] = users_june
f["June"] = users_june
f["July"] = users_july
f = pd.DataFrame(f.items())
f.columns = ['month', 'Unique Users (AU4s)']
final = pd.merge(final, f, on = "month")


print ("Sixth Run completed")
sep_unique_users = d2c_sep_withdrawals["user_id"].unique().tolist()
oct_unique_users = d2c_oct_withdrawals["user_id"].unique().tolist()
nov_unique_users = d2c_nov_withdrawals["user_id"].unique().tolist()
dec_unique_users = d2c_dec_withdrawals["user_id"].unique().tolist()
jan_unique_users = d2c_jan_withdrawals["user_id"].unique().tolist()
feb_unique_users = d2c_feb_withdrawals["user_id"].unique().tolist()
mar_unique_users = d2c_march_withdrawals["user_id"].unique().tolist()
apr_unique_users = d2c_april_withdrawals["user_id"].unique().tolist()
may_unique_users = d2c_may_withdrawals["user_id"].unique().tolist()
june_unique_users = d2c_june_withdrawals["user_id"].unique().tolist()
july_unique_users = d2c_july_withdrawals["user_id"].unique().tolist()
users_sep = len(sep_unique_users)
users_oct = len(oct_unique_users)
users_nov = len(nov_unique_users)
users_dec = len(dec_unique_users)
users_jan = len(jan_unique_users)
users_feb = len(feb_unique_users)
users_mar = len(mar_unique_users)
users_apr = len(apr_unique_users)
users_may = len(may_unique_users)
users_june = len(june_unique_users)
users_july = len(july_unique_users)
f = {}
f["Sep"] = users_sep
f["Oct"] = users_oct
f["Nov"] = users_nov
f["Dec"] = users_dec
f["Jan"] = users_jan
f["Feb"] = users_feb
f["March"] = users_mar
f["April"] = users_apr
f["May"] = users_may
f["June"] = users_june
f["July"] = users_july
f = pd.DataFrame(f.items())
f.columns = ['month', 'Unique Users AU4s(D2C)']
final = pd.merge(final, f, on = "month")
print ("Seventh Run completed")

sep_unique_users = b2b2c_sep_withdrawals["user_id"].unique().tolist()
oct_unique_users = b2b2c_oct_withdrawals["user_id"].unique().tolist()
nov_unique_users = b2b2c_nov_withdrawals["user_id"].unique().tolist()
dec_unique_users = b2b2c_dec_withdrawals["user_id"].unique().tolist()
jan_unique_users = b2b2c_jan_withdrawals["user_id"].unique().tolist()
feb_unique_users = b2b2c_feb_withdrawals["user_id"].unique().tolist()
mar_unique_users = b2b2c_march_withdrawals["user_id"].unique().tolist()
apr_unique_users = b2b2c_april_withdrawals["user_id"].unique().tolist()
may_unique_users = b2b2c_may_withdrawals["user_id"].unique().tolist()
june_unique_users = b2b2c_june_withdrawals["user_id"].unique().tolist()
july_unique_users = b2b2c_july_withdrawals["user_id"].unique().tolist()
users_sep = len(sep_unique_users)
users_oct = len(oct_unique_users)
users_nov = len(nov_unique_users)
users_dec = len(dec_unique_users)
users_jan = len(jan_unique_users)
users_feb = len(feb_unique_users)
users_mar = len(mar_unique_users)
users_apr = len(apr_unique_users)
users_may = len(may_unique_users)
users_june = len(june_unique_users)
users_july = len(july_unique_users)
f = {}
f["Sep"] = users_sep
f["Oct"] = users_oct
f["Nov"] = users_nov
f["Dec"] = users_dec
f["Jan"] = users_jan
f["Feb"] = users_feb
f["March"] = users_mar
f["April"] = users_apr
f["May"] = users_may
f["June"] = users_june
f["July"] = users_july
f = pd.DataFrame(f.items())
f.columns = ['month', 'Unique Users AU4s(B2B2C)']
final = pd.merge(final, f, on = "month")



print ("Eighth Run completed")


new_users_sep = d2c_sep_withdrawals[d2c_sep_withdrawals["New User"]=="Yes"].shape[0]
new_users_oct = d2c_oct_withdrawals[d2c_oct_withdrawals["New User"]=="Yes"].shape[0]
new_users_nov = d2c_nov_withdrawals[d2c_nov_withdrawals["New User"]=="Yes"].shape[0]
new_users_dec = d2c_dec_withdrawals[d2c_dec_withdrawals["New User"]=="Yes"].shape[0]
new_users_jan = d2c_jan_withdrawals[d2c_jan_withdrawals["New User"]=="Yes"].shape[0]
new_users_feb = d2c_feb_withdrawals[d2c_feb_withdrawals["New User"]=="Yes"].shape[0]
new_users_mar = d2c_march_withdrawals[d2c_march_withdrawals["New User"]=="Yes"].shape[0]
new_users_apr = d2c_april_withdrawals[d2c_april_withdrawals["New User"]=="Yes"].shape[0]
new_users_may = d2c_may_withdrawals[d2c_may_withdrawals["New User"]=="Yes"].shape[0]
new_users_june = d2c_june_withdrawals[d2c_june_withdrawals["New User"]=="Yes"].shape[0]
new_users_july = d2c_july_withdrawals[d2c_july_withdrawals["New User"]=="Yes"].shape[0]

f = {}
f["Sep"] = new_users_sep
f["Oct"] = new_users_oct
f["Nov"] = new_users_nov
f["Dec"] = new_users_dec
f["Jan"] = new_users_jan
f["Feb"] = new_users_feb
f["March"] = new_users_mar
f["April"] = new_users_apr
f["May"] = new_users_may
f["June"] = new_users_june
f["July"] = new_users_july
# sep_unique_users = d2c_sep_withdrawals["user_id"].unique().tolist()
# oct_unique_users = d2c_oct_withdrawals["user_id"].unique().tolist()
# nov_unique_users = d2c_nov_withdrawals["user_id"].unique().tolist()
# dec_unique_users = d2c_dec_withdrawals["user_id"].unique().tolist()
# jan_unique_users = d2c_jan_withdrawals["user_id"].unique().tolist()
# feb_unique_users = d2c_feb_withdrawals["user_id"].unique().tolist()
# mar_unique_users = d2c_march_withdrawals["user_id"].unique().tolist()
# apr_unique_users = d2c_april_withdrawals["user_id"].unique().tolist()
# may_unique_users = d2c_may_withdrawals["user_id"].unique().tolist()
# june_unique_users = d2c_june_withdrawals["user_id"].unique().tolist()
# new_users_sep = len(sep_unique_users)
# new_users_oct = len(list(set(oct_unique_users) - set(sep_unique_users)))
# new_users_nov = len(list(set(nov_unique_users) - set(oct_unique_users)))
# new_users_dec = len(list(set(dec_unique_users) - set(nov_unique_users)))
# new_users_jan = len(list(set(jan_unique_users) - set(dec_unique_users)))
# new_users_feb = len(list(set(feb_unique_users) - set(jan_unique_users)))
# new_users_mar = len(list(set(mar_unique_users) - set(feb_unique_users)))
# new_users_apr = len(list(set(apr_unique_users) - set(mar_unique_users)))
# new_users_may = len(list(set(may_unique_users) - set(apr_unique_users)))
# new_users_june = len(list(set(june_unique_users) - set(may_unique_users)))
# f = {}
# f["Sep"] = new_users_sep
# f["Oct"] = new_users_oct
# f["Nov"] = new_users_nov
# f["Dec"] = new_users_dec
# f["Jan"] = new_users_jan
# f["Feb"] = new_users_feb
# f["March"] = new_users_mar
# f["April"] = new_users_apr
# f["May"] = new_users_may
# f["June"] = new_users_june
f = pd.DataFrame(f.items())
f.columns = ['month', 'New users (D2C)']
final = pd.merge(final, f, on = "month")


print ("Ninth Run completed")



new_users_sep = b2b2c_sep_withdrawals[b2b2c_sep_withdrawals["New User"]=="Yes"].shape[0]
new_users_oct = b2b2c_oct_withdrawals[b2b2c_oct_withdrawals["New User"]=="Yes"].shape[0]
new_users_nov = b2b2c_nov_withdrawals[b2b2c_nov_withdrawals["New User"]=="Yes"].shape[0]
new_users_dec = b2b2c_dec_withdrawals[b2b2c_dec_withdrawals["New User"]=="Yes"].shape[0]
new_users_jan = b2b2c_jan_withdrawals[b2b2c_jan_withdrawals["New User"]=="Yes"].shape[0]
new_users_feb = b2b2c_feb_withdrawals[b2b2c_feb_withdrawals["New User"]=="Yes"].shape[0]
new_users_mar = b2b2c_march_withdrawals[b2b2c_march_withdrawals["New User"]=="Yes"].shape[0]
new_users_apr = b2b2c_april_withdrawals[b2b2c_april_withdrawals["New User"]=="Yes"].shape[0]
new_users_may = b2b2c_may_withdrawals[b2b2c_may_withdrawals["New User"]=="Yes"].shape[0]
new_users_june = b2b2c_june_withdrawals[b2b2c_june_withdrawals["New User"]=="Yes"].shape[0]
new_users_july = b2b2c_july_withdrawals[b2b2c_july_withdrawals["New User"]=="Yes"].shape[0]

f = {}
f["Sep"] = new_users_sep
f["Oct"] = new_users_oct
f["Nov"] = new_users_nov
f["Dec"] = new_users_dec
f["Jan"] = new_users_jan
f["Feb"] = new_users_feb
f["March"] = new_users_mar
f["April"] = new_users_apr
f["May"] = new_users_may
f["June"] = new_users_june
f["July"] = new_users_july

# sep_unique_users = b2b2c_sep_withdrawals["user_id"].unique().tolist()
# oct_unique_users = b2b2c_oct_withdrawals["user_id"].unique().tolist()
# nov_unique_users = b2b2c_nov_withdrawals["user_id"].unique().tolist()
# dec_unique_users = b2b2c_dec_withdrawals["user_id"].unique().tolist()
# jan_unique_users = b2b2c_jan_withdrawals["user_id"].unique().tolist()
# feb_unique_users = b2b2c_feb_withdrawals["user_id"].unique().tolist()
# mar_unique_users = b2b2c_march_withdrawals["user_id"].unique().tolist()
# apr_unique_users = b2b2c_april_withdrawals["user_id"].unique().tolist()
# may_unique_users = b2b2c_may_withdrawals["user_id"].unique().tolist()
# june_unique_users = b2b2c_june_withdrawals["user_id"].unique().tolist()
# new_users_sep = len(sep_unique_users)
# new_users_oct = len(list(set(oct_unique_users) - set(sep_unique_users)))
# new_users_nov = len(list(set(nov_unique_users) - set(oct_unique_users)))
# new_users_dec = len(list(set(dec_unique_users) - set(nov_unique_users)))
# new_users_jan = len(list(set(jan_unique_users) - set(dec_unique_users)))
# new_users_feb = len(list(set(feb_unique_users) - set(jan_unique_users)))
# new_users_mar = len(list(set(mar_unique_users) - set(feb_unique_users)))
# new_users_apr = len(list(set(apr_unique_users) - set(mar_unique_users)))
# new_users_may = len(list(set(may_unique_users) - set(apr_unique_users)))
# new_users_june = len(list(set(june_unique_users) - set(may_unique_users)))
# f = {}
# f["Sep"] = new_users_sep
# f["Oct"] = new_users_oct
# f["Nov"] = new_users_nov
# f["Dec"] = new_users_dec
# f["Jan"] = new_users_jan
# f["Feb"] = new_users_feb
# f["March"] = new_users_mar
# f["April"] = new_users_apr
# f["May"] = new_users_may
# f["June"] = new_users_june
f = pd.DataFrame(f.items())
f.columns = ['month', 'New users (B2B2C)']
final = pd.merge(final, f, on = "month")


f = {}
f["Sep"] = round(sep_withdrawals["processing_fees"].sum(),2)
f["Oct"] = round(oct_withdrawals["processing_fees"].sum(),2)
f["Nov"] = round(nov_withdrawals["processing_fees"].sum(), 2)
f["Dec"] = round(dec_withdrawals["processing_fees"].sum(), 2)
f["Jan"] = round(jan_withdrawals["processing_fees"].sum(), 2)
f["Feb"] = round(feb_withdrawals["processing_fees"].sum(), 2)
f["March"] = round(march_withdrawals["processing_fees"].sum(), 2)
f["April"] = round(april_withdrawals["processing_fees"].sum(), 2)
f["May"] = round(may_withdrawals["processing_fees"].sum(), 2)
f["June"] = round(june_withdrawals["processing_fees"].sum(), 2)
f["July"] = round(july_withdrawals["processing_fees"].sum(), 2)

f = pd.DataFrame(f.items())
f.columns = ['month', 'Total Revenue Generated (INR)']
final = pd.merge(final, f, on = "month")

print ("Tenth Run completed")
f = {}
f["Sep"] = round(d2c_sep_withdrawals["processing_fees"].sum(), 2)
f["Oct"] = round(d2c_oct_withdrawals["processing_fees"].sum(), 2)
f["Nov"] = round(d2c_nov_withdrawals["processing_fees"].sum(), 2)
f["Dec"] = round(d2c_dec_withdrawals["processing_fees"].sum(), 2)
f["Jan"] = round(d2c_jan_withdrawals["processing_fees"].sum(), 2)
f["Feb"] = round(d2c_feb_withdrawals["processing_fees"].sum(), 2)
f["March"] = round(d2c_march_withdrawals["processing_fees"].sum(), 2)
f["April"] = round(d2c_april_withdrawals["processing_fees"].sum(), 2)
f["May"] = round(d2c_may_withdrawals["processing_fees"].sum(), 2)
f["June"] = round(d2c_june_withdrawals["processing_fees"].sum(), 2)
f["July"] = round(d2c_july_withdrawals["processing_fees"].sum(), 2)

f = pd.DataFrame(f.items())
f.columns = ['month', 'Total Revenue Generated D2C (INR)']
final = pd.merge(final, f, on = "month")

f = {}
f["Sep"] = round(b2b2c_sep_withdrawals["processing_fees"].sum(), 2)
f["Oct"] = round(b2b2c_oct_withdrawals["processing_fees"].sum(), 2)
f["Nov"] = round(b2b2c_nov_withdrawals["processing_fees"].sum(), 2)
f["Dec"] = round(b2b2c_dec_withdrawals["processing_fees"].sum(), 2)
f["Jan"] = round(b2b2c_jan_withdrawals["processing_fees"].sum(), 2)
f["Feb"] = round(b2b2c_feb_withdrawals["processing_fees"].sum(), 2)
f["March"] = round(b2b2c_march_withdrawals["processing_fees"].sum(), 2)
f["April"] = round(b2b2c_april_withdrawals["processing_fees"].sum(), 2)
f["May"] = round(b2b2c_may_withdrawals["processing_fees"].sum(), 2)
f["June"] = round(b2b2c_june_withdrawals["processing_fees"].sum(), 2)
f["July"] = round(b2b2c_july_withdrawals["processing_fees"].sum(), 2)

f = pd.DataFrame(f.items())
f.columns = ['month', 'Total Revenue Generated B2B2C (INR)']
final = pd.merge(final, f, on = "month")

final["Revenue per user (Total)"] = round(final["Total Revenue Generated (INR)"]/final["Unique Users (AU4s)"],2)
final["Revenue per user (D2C"] = round(final["Total Revenue Generated D2C (INR)"]/final["Unique Users AU4s(D2C)"],2)
final["Revenue per user (B2B2C)"] = round(final["Total Revenue Generated B2B2C (INR)"]/final["Unique Users AU4s(B2B2C)"],2)

final["Revenue per txn (Total)"] = round(final["Total Revenue Generated (INR)"]/final["Total no of withdrawals"],2)
final["Revenue per txn (D2C"] = round(final["Total Revenue Generated D2C (INR)"]/final["D2C"],2)
final["Revenue per txn (B2B2C)"] = round(final["Total Revenue Generated B2B2C (INR)"]/final["B2B2C"],2)


# final = final.sort_values(columns = ["index"], ascending=True)

india_mom_bq = bq_cleaner(final)
pandas_gbq.to_gbq(india_mom_bq, destination_table="Marketing.India D2C and Quess - MoM", project_id="data-warehouse-india", if_exists="replace")












import datetime
print ("Connecting to Big Query Table")
query_string = "select * FROM `data-warehouse-india.Code_internal.Run_times_code`"
code__times = pd.read_gbq(query_string, project_id="data-warehouse-india")

c = code__times[code__times["Code"]==code_name]


code__times.at[c.index.tolist()[-1], "Run_time"] = time.time() - start_1
code__times.at[c.index.tolist()[-1], "time_updated"] = datetime.datetime.now()

code__times["time_updated"] = pd.to_datetime(code__times["time_updated"])
code__times = code__times.sort_values("time_updated")

code__times_bq = bq_cleaner(code__times)
pandas_gbq.to_gbq(code__times_bq, destination_table="Code_internal.Run_times_code", project_id="data-warehouse-india", if_exists="replace")

print ("\n")
print (time.time() - start_1)










