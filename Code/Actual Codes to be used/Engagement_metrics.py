#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 18 18:41:59 2022

@author: arunabhmajumdar
"""

code_name = "Engagement_metrics"
print ("Importing all packages and Google BQ credentials files")
import warnings
warnings.filterwarnings("ignore")
import pandas as pd,os
import base64
import boto3
import time
import pandas as pd, os
from oauth2client.service_account import ServiceAccountCredentials
import gspread
import gspread_dataframe as gd
import psycopg2
import df2gspread as d2g
import pandas as pd
import warnings
warnings.filterwarnings("ignore")
import numpy as np
import re
import calendar
import os
import json
import time
print ("Changing directory to Code to aid ease of access to the various jsons and xlsx")
print(os.getcwd())
os.chdir("..")
time.sleep(2)
print ("Changed Directory")
print (os.getcwd())
start_1 = time.time()
import numpy as np
import pytz
my_timezone = pytz.timezone('Asia/Calcutta')
import ast
import gzip
from datetime import timedelta
from currency_converter import CurrencyConverter
from google.oauth2 import service_account
from google.cloud import bigquery
import pandas_gbq
import os, pandas as pd
os.getcwd()
KEY_PATH = "data-warehouse-india-84f5f8a775d1.json"
CREDS = service_account.Credentials.from_service_account_file(KEY_PATH)
bq_client = bigquery.Client(credentials=CREDS, project="data-warehouse-india")
import warnings
warnings.filterwarnings("ignore")


print ("Connecting to Dynamo DB using boto3")
start = time.time()
print ("starting run")
session = boto3.session.Session(profile_name="rain-india-prod")
client = session.client("dynamodb")
dynamodb = boto3.resource("dynamodb")
print ("Connecting to Postgres using psycopg2")
connection = psycopg2.connect(user="rainadmin",
                                      password="Mudar123",
                                      host="localhost",
                                      port=55432,
                                      database="rain")
cursor = connection.cursor()
# Print PostgreSQL details
print("PostgreSQL server information")
print(connection.get_dsn_parameters(), "\n")
# cursor.itersize = 10000
cursor.execute("SELECT version();")
    # Fetch result
record = cursor.fetchone()
print("You are connected to - ", record, "\n")

print ("Function to download Postgres data and concert that to dataframe")
def dataframe_generator(query):
    cursor.execute(query)
    print('Read table in PostgreSQL')
    data = cursor.fetchall()
    cols = []
    for elt in cursor.description:
        cols.append(elt[0])
    df= pd.DataFrame(data = data, columns=cols)
    return df

print ("Function to clean dataframe to include only data post Sept 1, 2021")
def clean(df):
    df["created_at"] = df["created_at"].dt.date.astype(str)
    df = df[df["created_at"]>"2021-08-31"]
    return df
print (time.time() - start)


print ("Function to clean dataframe to upload to BQ, BQ doesn't take spaces, dashes etc;")
def bq_cleaner(df):
    new_cols = []
    l = df.columns.tolist()
    for x in l:
        x = x.replace("(","_")
        x = x.replace(" ","_")
        x = x.replace(")","_")
        new_cols.append(x)
    df.columns = new_cols
    df = df.astype(str)
    return df



print ("Connecting to Big Query Table")
query_string = "select * FROM `data-warehouse-india.Processed_data.withdrawals-txns_without_bank`"
all_rows = pd.read_gbq(query_string, project_id="data-warehouse-india")





all_rows["organization_id"].replace({"c2a6a007-e625-456f-8c36-92cd2654c971": 'Quess',
                                              "916227f6-cb69-46ec-8cb1-a735ed98f2c4": 'D2C Org', 
                                             "d779558a-09cc-4920-9f39-d8409c8f0728":"B2B Test", 
                                             "0706c2e4-5153-4429-9645-a6d0c4a26a04":"HCL", 
                                    "6740ddac-09fa-4c0b-9cad-a261cc23997e":"Cloudnine", 
                                    "2f57a3bb-26a6-4cd6-953e-ce61fe607e6a":"Wissen Infotech", 
                                    "a8957481-3705-4d7f-b521-491a7e859f47":"Support.com", 
                                    "752927b6-2655-4760-8fea-33e7417b2a75":"Sutherland"},inplace=True)
print (time.time() - start)

all_rows["disbursal_txn__date"] = pd.to_datetime(all_rows["disbursal_txn__date"]).dt.date.astype(str)


from datetime import datetime
today = datetime.strftime(pd.to_datetime("today"), "%Y-%m-%d")


under30 = datetime.strftime(pd.to_datetime("today") - timedelta(30), "%Y-%m-%d")
under60 = datetime.strftime(pd.to_datetime("today") - timedelta(60), "%Y-%m-%d")
under90 = datetime.strftime(pd.to_datetime("today") - timedelta(90), "%Y-%m-%d")



_30_days = all_rows[(all_rows["disbursal_txn__date"]>under30)]
_60_days = all_rows[(all_rows["disbursal_txn__date"]>under60)]
_90_days = all_rows[(all_rows["disbursal_txn__date"]>under90)]




_30_days = _30_days.groupby("user_id").count().reset_index()[["user_id", "Total_Amount"]]
_60_days = _60_days.groupby("user_id").count().reset_index()[["user_id", "Total_Amount"]]
_90_days = _90_days.groupby("user_id").count().reset_index()[["user_id", "Total_Amount"]]





a = all_rows["user_id"].unique().tolist()
b = _90_days["user_id"].unique().tolist()
c = _60_days["user_id"].unique().tolist()
d = _30_days["user_id"].unique().tolist()



churned_list = list(set(a) - set(b))
dormant_list = list(set(b) - set(c))
no_txn_30_days = list(set(c) - set(d))

power_users = _30_days[_30_days["Total_Amount"]>10]
power_list = power_users["user_id"].unique().tolist()
core_users = _30_days[(_30_days["Total_Amount"]>5)&(_30_days["Total_Amount"]<10)]
core_list = core_users["user_id"].unique().tolist()
casual_users = _30_days[(_30_days["Total_Amount"]>1)&(_30_days["Total_Amount"]<5)]
casual_list = casual_users["user_id"].unique().tolist()
power = all_rows[all_rows["user_id"].isin(power_list)]
core = all_rows[all_rows["user_id"].isin(core_list)]
casual = all_rows[all_rows["user_id"].isin(casual_list)]
dormant = all_rows[all_rows["user_id"].isin(dormant_list)]
churn = all_rows[all_rows["user_id"].isin(churned_list)]
no_txn_30_days = all_rows[all_rows["user_id"].isin(no_txn_30_days)]
power_bq = bq_cleaner(power)
pandas_gbq.to_gbq(power_bq, destination_table="Engagement_metrics.power_users", project_id="data-warehouse-india", if_exists="replace")
core_bq = bq_cleaner(core)
pandas_gbq.to_gbq(core_bq, destination_table="Engagement_metrics.core_users", project_id="data-warehouse-india", if_exists="replace")
casual_bq = bq_cleaner(casual)
pandas_gbq.to_gbq(casual_bq, destination_table="Engagement_metrics.casual_users", project_id="data-warehouse-india", if_exists="replace")
dormant_bq = bq_cleaner(dormant)
pandas_gbq.to_gbq(dormant_bq, destination_table="Engagement_metrics.dormant_users", project_id="data-warehouse-india", if_exists="replace")
churn_bq = bq_cleaner(churn)
pandas_gbq.to_gbq(churn_bq, destination_table="Engagement_metrics.churn_users", project_id="data-warehouse-india", if_exists="replace")
no_txn_30_days_bq = bq_cleaner(no_txn_30_days)
pandas_gbq.to_gbq(no_txn_30_days_bq, destination_table="Engagement_metrics.no_txn_30_days", project_id="data-warehouse-india", if_exists="replace")

import datetime

print ("Connecting to Big Query Table")
query_string = "select * FROM `data-warehouse-india.Code_internal.Run_times_code`"
code__times = pd.read_gbq(query_string, project_id="data-warehouse-india")

c = code__times[code__times["Code"]==code_name]


code__times.at[c.index.tolist()[-1], "Run_time"] = time.time() - start_1
code__times.at[c.index.tolist()[-1], "time_updated"] = datetime.datetime.now()

code__times["time_updated"] = pd.to_datetime(code__times["time_updated"])
code__times = code__times.sort_values("time_updated")

code__times_bq = bq_cleaner(code__times)
pandas_gbq.to_gbq(code__times_bq, destination_table="Code_internal.Run_times_code", project_id="data-warehouse-india", if_exists="replace")

print ("\n")
print ("Total Run Time: "+str(time.time() - start_1))




print ("/n")
print ("Engagement metrics run in "+str(time.time() - start_1))
