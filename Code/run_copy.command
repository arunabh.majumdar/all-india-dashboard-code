#!/bin/sh


echo "Cron Started"
start=$SECONDS

# cd /Users/arunabhmajumdar/Desktop/
# echo 'This is a test' > cron_test.txt
# echo 'yet another line' >> cron_test.txt
# echo 'yet another another line' >> cron_test.txt


echo "Start Bastion server"
osascript -e 'tell app "Terminal"
   do script "ssh -i ~/.ssh/prod-bastion-india.pem ec2-user@13.233.106.82 -p 22 -L 55432:prod-rds.ctzcckgcc8qw.ap-south-1.rds.amazonaws.com:5432"
end tell'


echo "Start AWS Login"
aws sso login --profile rain-india-prod


echo "print current working directory"

pwd
cd
echo "changing directory to home"
pwd


echo "Running Google Drive update"
pwd
cd Google\ Drive/Shared\ drives/KYC_and_Agreements/AWS_Data/
pwd

aws s3 sync s3://india.kyc.rain.us . --profile rain-india-prod
cd ..
cd Agreements/
aws s3 sync s3://loan-agreements.in . --profile rain-india-prod 
cd

pwd




echo "Running AWS_Quess_Landing_Page S3 Pull"

aws s3 sync s3://quess-landing-page /Users/arunabhmajumdar/Documents/all-india-dashboard-code/AWS_Quess_Landing_Page --profile rain-india-prod 



echo "Running OTP Kinesis S3 pull"

aws s3 sync s3://cw-kinesis-s3-replica /Users/arunabhmajumdar/Documents/all-india-dashboard-code/otp_S3_Kinesis_Dump --profile rain-india-prod 


pwd
echo "Paused/Unpaused Run"

cd /Users/arunabhmajumdar/Documents/all-india-dashboard-code/Code/Actual\ Codes\ to\ be\ used/
python paused_unpaused.py

echo "All clear"

echo "Cron Ended"


