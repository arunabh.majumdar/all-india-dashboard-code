import requests
import pandas as pd
import pandas_gbq
import os

session = requests.Session()
session.headers.update({'Authorization': 'Bearer dG9rOjdmY2ZlYWJkXzgxN2NfNDdmNl9hYjI4X2JkYjdjNGEwNmQwMDoxOjA=',
                       'Accept' : 'application/json'})
def get_jobs():
    url = "https://api.intercom.io/contacts" 
    first_page = session.get(url, params={'per_page':150}).json()
    yield first_page
    num_pages = first_page['pages']['total_pages']
    starting_after = first_page['pages']['next']['starting_after']
    for page in range(2, num_pages):
        next_page = session.get(url, params={'per_page':150,'starting_after': starting_after}).json()
        starting_after = next_page['pages']['next']['starting_after']
        print(page)
        yield next_page
    last_page = session.get(url, params={'per_page':150,'starting_after': starting_after}).json()
    yield last_page

all_rows = []
for page in get_jobs():
    all_rows = all_rows + page['data']
    
df = pd.DataFrame(all_rows)

print ("Function to clean dataframe to upload to BQ, BQ doesn't take spaces, dashes etc;")
def bq_cleaner(df):
    new_cols = []
    l = df.columns.tolist()
    for x in l:
        x = x.replace("(","_")
        x = x.replace(" ","_")
        x = x.replace(")","_")
        x = x.replace("/","_")
        x = x.replace(".","_")
        new_cols.append(x)
    df.columns = new_cols
    df = df.astype(str)
    return df
gcp_project = "data-warehouse-india-copy"
KEY_PATH = "data-warehouse-india-copy-5949defd88ea.json"
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = KEY_PATH

df_bq = bq_cleaner(df)
pandas_gbq.to_gbq(df_bq, destination_table="Processed_data.Intercom_dump", project_id=gcp_project, if_exists="replace")
